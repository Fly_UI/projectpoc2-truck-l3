#ifndef CHASSIC_H
#define CHASSIC_H
struct chassic_cmd {
  int acc_vaild; 
  float long_acc; 
  float target_acc_dec;

  float target_steer_angle;
  int is_request_steer_control;
  float target_steer_angle_rate;
  int is_request_steer_rate_control;
  int is_request_eps_hand;

  float target_torque;
  int is_request_torque_control;
  int acc_state;

  void clear() {
    acc_vaild = 0; 
    long_acc = 0.0; 
    target_acc_dec = 0.0;

    target_steer_angle = 0.0;
    is_request_steer_control = 0;
    target_steer_angle_rate = 0.0;
    is_request_steer_rate_control = 0;
    is_request_eps_hand = 0;

    target_torque = 0;
    is_request_torque_control = 0;

    target_torque = 0; 
    is_request_torque_control = 0;
    acc_state;

  };

  chassic_cmd() {
    clear();
  };
};


#endif