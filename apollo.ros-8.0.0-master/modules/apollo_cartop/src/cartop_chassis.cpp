#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include "controlcan.h"
#include "chassic.h"

#include <ctime>
#include <cstdlib>
#include "unistd.h"

#include <iostream>

#include "ros/ros.h"
#include <sstream>
#include <geometry_msgs/Twist.h>

#include <string>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

#include <ctime>
#include <stdint.h>
// add by travis
#include "cartop_msgs/carTop.h"//自定义消息类型
#include <csignal> 
#include <thread>
#include <chrono>
// add by traviss
#define PI 3.141592653

#define Tack_CAN_LOG (1)
#define IS_AKM (0)

VCI_BOARD_INFO pInfo;
bool isWriteNow;
#if IS_AKM

#else
// VCU运行状态
UCHAR vcu_sts_gear;           // 档位状态
USHORT vcu_pm_accpedl_value;  // 加速踏板开度值
UCHAR vcu_sts_veh_ready;      // 车辆READY状态
UCHAR vcu_sts_brkpdl_sw;      // 制动踏板开关状态
UCHAR vcu_pm_can_life;        // VCU心跳
// VCU使能
UCHAR vcu_sts_ft_level;       // VCU故障等级
// BCM运行状态
UCHAR BCM_FrontFogLampSts;    // 前雾灯工作状态
UCHAR BCM_TurnIndicatorLeft;  // 左转向灯工作状态
UCHAR BCM_TurnIndicatorRight; // 右转向灯工作状态
UCHAR BCM_DriverDoorStatus;   // 主驾门状态
UCHAR BCM_PassengerDoorStatus;// 副驾门状态
UCHAR BCM_LeftRearDoorStatus; // 左后门状态
UCHAR BCM_RightRearDoorStatus;// 右后门状态
// EPS运行状态
short eps_pm_angle;           // 转向角度
USHORT eps_pm_angle_spd;      // 转向速度
UCHAR EPS_CtlSt;              // EPS握手反馈信号 
UCHAR eps_sts_cal;            // 回位标定
UCHAR eps_sts_ft;             // EPS故障
// ABS故障信息
UCHAR abs_ft_fl_spd;          // ABS左前轮速度故障
float abs_pm_fl_spd;          // ABS左前轮速度
UCHAR abs_ft_fr_spd;          // ABS右前轮速度故障
float abs_pm_fr_spd;          // ABS右前轮速度
UCHAR abs_ft_rr_spd;          // ABS右后轮速度故障
UCHAR abs_ft_rl_spd;          // ABS左前轮速度故障
float abs_pm_rl_spd;          // ABS左后轮速度
UCHAR abs_pm_can_life_2;      // 心跳值2
float abs_pm_rr_spd;          // ABS右后轮车速
// ABS运行状态
UCHAR abs_ft_abs;             // ABS故障状态
UCHAR abs_sts_active;         // ABS激活状态
float abs_pm_veh_spd;         // ABS车速值
UCHAR abs_sts_veh_spd_valid;  // 车速有效性
UCHAR RoadMu;                 // 路面附着
// 电子驻车运行状态
UCHAR EPB_SystemStatus;       // EPB状态
UCHAR EPB_FAULT_LAMP;         // EPB故障指示灯
// 整车故障信息
UCHAR vcu_ft_veh_brksys;      // 制动系统故障
// 整车能量信息
float vcu_pm_veh_spd;         // 整车车速值
// VCU运行状态2
UCHAR vcu_sts_dcu_handshake;  // DCU握手反馈信号
float gw_mcu_pm_otp_torq;     // 电机扭矩
// VCU转发电机信息
float mcu_pm_max_drv_torq;    // MCU最大允许驱动扭矩
float mcu_pm_max_regen_torq;  // MCU最大允许发电扭矩
// 时间信息
USHORT tbox_pm_time_hr;       // 小时
USHORT tbox_pm_time_mn;       // 分钟
USHORT tbox_pm_time_sc;       // 秒
USHORT tbox_pm_date_yy;       // 年
USHORT tbox_pm_date_mm;       // 月
USHORT tbox_pm_date_dd;       // 日
// 仪表信息
UCHAR ic_sts_safetybelt;      // 安全带状态

UCHAR DCU_EHBHydrMainReq;     // 保压请求
UCHAR DCU_EHBHandshakeReq;    // EHB握手请求
UCHAR LongitudinalACCValid;   // 目标加减速度
UINT DCU_EHBTargetAccDec;    // 纵向加速度有效位
UCHAR DCU_EHBPrefill;         // 预冲压请求
UCHAR Rollingcounter_1;       // E2E保护
USHORT Checksum_1;            // E2E保护
float LongitudinalACC;        // 纵向加速度 

UCHAR DCU_EPSSteerAngleReq;   // 方向盘转角请求
UINT DCU_EPSSteerAngle;      // 方向盘转角（左正右负）
UCHAR DCU_EPSSteerAngleSpdReq;// 转角速度请求请求，预留
UINT DCU_EPSSteerAngleSpd;   // 转角速度（左正右负），预留
UCHAR DCU_EPSHandshakeReq;    // EPS握手请求
UINT DCU_EPSWorkmodeReq;     // EPS工作模式请求

UCHAR DCU_EPBReq;             // EPB拉起请求
UCHAR DCU_BCMTurnLightReq;    // 转向灯请求
UCHAR DCU_BCMBrakeLightReq;   // 制动灯请求
UCHAR DCU_BCMLowBeamReq;      // 近光灯请求
UCHAR DCU_BCMEmergFlash;      // 双闪请求
UCHAR Rollingcounter_2;       // E2E保护
USHORT Checksum_2;            // E2E保护

UCHAR DCU_VCUTorqReq;         // 扭矩请求
UINT DCU_VCUTorq;            // 扭矩请求值
UCHAR DCU_VCUHandshakeReq;    // VCU握手请求
UCHAR DCU_GearReq;            // 档位请求
UCHAR DCU_DriverOverride;     // 驾驶员踩油门接管车辆状态
UCHAR Rollingcounter_3;       // E2E保护
USHORT Checksum_3;            // E2E保护
UCHAR DCU_ACCSt;              // ACC状态
UCHAR DCU_SystemFailure;      // DCU故障状态
UCHAR DCU_EHBTargetAccDecReq; // 目标加减速度请求

USHORT TBS_pub1_CheckSum;     // 报文检验和
UCHAR TBS_RollingCounter;     // 循环计数
UCHAR TBS_BrkPedlAppldFlg;    // 制动踏板踩下标志位
UCHAR TBS_BrkPedlAppldVld;    // 制动踏板踩下标志位的有效性
USHORT TBS_OutputRodAct_1;    // 制动踏板位移信号
float TBS_Motor_TgtT;         // 再生制动时，TBS目标电机回收扭矩
UCHAR TBS_Motor_TgtTQ;        // 再生制动时，TBS_Motor_TgtT的有效性
UCHAR TBS_WarnOn;             // 需求仪表显示故障警示灯
UCHAR TBS_FailLevel;          // TBS故障等级
UCHAR TBS_Boostlimitied;      // 助力是否受限
UCHAR TBS_Handshake;          // 握手反馈

USHORT TBS_pub2_CheckSum;     // 报文检验和
UCHAR TBS_RollingCounter_2;   // 循环计数
float TBS_MCP;                // 主缸压力
float TBS_OutputRodAct_2;     // 主缸位移信号
UCHAR TBS_Prefill_Handshake;  // 预充握手信号
UCHAR TBS_EBRAcailable;       // EBR是否可用
// EPS信号回传
USHORT eps_Torque;            // 扭矩信号
USHORT eps_FaultCode;         // 当前故障码
USHORT eps_sts_angle_aligmidleFbk;// 角度对中结果反馈
USHORT eps_SoftwareVersion;   // 软件版本
USHORT eps_HardwareVersion;   // 硬件版本
UCHAR eps_steeringwheelcontrol;// 驾驶员方向盘操控状态

USHORT eps_sts_angle_aligmidle; // 角度对中指令

float temp_abs_pm_veh_spd;
float temp_abs_pm_fl_spd;
float temp_abs_pm_fr_spd;
float temp_abs_pm_rl_spd;
float temp_abs_pm_rr_spd;
float temp_gw_mcu_pm_otp_torq;
float temp_mcu_pm_max_drv_torq; 
float temp_mcu_pm_max_regen_torq;
float temp_TBS_OutputRodAct_1;
float temp_TBS_Motor_TgtT;
float temp_eps_Torque;
float temp_TBS_MCP;
float temp_TBS_OutputRodAct_2;
#endif
//add by travis
void signalHandler(int signum) {
    VCI_CloseDevice(VCI_USBCAN2,0);
    usleep(50000);
    // 终止程序
    exit(signum);
}
//add by travis
#if IS_AKM
//send can data to AKM
void SendSpeedAndAngleToAKM(float target_lineSpeed, float target_angle) {
  std::cout << "Send Can Speed and Angle Start !" << std::endl;
  // 速度 m/s转化为mm/s, 角度rad 转 deg * 1000 
  float physics_linespeed = target_lineSpeed * 1000.0;      
  float physics_angle = target_angle * 100.0 * 180.0 / PI;
  //需要发送的帧，结构体设置
  VCI_CAN_OBJ send[1];
  send[0].ID = 0x00000001;//根据底盘can协议，发送时候can的ID为0x01
  send[0].SendType = 0;
  send[0].RemoteFlag = 0;
  send[0].ExternFlag = 0;
  send[0].DataLen = 8;

  //要写入的数据
  send[0].Data[0] = 0x00000001;
  send[0].Data[1] = 0x01;
  send[0].Data[2] = (int16_t)physics_linespeed & 0xFF;
  send[0].Data[3] = ((int16_t)physics_linespeed >> 8) & 0xFF;
  send[0].Data[4] = (int16_t)physics_angle & 0xFF;
  send[0].Data[5] = ((int16_t)physics_angle >> 8) & 0xFF;
  send[0].Data[6] = 0x00;
  send[0].Data[7] = 0x00;
  
  //写入数据
  if(VCI_Transmit(VCI_USBCAN2, 0, 0, send, 1) == 1) {
    printf("SendSpeedAndAngle TX data successful!\n");
  }
}
//档位设置函数0：低速档，1：中速档，2：高速档，默认低速档
void SetGearToAKM(int target_gear) {
  std::cout << "Send Can Speed and Angle Start !" << std::endl;
  //需要发送的帧，结构体设置
  VCI_CAN_OBJ send[1];
  send[0].ID = 0x00000001;//根据底盘can协议，发送时候can的ID为0x01
  send[0].SendType = 0;
  send[0].RemoteFlag = 0;
  send[0].ExternFlag = 0;
  send[0].DataLen = 8;

  //要写入的数据
  send[0].Data[0] = 0x00000001;
  send[0].Data[1] = 0x03;
  send[0].Data[2] = target_gear;
  send[0].Data[3] = 0x00;
  send[0].Data[4] = 0x00;
  send[0].Data[5] = 0x00;
  send[0].Data[6] = 0x00;
  send[0].Data[7] = 0x00;

  //写入数据
  if(VCI_Transmit(VCI_USBCAN2, 0, 0, send, 1) == 1) {
    printf("SetGear TX data successful!\n");
  }
}

void GetInfoFromAKM(const VCI_CAN_OBJ &can_msg, carTop::carTop &msg) {
  if((can_msg.Data[0] == 02) && (can_msg.Data[1] == 02)) {   
  //上报的后轮速度
  float temp_back_wheel_speed  = (can_msg.Data[3] << 8) | can_msg.Data[2];
  //上报的转向角度
  float temp_turn_angle = (can_msg.Data[5] << 8) | can_msg.Data[4];
  //上报的电量百分比
  char temp_battery_level = can_msg.Data[6];
  //上报的错误状态
  char temp_error_flag = can_msg.Data[7];

  msg.back_wheel_speed = temp_back_wheel_speed;
  msg.turn_angle = temp_turn_angle / 100.0 * PI / 180.0;
  msg.battery_level = temp_battery_level;
  msg.error_flag = temp_error_flag;
#if Tack_CAN_LOG
  std::cout << "back_wheel_speed : " << temp_back_wheel_speed << "mm/s, turn_angle :"
            << temp_turn_angle / 100.00 * PI / 180.0 << "rad, error_flag : " 
            << temp_error_flag << "[-], battery_level : " << temp_battery_level << std::endl;
#endif
  }
}

#else
void GetInfoFromChassic(const VCI_CAN_OBJ &can_msg, cartop_msgs::carTop &msg) {

  switch (can_msg.ID) {
    
    // case (0x210): {                                                                           // VCU运行状态
    //   vcu_sts_gear = (can_msg.Data[0] >> 2) & 0x03;                                           // 档位状态
    //   vcu_pm_accpedl_value = can_msg.Data[1] & 0xFF;                                          // 加速踏板开度值
    //   vcu_sts_veh_ready = (can_msg.Data[3] >> 3) & 0x01;                                      // 车辆READY状态
    //   vcu_sts_brkpdl_sw = (can_msg.Data[3] >> 7) & 0x01;                                      // 制动踏板开关状态
    //   vcu_pm_can_life = can_msg.Data[7] & 0x0F;                                               // VCU心跳
    //   break; }
    // case (0x211): {                                                                           // VCU使能
    //   vcu_sts_ft_level = (can_msg.Data[0] >> 4) & 0x03;                                       // VCU故障等级
    //   break; }
    // case (0x214): {                                                                           // BCM运行状态           
    //   BCM_FrontFogLampSts = (can_msg.Data[0] >> 4) & 0x03;                                    // 前雾灯工作状态
    //   BCM_TurnIndicatorLeft = can_msg.Data[1] & 0x03;                                         // 左转向灯工作状态
    //   BCM_TurnIndicatorRight = (can_msg.Data[1] >> 2) & 0x03;                                 // 右转向灯工作状态
    //   BCM_DriverDoorStatus = (can_msg.Data[1] >> 4) & 0x01;                                   // 主驾门状态
    //   BCM_PassengerDoorStatus = (can_msg.Data[1] >> 5) & 0x01;                                // 副驾门状态
    //   BCM_LeftRearDoorStatus = (can_msg.Data[1] >> 6) & 0x01;                                 // 左后门状态
    //   BCM_RightRearDoorStatus = (can_msg.Data[1] >> 7) & 0x01;                                // 右后门状态
    //   break; }
    case (0x221): {                                                                           // EPS运行状态
      eps_pm_angle = (can_msg.Data[0] & 0xFF) << 8 | can_msg.Data[1] & 0xFF;                  // 转向角度
      eps_pm_angle_spd = can_msg.Data[2] & 0xFF;                                              // 转向速度
      EPS_CtlSt = can_msg.Data[3] & 0x01;                                                     // EPS握手反馈信号 
      eps_sts_cal = (can_msg.Data[3] >> 6) & 0x01;                                            // 回位标定
      eps_sts_ft = (can_msg.Data[3] >> 7) & 0x01;                                             // EPS故障

      eps_pm_angle = eps_pm_angle * 0.1;
      eps_pm_angle_spd = eps_pm_angle_spd * 4;
      std::cout << std::dec << "eps_pm_angle" << eps_pm_angle << std::endl;
      break; }
    // case (0x310): {                                                                           // ABS运行状态
    //   abs_ft_fl_spd = can_msg.Data[1] & 0x01;                                                 // ABS左前轮速度故障
    //   abs_pm_fl_spd = (can_msg.Data[0] & 0xFF) << 5 | (can_msg.Data[1] & 0xF8) >> 3;          // ABS左前轮速度
    //   abs_ft_fr_spd = can_msg.Data[3] & 0x01;                                                 // ABS右前轮速度故障
    //   abs_pm_fr_spd = (can_msg.Data[2] & 0xFF) << 5 | (can_msg.Data[3] & 0xF8) >> 3;          // ABS右前轮速度
    //   abs_ft_rr_spd = (can_msg.Data[5] & 0x02) >> 1;                                          // ABS右后轮速度故障
    //   abs_ft_rl_spd = (can_msg.Data[5] & 0x04) >> 2;                                          // ABS左前轮速度故障
    //   abs_pm_rl_spd = (can_msg.Data[4] & 0xFF) << 5 | (can_msg.Data[5] & 0xF8) >> 3;          // ABS左后轮速度
    //   abs_pm_can_life_2 = (can_msg.Data[6] & 0x20) >> 5 | (can_msg.Data[5] & 0x01) << 3;      // 心跳值2
    //   abs_pm_rr_spd = (can_msg.Data[6] & 0x1F) << 8 | (can_msg.Data[6] & 0xFF);               // ABS右后轮车速

    //   temp_abs_pm_fl_spd = abs_pm_fl_spd * 0.05625;
    //   temp_abs_pm_fr_spd = abs_pm_fr_spd * 0.05625;
    //   temp_abs_pm_rl_spd = abs_pm_rl_spd * 0.05625;
    //   temp_abs_pm_rr_spd = abs_pm_rr_spd * 0.05625;
    //   break;}
    // case (0x330): {
      
    //   abs_ft_abs = (can_msg.Data[0] >> 6) & 0x01;
    //   abs_sts_active = (can_msg.Data[0] >> 7) & 0x01; 
    //   abs_pm_veh_spd = (can_msg.Data[0] & 0x1F) << 8 | (can_msg.Data[1] & 0xFF); 
    //   abs_sts_veh_spd_valid = can_msg.Data[2] & 0x01;
    //   RoadMu = (can_msg.Data[2] >> 2) & 0x03;
      
    //   temp_abs_pm_veh_spd = abs_pm_veh_spd * 0.05625;
    //   std::cout << "abs_pm_veh_spd" << abs_pm_veh_spd << std::endl;
    //   msg.back_wheel_speed =  abs_pm_veh_spd; 

    //   break;}
    // case (0x510): {                                                                           // 电子驻车运行状态
    //   EPB_SystemStatus = (can_msg.Data[0] >> 5) & 0x07;                                       // EPB状态
    //   EPB_FAULT_LAMP = can_msg.Data[1] & 0x03;                                                // EPB故障指示灯
    //   break; }
    // case (0x613): {                                                                           // 整车故障信息
    //   vcu_ft_veh_brksys = (can_msg.Data[5] & 0x02) >> 1;                                      // 制动系统故障
    //   break; }
    case (0x614): {  
      std::cout << __LINE__ << std::endl;                                                                         // 整车能量信息
      vcu_pm_veh_spd = (can_msg.Data[0] & 0xFF) << 8 | can_msg.Data[1] & 0xFF;                // 整车车速值

      vcu_pm_veh_spd = vcu_pm_veh_spd * 0.1;
      std::cout << "vcu_pm_veh_spd" << vcu_pm_veh_spd << std::endl;
      break; }
    // case (0x618): {
    //   vcu_sts_dcu_handshake = (can_msg.Data[0] & 0x40) >> 6;                                  // DCU握手反馈信号
    //   gw_mcu_pm_otp_torq = (can_msg.Data[4] & 0xFF) << 8 | (can_msg.Data[5] & 0xFF);          // 电机扭矩

    //   temp_gw_mcu_pm_otp_torq = gw_mcu_pm_otp_torq * 0.1 - 2000.0; 
    //   break;}
    // case (0x619): {
    //   mcu_pm_max_drv_torq = (can_msg.Data[2] & 0xFF) << 8 | (can_msg.Data[3] & 0xFF);        // MCU最大允许驱动扭矩
    //   mcu_pm_max_regen_torq = (can_msg.Data[4] & 0xFF) << 8 | (can_msg.Data[5] & 0xFF);      // MCU最大允许发电扭矩

    //   temp_mcu_pm_max_drv_torq = mcu_pm_max_drv_torq * 0.1 - 2000.0; 
    //   temp_mcu_pm_max_regen_torq = mcu_pm_max_regen_torq * 0.1 - 2000.0; 
    //   break;}
    // case (0x661): {
    //   tbox_pm_time_hr = can_msg.Data[0] & 0xFF;                                               // 小时
    //   tbox_pm_time_mn = can_msg.Data[1] & 0xFF;                                               // 分钟
    //   tbox_pm_time_sc = can_msg.Data[2] & 0xFF;                                               // 秒
    //   tbox_pm_date_yy = can_msg.Data[4] & 0xFF;                                               // 年
    //   tbox_pm_date_mm = can_msg.Data[5] & 0xFF;                                               // 月
    //   tbox_pm_date_dd = can_msg.Data[6] & 0xFF;                                               // 日
    //   break;}
    // case (0x680): {
    //   ic_sts_safetybelt = (can_msg.Data[4] & 0x0C) >> 2;                                      // 安全带状态
    //   break;}
    // case (0x14E): {
    //   TBS_pub1_CheckSum = can_msg.Data[0] & 0xFF;                                             // 报文检验和
    //   TBS_RollingCounter = can_msg.Data[1] & 0x0F;                                            // 循环计数
    //   TBS_BrkPedlAppldFlg = can_msg.Data[2] & 0x01;                                           // 制动踏板踩下标志位
    //   TBS_BrkPedlAppldVld = (can_msg.Data[2] & 0x30) >> 4;                                    // 制动踏板踩下标志位的有效性
    //   TBS_OutputRodAct_1 = can_msg.Data[3] & 0xFF;                                            // 制动踏板位移信号
    //   TBS_Motor_TgtT = (can_msg.Data[4] & 0xFF) << 8 | (can_msg.Data[5] & 0xFF);              // 再生制动时，TBS目标电机回收扭矩
    //   TBS_Motor_TgtTQ = (can_msg.Data[6] & 0x01);                                             // 再生制动时，TBS_Motor_TgtT的有效性
    //   TBS_WarnOn = (can_msg.Data[6] & 0x10) >> 4;                                             // 需求仪表显示故障警示灯
    //   TBS_FailLevel = can_msg.Data[7] & 0x07;                                                 // TBS故障等级
    //   TBS_Boostlimitied = (can_msg.Data[7] & 0x10) >> 4;                                      // 助力是否受限
    //   TBS_Handshake = (can_msg.Data[7] & 0x20) >> 5;                                          // 握手反馈

    //   temp_TBS_OutputRodAct_1 = TBS_OutputRodAct_1 * 0.1;
    //   temp_TBS_Motor_TgtT = TBS_Motor_TgtT * 0.1 - 3276.7;
    //   break;}
    // case (0x14F): {
    //   TBS_pub2_CheckSum = can_msg.Data[0] & 0xFF;                                             // 报文检验和
    //   TBS_RollingCounter_2 = can_msg.Data[1] & 0x0F;                                          // 循环计数
    //   TBS_MCP = (can_msg.Data[2] & 0xFF) << 4 | (can_msg.Data[3] & 0xF0) >> 4;                // 主缸压力
    //   TBS_OutputRodAct_2 = (can_msg.Data[4] & 0xFF) << 8 | (can_msg.Data[5] & 0xFF);          // 主缸位移信号
    //   TBS_Prefill_Handshake = can_msg.Data[6] & 0x03;                                         // 预充握手信号
    //   TBS_EBRAcailable = (can_msg.Data[6] & 0x10) >> 4;                                       // EBR是否可用

    //   temp_TBS_MCP = TBS_MCP * 0.1;
    //   temp_TBS_OutputRodAct_2 = TBS_OutputRodAct_2 * 0.1;
    //   break;}
    // case (0x223): {
    //   eps_Torque = can_msg.Data[0] & 0xFF;                                                   // 扭矩信号
    //   eps_FaultCode = can_msg.Data[2] & 0xFF;                                                // 当前故障码
    //   eps_sts_angle_aligmidleFbk = can_msg.Data[3] & 0xFF;                                   // 角度对中结果反馈
    //   eps_SoftwareVersion = can_msg.Data[4] & 0xFF;                                          // 软件版本
    //   eps_HardwareVersion = can_msg.Data[5] & 0xFF;                                          // 硬件版本
    //   eps_steeringwheelcontrol = can_msg.Data[5] & 0x01;                                     // 驾驶员方向盘操控状态

    //   temp_eps_Torque = eps_Torque * 0.1;
    //   break;}
    // case (0x224): {
    //   eps_sts_angle_aligmidle = can_msg.Data[0] & 0xFF;                                      // 角度对中指令
    //   break;}
    default:
    {
    std::cout << "can_msg.ID" << can_msg.ID << std::endl;
      break;
    }
  }

#if Tack_CAN_LOG
msg.back_wheel_speed =  vcu_pm_veh_spd; 
msg.turn_angle = eps_pm_angle;
std::cout << "<===========publishing chissis signal:back_wheel_speed" << vcu_pm_veh_spd << std::endl;
std::cout << "<===========publishing chissis signal:eps_pm_angle" << eps_pm_angle << std::endl;
#endif
}

void SendDcu_14A(const int &acc_vaild, const float &long_acc, const float &target_acc_dec,const int &accdecShakeHands, VCI_CAN_OBJ &can_msg) {
  can_msg.ID = 0x14A;
  can_msg.SendType = 0;
  can_msg.RemoteFlag = 0;
  can_msg.ExternFlag = 0;
  can_msg.DataLen = 8;
  // 保压请求 0 "No Request"、1 "Request"
  DCU_EHBHydrMainReq = 0;
  // EHB握手请求 0 "No Request"、1 "Request"
  DCU_EHBHandshakeReq = accdecShakeHands;
  // 纵向加速度有效位
  LongitudinalACCValid = acc_vaild;
  // 目标加减速度
  if (target_acc_dec > 10.23) {
    DCU_EHBTargetAccDec = (unsigned int)((10.23 + 10.23) / 0.01);
  } else if (target_acc_dec < - 10.23) {
    DCU_EHBTargetAccDec = (unsigned int)((-10.23 + 10.23) / 0.01);
  } else {
    DCU_EHBTargetAccDec = (unsigned int)((target_acc_dec + 10.23) / 0.01);
  }
  
  // 预冲压请求 0 "No Request"、1 "Request"
  DCU_EHBPrefill = 0;
  // E2E保护
  Rollingcounter_1 = 0;
  // E2E保护       
  Checksum_1 = 0;  
  // 纵向加速度          
  LongitudinalACC = long_acc;  

  can_msg.Data[0] = (DCU_EHBHydrMainReq & 0x01) |
                    ((DCU_EHBHandshakeReq & 0x01) << 1) |
                    ((LongitudinalACCValid & 0x01) << 2);
  can_msg.Data[1] = (DCU_EHBTargetAccDec & 0x7F8) >> 3;                
  can_msg.Data[2] = (DCU_EHBTargetAccDec & 0x07) << 5;
  can_msg.Data[3] = (DCU_EHBPrefill & 0x01) << 4;
  can_msg.Data[4] = (Rollingcounter_1 & 0x0F);
  can_msg.Data[5] = (Checksum_1 & 0xFF);

  ///TODO:1.6 DBC 没有 LongitudinalACC这个值
}

void SendDcu_14B(const float &target_steer_angle, const int &is_request_steer_control,
                 const float &target_steer_angle_rate, const int &is_request_steer_rate_control,
                 const int &is_request_eps_hand, VCI_CAN_OBJ &can_msg,const int DCU_EPSWorkmodeReq) {  //modify by travis added ",const int DCU_EPSWorkmodeReq"
  
  can_msg.ID = 0x14B;
  can_msg.SendType = 0;
  can_msg.RemoteFlag = 0;
  can_msg.ExternFlag = 0;
  can_msg.DataLen = 8;

  // 方向盘转角请求 0 "No Request"、1 "Request"
  DCU_EPSSteerAngleReq = is_request_steer_control;
  // 方向盘转角（左正右负）[deg]
  ///TODO：接口是 rad??
  if (target_steer_angle > 780) {
    DCU_EPSSteerAngle = (unsigned int)((780 + 780) / 0.0345);
  } else if (target_steer_angle < -780) {
    DCU_EPSSteerAngle = (unsigned int)((-780 + 780) / 0.0345);
  } else {
    DCU_EPSSteerAngle = (unsigned int)((target_steer_angle + 780) / 0.0345); 
  }
  // 转角速度请求请求，预留 0 "No Request"、1 "Request"
  DCU_EPSSteerAngleSpdReq = is_request_steer_rate_control;  
  // 转角速度请求请求，预留 [deg/s]
  if (target_steer_angle_rate > 1023) {
    DCU_EPSSteerAngleSpd = (unsigned int)((1023 + 1024) / 0.125);
  } else if (target_steer_angle_rate < -1024) {
    DCU_EPSSteerAngleSpd = (unsigned int)((-1024 + 1024) / 0.125);
  } else {
    DCU_EPSSteerAngleSpd = (unsigned int)((target_steer_angle_rate + 1024) / 0.125); 
  }
  // EPS握手请求0 "No Request"、1 "Request"
  DCU_EPSHandshakeReq = is_request_eps_hand;
  // EPS工作模式请求 0x00：机械模式 0x10：助力模式 0x20：角度控制模式   
  // DCU_EPSWorkmodeReq = 0x20;  //mark by travis from 0x10 to 0x20 to control the angle

  can_msg.Data[0] = DCU_EPSSteerAngleReq & 0x01;
  can_msg.Data[1] = DCU_EPSSteerAngle >> 8;                
  can_msg.Data[2] = DCU_EPSSteerAngle & 0xFF;
  can_msg.Data[3] = DCU_EPSSteerAngleSpdReq & 0x01;
  can_msg.Data[4] = (DCU_EPSSteerAngleSpd >> 8) & 0x3F; 
  can_msg.Data[5] = DCU_EPSSteerAngleSpd & 0xFF;
  ///TODO：又与 1.6DBC不一致
  can_msg.Data[6] = DCU_EPSHandshakeReq & 0x01;
  can_msg.Data[7] = DCU_EPSWorkmodeReq;
  std::cout << "can_msg.data_0= " << static_cast<double>(can_msg.Data[0]) << 8 <<std::endl;
  std::cout << "can_msg.data_1= " << static_cast<double>(can_msg.Data[1]) <<std::endl;
  std::cout << "can_msg.data_2= " << static_cast<double>(can_msg.Data[2]) <<std::endl;
  std::cout << "can_msg.data_3= " << static_cast<double>(can_msg.Data[3]) <<std::endl;
  std::cout << "can_msg.data_4= " << static_cast<double>(can_msg.Data[4]) <<std::endl;
  std::cout << "can_msg.data_5= " << static_cast<double>(can_msg.Data[5]) <<std::endl;
  std::cout << "can_msg.data_6= " << static_cast<double>(can_msg.Data[6]) <<std::endl;
  std::cout << "can_msg.data_7= " << std::hex<< static_cast<double>(can_msg.Data[7]) <<std::endl;
}

void SendDcu_14C(VCI_CAN_OBJ &can_msg) {

  can_msg.ID = 0x14C;
  can_msg.SendType = 0;
  can_msg.RemoteFlag = 0;
  can_msg.ExternFlag = 0;
  can_msg.DataLen = 8;

  // EPB拉起请求 0 No Request"、 1 "Request"
  DCU_EPBReq = 0;
  // 转向灯请求 0 左转向灯请求、1 右转向灯请求、 2 未请求 、3 未定义
  DCU_BCMTurnLightReq = 2;          
  // 制动灯请求 0 No Request"、 1 "Request"
  DCU_BCMBrakeLightReq = 0;
  // 近光灯请求 0 No Request"、 1 "Request"
  DCU_BCMLowBeamReq = 0;
  // 双闪请求 0 No Request"、 1 "Request"
  DCU_BCMEmergFlash = 0;
  // E2E保护
  Rollingcounter_2 = 0;
  // E2E保护
  Checksum_2 = 0;     

  can_msg.Data[0] = (DCU_EPBReq & 0x01) |
                    (DCU_BCMTurnLightReq & 0x03) << 1 |
                    (DCU_BCMBrakeLightReq & 0x01) << 3 |
                    (DCU_BCMLowBeamReq & 0x01) << 4 |
                    (DCU_BCMEmergFlash & 0x01) << 5;

  can_msg.Data[1] = Rollingcounter_2 & 0x0F;                
  can_msg.Data[2] = Checksum_2 & 0xFF;
}


void SendDcu_14D(const float &target_torque, const int &is_request_torque_control,
                 const int &acc_state, VCI_CAN_OBJ &can_msg) {

  can_msg.ID = 0x14D;
  can_msg.SendType = 0;
  can_msg.RemoteFlag = 0;
  can_msg.ExternFlag = 0;
  can_msg.DataLen = 8;
  
  // 扭矩请求 0 "No Request"、1 "Request"
  DCU_VCUTorqReq = is_request_torque_control; 
  // 扭矩请求值 [N/m]
  DCU_VCUTorq = target_torque;
  if (target_torque > 1199) {
    DCU_VCUTorq = (unsigned int)((1199 + 848) / 0.5);
  } else if (target_torque < -848) {
    DCU_VCUTorq = (unsigned int)((-848 + 848) / 0.5);
  } else {
    DCU_VCUTorq = (unsigned int)((target_torque + 848) / 0.5); 
  }          
  // VCU握手请求 0 "No Request"、1 "Request"
  DCU_VCUHandshakeReq = 0;
  // 档位请求 0 "No request" 、1 "P（Reserved）"、2 "R"、3 "N"、4 "D"、5 "（Reserved）"、6 "（Reserved）"、7 "（Reserved）"
  DCU_GearReq = 0;
  // 驾驶员踩油门接管车辆状态 0 "No Override"、1 "Override"       
  DCU_DriverOverride = 0;    
  // E2E保护
  Rollingcounter_3 = 0;  
  // E2E保护     
  Checksum_3 = 0; 
  // ACC状态 0 "Off"、1 "Standby"、2 "Active" 、3 "Override"、4 "Standstill Active"、5 "Standstill Wait"、6 "Fault"、7 "Passive"
  DCU_ACCSt = acc_state;
  // DCU故障状态 0 "No Failure"、1 "Failure"               
  DCU_SystemFailure = 0;      
  // 目标加减速度请求 0 "No Request"、1 "Request"
  DCU_EHBTargetAccDecReq = 1; 

  can_msg.Data[0] = DCU_VCUTorqReq & 0x01;
  can_msg.Data[1] = (DCU_VCUTorq >> 8) & 0x0F;                
  can_msg.Data[2] = DCU_VCUTorq & 0xFF;
  can_msg.Data[3] = (DCU_VCUHandshakeReq & 0x01) |
                    (DCU_GearReq & 0x01) << 1 |
                    (DCU_GearReq & 0x07) << 2 |
                    (DCU_DriverOverride & 0x01) << 5;

  can_msg.Data[4] = Rollingcounter_3 & 0x0F; 
  can_msg.Data[5] = Checksum_3 & 0xFF;
  ///TODO：又与 1.6DBC不一致
  can_msg.Data[6] = (DCU_ACCSt & 0x07) |
                    (DCU_SystemFailure & 0x01) << 3 |
                    (DCU_EHBTargetAccDecReq & 0x01) << 4;

}

void SendControlCommondToChassis(const chassic_cmd &control_command) {
  VCI_CAN_OBJ send_0x14A;
  VCI_CAN_OBJ send_0x14B;
  VCI_CAN_OBJ send_0x14C;
  VCI_CAN_OBJ send_0x14D;

  SendDcu_14A(control_command.acc_vaild, control_command.long_acc, control_command.target_acc_dec,1, send_0x14A);


  // SendDcu_14B(control_command.target_steer_angle, control_command.is_request_steer_control,
  //             control_command.target_steer_angle_rate, control_command.is_request_steer_rate_control, control_command.is_request_eps_hand,
  //             send_0x14B,0x20);
  float target_angle = control_command.target_steer_angle;
  // temporary begin
  
  SendDcu_14B(control_command.target_steer_angle, control_command.is_request_steer_control,
              control_command.target_steer_angle_rate, control_command.is_request_steer_rate_control, control_command.is_request_eps_hand,
              send_0x14B,0x20);

  // temporary end
  SendDcu_14C(send_0x14C);
  SendDcu_14D(control_command.target_torque, control_command.is_request_torque_control, control_command.acc_state,send_0x14D);

  if(VCI_Transmit(VCI_USBCAN2, 0, 0, &send_0x14A, 1) == 1) {
    printf("send_0x14A TX data successful!\n");
  }

  if(VCI_Transmit(VCI_USBCAN2, 0, 0, &send_0x14B, 1) == 1) {
    printf("send_0x14B TX data successful!\n");
  }

  if(VCI_Transmit(VCI_USBCAN2, 0, 0, &send_0x14C, 1) == 1) {
    printf("send_0x14C TX data successful!\n");
  }

  if(VCI_Transmit(VCI_USBCAN2, 0, 0, &send_0x14D, 1) == 1) {
    printf("send_0x14D TX data successful!\n");
  }
}

#endif


//速度控制回调
void cmd_velCallback(const geometry_msgs::Twist &twist_aux) {
#if IS_AKM
  SendSpeedAndAngleToAKM(twist_aux.linear.x,twist_aux.linear.y);
  int temp_gear = (int)twist_aux.linear.z;
  SetGearToAKM(temp_gear);
#else
  ///TODO:ros_msg需要映射至cmd中
  chassic_cmd cmd;
  cmd.target_steer_angle = twist_aux.angular.z;
  cmd.is_request_eps_hand = 1;
  cmd.is_request_steer_control = 1;
  cmd.target_steer_angle_rate = 1;   //try to modify
  cmd.is_request_steer_rate_control = 1;
  isWriteNow = true;

  cmd.target_acc_dec = twist_aux.linear.x;  //加减速度值
  std::cout << "<=============cmd.target_acc_dec = " << cmd.target_acc_dec << std::endl;
  SendControlCommondToChassis(cmd);
  isWriteNow = false;
#endif
}

int main(int argc, char **argv) {
  int i = 0;
  cartop_msgs::carTop msg;
  signal(SIGINT, signalHandler); //add by travis 注册信号函数
  //指示程序已运行
  printf(">>this is hello !\r\n");

  //打开设备
  int num = VCI_OpenDevice(VCI_USBCAN2,0,0);
  if(num == 0 || num == 1) {
    //打开设备成功
    printf(">>open deivce success!\n");
  } else {
    printf(">>open deivce error %d!\n",num);
    exit(1);
  }

  //读取设备序列号、版本等信息。
  if(VCI_ReadBoardInfo(VCI_USBCAN2, 0, &pInfo) ==1) {
    printf(">>Get VCI_ReadBoardInfo success!\n");
  } else {
    printf(">>Get VCI_ReadBoardInfo error!\n");
    exit(1);
  }

  //初始化参数，严格参数二次开发函数库说明书。
  VCI_INIT_CONFIG config;
  config.AccCode = 0;
  config.AccMask = 0xFFFFFFFF; //FFFFFFFF全部接收
  config.Filter = 2;           //接收所有帧  2-只接受标准帧  3-只接受扩展帧
  config.Timing0 = 0x00;       /*波特率500 Kbps  0x00  0x1C*/
  config.Timing1 = 0x1C;
  config.Mode = 0;             //正常模式

  if(VCI_InitCAN(VCI_USBCAN2,0,0,&config)!=1) {
    printf(">>Init CAN1 error\n");
    VCI_CloseDevice(VCI_USBCAN2,0);
  }

  if(VCI_StartCAN(VCI_USBCAN2,0,0)!=1) {
    printf(">>Start CAN1 error\n");
    VCI_CloseDevice(VCI_USBCAN2,0);
  }

  //需要读取的帧，结构体设置
  VCI_CAN_OBJ rev;
  rev.SendType = 0;
  rev.RemoteFlag = 0;
  rev.ExternFlag = 0;
  rev.DataLen = 8;

  int m_run0=1;
  pthread_t threadid;
  int ret;
  //ret=pthread_create(&threadid,NULL,receive_func,&m_run0);
  int times = 5;

  ros::init(argc, argv, "apollo_cartop");
  ros::NodeHandle n;
  ros::Subscriber cmd_vel_sub = n.subscribe("/cmd_vel", 100, cmd_velCallback);//速度回调
  ros::Publisher car_pub = n.advertise<cartop_msgs::carTop>("/car_message", 100);   //自定义消息发布
  ros::Rate loop_rate(50);
  
  while (ros::ok()) {
    //读取数据
    if (isWriteNow) break;
    if(VCI_Receive(VCI_USBCAN2, 0, 0, &rev, 2500, 0)>0) {
      std::cout << std::dec << __LINE__ <<  "into VCI_Receive " << std::endl;
      //can发送 02 代表接收的数据
#if IS_AKM
      GetInfoFromAKM(rev, msg);
#else
      GetInfoFromChassic(rev, msg);
#endif
    }
    car_pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }
  VCI_CloseDevice(VCI_USBCAN2,0);//关闭设备。
  return 0;
}
