
// #include <ros/ros.h>
// #include <geometry_msgs/Twist.h>
// #include <dynamic_reconfigure/server.h>
// #include "cartop_msgs/carTop.h"//自定义消息类型
// #include "apollo_cartop/cartopConfig.h"

// double current_angle = 0.0;
// class CallbackWrapper {
//  public:
//   void operator()(apollo_cartop::cartopConfig& config, uint32_t level) {
//     twist_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
//     sub_chassis_now_ = nh_.subscribe("car_message", 100, &CallbackWrapper::SubChassisNowCallback);
//     std::cout << __LINE__ << std::endl;
//     nh_.param<double>("execution_time", execution_time_, 1.0);
//     remaining_angle_ = config.angle-current_angle;
//     step_angle_ = remaining_angle_ / (execution_time_ * 10); // 将总角度划分为100个部分
//     current_angle_ = current_angle;
//     timer_ = nh_.createTimer(ros::Duration(execution_time_ / 10), &CallbackWrapper::sendAngleCallback, this); // 每隔100ms触发一次sendAngleCallback函数
//     std::cout << __LINE__ << std::endl;
//   };
//     static void SubChassisNowCallback(const cartop_msgs::carTop::ConstPtr& ChassisSignal)
//   {
//     current_angle = ChassisSignal->turn_angle;
//     std::cout << "ChassisSignal = " << ChassisSignal->turn_angle << std::endl;
//   }
//  public:
//   geometry_msgs::Twist twist_;
//   ros::NodeHandle nh_;
//   ros::Publisher twist_pub_;
//   ros::Subscriber sub_chassis_now_;
//   double total_angle_;
//   double remaining_angle_;
//   double step_angle_;
//   double execution_time_;
//   double current_angle_;
//   ros::Timer timer_;

//   void sendAngleCallback(const ros::TimerEvent& event) {
//     if (remaining_angle_ > 3) {
//       double send_angle = std::min(step_angle_, remaining_angle_); // 计算本次发送的角度
//       current_angle_ += send_angle;
//       std::cout << __LINE__ << std::endl;
//       // 构造发送命令
//       twist_.angular.z = current_angle_;
//       twist_pub_.publish(twist_);

//       ROS_INFO_STREAM("--------------------------------");
//       ROS_INFO_STREAM("publish once speed is: " << twist_.linear.x << " m/s");
//       ROS_INFO_STREAM("publish once angle is: " << twist_.angular.z << " degree");
//     } else {
//       timer_.stop(); // 停止定时器
//     }
//   }
// };


// int main(int argc, char** argv) {
//   ros::init(argc, argv, "cartop_control");
//   dynamic_reconfigure::Server<apollo_cartop::cartopConfig> server;
//   dynamic_reconfigure::Server<apollo_cartop::cartopConfig>::CallbackType f;
//   CallbackWrapper twist_msg;
//   f = boost::bind(&CallbackWrapper::operator(), &twist_msg, _1, _2);
//   server.setCallback(f);
//   ros::spin();
//   return 0;
// }

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <dynamic_reconfigure/server.h>
#include "cartop_msgs/carTop.h"//自定义消息类型
#include "apollo_cartop/cartopConfig.h"//自定义配置

class CallbackWrapper {
public:
  void operator()(apollo_cartop::cartopConfig& config, uint32_t level) {
    twist_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
    sub_chassis_now_ = nh_.subscribe("car_message", 100, &CallbackWrapper::SubChassisNowCallback);
    // config.speed是m/s, config.angle是角度
    twist_.linear.x = config.targetAccDec;
    twist_.angular.z = config.angle;
    twist_pub_.publish(twist_);
    ROS_INFO_STREAM("--------------------------------");
    ROS_INFO_STREAM("publish once targetAccDec is : " << twist_.linear.x << " m/s");
    
    ROS_INFO_STREAM("publish once angle is : " << twist_.angular.z << " degree");
  }
  
  static void SubChassisNowCallback(const cartop_msgs::carTop::ConstPtr& ChassisSignal)
  {
    // std::cout << "ChassisSignal = " << ChassisSignal->turn_angle << std::endl;
  }
  
public:
  geometry_msgs::Twist twist_;
  ros::NodeHandle nh_;
  ros::Publisher twist_pub_;
  ros::Subscriber sub_chassis_now_;
};

int main(int argc, char** argv) {
  ros::init(argc, argv, "cartop_control");
  
  CallbackWrapper twist_msg;
  
  
  dynamic_reconfigure::Server<apollo_cartop::cartopConfig> server;
  dynamic_reconfigure::Server<apollo_cartop::cartopConfig>::CallbackType f;
  f = boost::bind(&CallbackWrapper::operator(), &twist_msg, _1, _2);
  server.setCallback(f);
  
  ros::spin();
  
  return 0;
}

// #include <ros/ros.h>
// #include <geometry_msgs/Twist.h>
// #include <dynamic_reconfigure/server.h>
// #include "cartop_msgs/carTop.h" //自定义消息类型
// #include "apollo_cartop/cartopConfig.h" //自定义配置

// class CallbackWrapper {
// public:
//   CallbackWrapper() : nh_("~"), target_angle_(0.0), current_angle_(0.0), remaining_time_(0.0) {
//     twist_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
// //     sub_chassis_now_ = nh_.subscribe("car_message", 100, &CallbackWrapper::SubChassisNowCallback);
//     timer_ = nh_.createTimer(ros::Duration(0.1), &CallbackWrapper::timerCallback, this);
//   }

//   void operator()(apollo_cartop::cartopConfig& config, uint32_t level) {
//     target_angle_ = config.angle;
//     remaining_time_ = 1.0;
//     ROS_INFO_STREAM("--------------------------------");
//     ROS_INFO_STREAM("Received target angle: " << target_angle_ << " degree");
//   }

//   static void SubChassisNowCallback(const cartop_msgs::carTop::ConstPtr& ChassisSignal, CallbackWrapper* wrapper) {
//     wrapper->current_angle_ = ChassisSignal->turn_angle;
//   }

//   void timerCallback(const ros::TimerEvent& event) {
//     if (remaining_time_ > 0) {
//       double angle_diff = target_angle_ - current_angle_;
//       double step = angle_diff / (remaining_time_ * 10);
//       current_angle_ += step;
//       remaining_time_ -= 0.1;

//       // 构造发送命令
//       geometry_msgs::Twist twist;
//       twist.angular.z = current_angle_;
//       twist_pub_.publish(twist);

//       ROS_INFO_STREAM("--------------------------------");
//       ROS_INFO_STREAM("Publishing angle command: " << twist.angular.z << " degree");
//     } else {
//       // 检查是否需要纠正角度
//       double angle_diff = target_angle_ - current_angle_;
//       if (std::abs(angle_diff) > 0.1) {
//         // 发送最终角度指令
//         geometry_msgs::Twist twist;
//         twist.angular.z = target_angle_;
//         twist_pub_.publish(twist);

//         ROS_INFO_STREAM("--------------------------------");
//         ROS_INFO_STREAM("Publishing final angle command: " << twist.angular.z << " degree");
//       }

//       // 停止定时器
//       timer_.stop();
//     }
//   }

// private:
//   ros::NodeHandle nh_;
//   ros::Publisher twist_pub_;
//   ros::Subscriber sub_chassis_now_;
//   ros::Timer timer_;
//   double target_angle_;
//   double current_angle_;
//   double remaining_time_;
// };

// int main(int argc, char** argv) {
//   ros::init(argc, argv, "cartop_control");
//   CallbackWrapper twist_msg;

//   dynamic_reconfigure::Server<apollo_cartop::cartopConfig> server;
//   dynamic_reconfigure::Server<apollo_cartop::cartopConfig>::CallbackType f;
//   f = boost::bind(&CallbackWrapper::operator(), &twist_msg, _1, _2);
//   server.setCallback(f);

//   ros::spin();

//   return 0;
// }