/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>
#include <string>

#include "apollo_msgs/chassis_msgs/chassis.pb.h"
#include "apollo_msgs/control_msgs/control_cmd.pb.h"
#include "apollo_msgs/control_msgs/control_conf.pb.h"
#include "apollo_msgs/control_msgs/pad_msg.pb.h"
#include "apollo_msgs/localization_msgs/localization.pb.h"
#include "apollo_msgs/planning_msgs/planning.pb.h"
#include "apollo_msgs/control_msgs/preprocessor.pb.h"
#include "apollo_common/component.h"
#include "apollo_common/time/time.h"
#include "apollo_common/monitor_log/monitor_log_buffer.h"
#include "apollo_common/util/util.h"
#include "apollo_control/common/dependency_injector.h"
#include "apollo_control/controller/controller_agent.h"

/**
 * @namespace apollo::control
 * @brief apollo::control
 */
namespace apollo {
namespace control {

/**
 * @class Control
 *
 * @brief control module main class, it processes localization, chassis, and
 * pad data to compute throttle, brake and steer values.
 */
class ControlComponent final
    : public apollo::common::Component {
        
 public:
  ControlComponent();

  std::string Name() const override;

  bool Init() override;

  void Proc(const ros::TimerEvent &);

 private:
  // Upon receiving pad message
  void OnPad(const std_msgs::String::ConstPtr &pad_msg);

  void OnChassis(const std_msgs::String::ConstPtr &chassis_msg);

  void OnLocalization(
      const std_msgs::String::ConstPtr &localization_msg);

  void OnPlanning(
      const std_msgs::String::ConstPtr &trajectory_msg);

  // Upon receiving monitor message
  void OnMonitor(
      const apollo::common::monitor::MonitorMessage &monitor_message);

  common::Status ProduceControlCommand(ControlCommand *control_command);
  common::Status CheckInput(LocalView *local_view);
  common::Status CheckTimestamp(const LocalView &local_view);
  common::Status CheckPad();
  void ResetAndProduceZeroControlCommand(ControlCommand *control_command);

 private:
  apollo::common::time::Time init_time_;

  
  std::shared_ptr<localization::LocalizationEstimate> latest_localization_;
  std::shared_ptr<canbus::Chassis> latest_chassis_;
  std::shared_ptr<planning::ADCTrajectory> latest_trajectory_;
  std::shared_ptr<PadMessage> pad_msg_;
  common::Header latest_replan_trajectory_header_;

  ControllerAgent controller_agent_;

  bool estop_ = false;
  std::string estop_reason_;
  bool pad_received_ = false;

  unsigned int status_lost_ = 0;
  unsigned int status_sanity_check_failed_ = 0;
  unsigned int total_status_lost_ = 0;
  unsigned int total_status_sanity_check_failed_ = 0;

  ControlConf control_conf_;

  std::mutex mutex_;

  ros::Timer proc_timer_;
  ros::Subscriber chassis_reader_;
  ros::Subscriber pad_msg_reader_;
  ros::Subscriber localization_reader_;
  ros::Subscriber trajectory_reader_;
  ros::Publisher control_cmd_writer_;

  common::monitor::MonitorLogBuffer monitor_logger_buffer_;

  LocalView local_view_;

  std::shared_ptr<DependencyInjector> injector_;
};

}  // namespace control
}  // namespace apollo
