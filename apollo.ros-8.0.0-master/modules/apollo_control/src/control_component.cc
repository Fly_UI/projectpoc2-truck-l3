/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_control/control_component.h"

#include "absl/strings/str_cat.h"
#include "apollo_common/file.h"
#include "apollo_common/log.h"
#include "apollo_common/time/clock.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_common/vehicle_state/vehicle_state_provider.h"
#include "apollo_control/common/control_gflags.h"

namespace apollo {
namespace control {

using apollo::canbus::Chassis;
using apollo::common::ErrorCode;
using apollo::common::Status;
using apollo::common::VehicleStateProvider;
using apollo::common::time::Clock;
using apollo::localization::LocalizationEstimate;
using apollo::planning::ADCTrajectory;

ControlComponent::ControlComponent()
    : monitor_logger_buffer_(common::monitor::MonitorMessageItem::CONTROL) {}

std::string ControlComponent::Name() const { return FLAGS_control_node_name; }

bool ControlComponent::Init() {
  injector_ = std::make_shared<DependencyInjector>();
  init_time_ = Clock::Now();

  AINFO << "Control init, starting ...";

  ACHECK(
      apollo::common::GetProtoFromFile(FLAGS_control_conf_file, &control_conf_))
      << "Unable to load control conf file: " + FLAGS_control_conf_file;

  AINFO << "Conf file: " << FLAGS_control_conf_file << " is loaded.";

  // initial controller agent
  if (!controller_agent_.Init(injector_, &control_conf_).ok()) {
    // set controller
    ADEBUG << "original control";
    monitor_logger_buffer_.ERROR("Control init controller failed! Stopping...");
    return false;
  }

  ros::NodeHandle nh;

  chassis_reader_ = nh.subscribe(FLAGS_chassis_topic, FLAGS_chassis_pending_queue_size, 
      &ControlComponent::OnChassis, this);
  ACHECK(chassis_reader_ != nullptr);

  trajectory_reader_ = nh.subscribe(FLAGS_planning_trajectory_topic, FLAGS_planning_pending_queue_size, 
      &ControlComponent::OnPlanning, this);
  ACHECK(trajectory_reader_ != nullptr);

  localization_reader_ = nh.subscribe(FLAGS_localization_topic, FLAGS_localization_pending_queue_size, 
      &ControlComponent::OnLocalization, this);
  ACHECK(localization_reader_ != nullptr);

  pad_msg_reader_ = nh.subscribe(FLAGS_pad_topic, FLAGS_pad_msg_pending_queue_size, 
      &ControlComponent::OnPad, this);
  ACHECK(pad_msg_reader_ != nullptr);

  control_cmd_writer_ = nh.advertise<std_msgs::String>(FLAGS_control_command_topic, 1);
  ACHECK(control_cmd_writer_ != nullptr);
  
  // set initial vehicle state by cmd
  // need to sleep, because advertised channel is not ready immediately
  // simple test shows a short delay of 80 ms or so
  AINFO << "Control resetting vehicle state, sleeping for 1000 ms ...";
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  // should init_vehicle first, let car enter work status, then use status msg
  // trigger control

  AINFO << "Control default driving action is "
        << DrivingAction_Name(control_conf_.action());
  if (pad_msg_ == nullptr) {
    pad_msg_ = std::make_shared<PadMessage>();
  }
  pad_msg_->set_action(control_conf_.action());

  proc_timer_ = nh.createTimer(ros::Duration(control_conf_.control_period()), 
      &ControlComponent::Proc, this);

  return true;
}

void ControlComponent::OnPad(const std_msgs::String::ConstPtr &pad_msg) {
  auto pad = std::make_shared<PadMessage>();
  if (!pad->ParseFromString(pad_msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  std::lock_guard<std::mutex> lock(mutex_);
  if (pad_msg_ == nullptr) {
    pad_msg_ = std::make_shared<PadMessage>();
  }
  pad_msg_->CopyFrom(*pad);
  ADEBUG << "Received Pad Msg:" << pad_msg_->DebugString();
  AERROR_IF(!pad_msg_->has_action()) << "pad message check failed!";
}

void ControlComponent::OnChassis(const std_msgs::String::ConstPtr &chassis_msg) {
  auto chassis = std::make_shared<Chassis>();
  if (!chassis->ParseFromString(chassis_msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  std::lock_guard<std::mutex> lock(mutex_);
  if (latest_chassis_ == nullptr) {
    latest_chassis_ = std::make_shared<Chassis>();
  }
  latest_chassis_->CopyFrom(*chassis);
}

void ControlComponent::OnPlanning(
    const std_msgs::String::ConstPtr &trajectory_msg) {
  auto trajectory = std::make_shared<ADCTrajectory>();
  if (!trajectory->ParseFromString(trajectory_msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  std::lock_guard<std::mutex> lock(mutex_);
  if (latest_trajectory_ == nullptr) {
    latest_trajectory_ = std::make_shared<ADCTrajectory>();
  }
  latest_trajectory_->CopyFrom(*trajectory);
}

void ControlComponent::OnLocalization(
    const std_msgs::String::ConstPtr &localization_msg) {
  auto localization = std::make_shared<LocalizationEstimate>();
  if (!localization->ParseFromString(localization_msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  std::lock_guard<std::mutex> lock(mutex_);
  if (latest_localization_ == nullptr) {
    latest_localization_ = std::make_shared<LocalizationEstimate>();
  }
  latest_localization_->CopyFrom(*localization);
}

void ControlComponent::OnMonitor(
    const common::monitor::MonitorMessage &monitor_message) {
  for (const auto &item : monitor_message.item()) {
    if (item.log_level() == common::monitor::MonitorMessageItem::FATAL) {
      estop_ = true;
      return;
    }
  }
}

Status ControlComponent::ProduceControlCommand(
    ControlCommand *control_command) {
  Status status = CheckInput(&local_view_);
  // check data
  if (!status.ok()) {
    AERROR_EVERY(100) << "Control input data failed: "
                      << status.error_message();
    control_command->mutable_engage_advice()->set_advice(
        apollo::common::EngageAdvice::DISALLOW_ENGAGE);
    control_command->mutable_engage_advice()->set_reason(
        status.error_message());
    estop_ = true;
    estop_reason_ = status.error_message();
  } else {
    Status status_ts = CheckTimestamp(local_view_);
    if (!status_ts.ok()) {
      AERROR << "Input messages timeout";
      // Clear trajectory data to make control stop if no data received again
      // next cycle.
      status = status_ts;
      if (local_view_.chassis().driving_mode() !=
          apollo::canbus::Chassis::COMPLETE_AUTO_DRIVE) {
        control_command->mutable_engage_advice()->set_advice(
            apollo::common::EngageAdvice::DISALLOW_ENGAGE);
        control_command->mutable_engage_advice()->set_reason(
            status.error_message());
      }
    } else {
      control_command->mutable_engage_advice()->set_advice(
          apollo::common::EngageAdvice::READY_TO_ENGAGE);
    }
  }

  // check estop
  estop_ = control_conf_.enable_persistent_estop()
               ? estop_ || local_view_.trajectory().estop().is_estop()
               : local_view_.trajectory().estop().is_estop();

  if (local_view_.trajectory().estop().is_estop()) {
    estop_ = true;
    estop_reason_ = "estop from planning : ";
    estop_reason_ += local_view_.trajectory().estop().reason();
  }

  if (local_view_.trajectory().trajectory_point().empty()) {
    AWARN_EVERY(100) << "planning has no trajectory point. ";
    estop_ = true;
    estop_reason_ = "estop for empty planning trajectory, planning headers: " +
                    local_view_.trajectory().header().ShortDebugString();
  }

  if (FLAGS_enable_gear_drive_negative_speed_protection) {
    const double kEpsilon = 0.001;
    auto first_trajectory_point = local_view_.trajectory().trajectory_point(0);
    if (local_view_.chassis().gear_location() == Chassis::GEAR_DRIVE &&
        first_trajectory_point.v() < -1 * kEpsilon) {
      estop_ = true;
      estop_reason_ = "estop for negative speed when gear_drive";
    }
  }

  if (!estop_) {
    if (local_view_.chassis().driving_mode() == Chassis::COMPLETE_MANUAL) {
      controller_agent_.Reset();
      AINFO_EVERY(100) << "Reset Controllers in Manual Mode";
    }

    auto debug = control_command->mutable_debug()->mutable_input_debug();
    debug->mutable_localization_header()->CopyFrom(
        local_view_.localization().header());
    debug->mutable_canbus_header()->CopyFrom(local_view_.chassis().header());
    debug->mutable_trajectory_header()->CopyFrom(
        local_view_.trajectory().header());

    if (local_view_.trajectory().is_replan()) {
      latest_replan_trajectory_header_ = local_view_.trajectory().header();
    }

    if (latest_replan_trajectory_header_.has_sequence_num()) {
      debug->mutable_latest_replan_trajectory_header()->CopyFrom(
          latest_replan_trajectory_header_);
    }
    // controller agent
    Status status_compute = controller_agent_.ComputeControlCommand(
        &local_view_.localization(), &local_view_.chassis(),
        &local_view_.trajectory(), control_command);

    if (!status_compute.ok()) {
      AERROR << "Control main function failed"
             << " with localization: "
             << local_view_.localization().ShortDebugString()
             << " with chassis: " << local_view_.chassis().ShortDebugString()
             << " with trajectory: "
             << local_view_.trajectory().ShortDebugString()
             << " with cmd: " << control_command->ShortDebugString()
             << " status:" << status_compute.error_message();
      estop_ = true;
      estop_reason_ = status_compute.error_message();
      status = status_compute;
    }
  }
  // if planning set estop, then no control process triggered
  if (estop_) {
    AWARN_EVERY(100) << "Estop triggered! No control core method executed!";
    // set Estop command
    control_command->set_speed(0);
    control_command->set_throttle(0);
    control_command->set_brake(control_conf_.soft_estop_brake());
    control_command->set_gear_location(Chassis::GEAR_DRIVE);
  }
  // check signal
  if (local_view_.trajectory().decision().has_vehicle_signal()) {
    control_command->mutable_signal()->CopyFrom(
        local_view_.trajectory().decision().vehicle_signal());
  }
  return status;
}

void ControlComponent::Proc(const ros::TimerEvent &) {
  const auto start_time = Clock::Now();

  if (latest_chassis_ == nullptr) {
    AERROR << "Chassis msg is not ready!";
    return;
  }

  if (latest_localization_ == nullptr) {
    AERROR << "localization msg is not ready!";
    return;
  }

  if (latest_trajectory_ == nullptr) {
    AERROR << "planning msg is not ready!";
    return;
  }

  {
    // TODO(SHU): to avoid redundent copy
    std::lock_guard<std::mutex> lock(mutex_);
    local_view_.mutable_chassis()->CopyFrom(*latest_chassis_);
    local_view_.mutable_trajectory()->CopyFrom(*latest_trajectory_);
    local_view_.mutable_localization()->CopyFrom(*latest_localization_);
    if (pad_msg_ != nullptr) {
      local_view_.mutable_pad_msg()->CopyFrom(*pad_msg_);
    }
  }

  if (pad_msg_ != nullptr) {
    ADEBUG << "pad_msg: " << pad_msg_->ShortDebugString();
    if (pad_msg_->action() == DrivingAction::RESET) {
      AINFO << "Control received RESET action!";
      estop_ = false;
      estop_reason_.clear();
    }
    pad_received_ = true;
  }

  if (control_conf_.is_control_test_mode() &&
      control_conf_.control_test_duration() > 0 &&
      (start_time - init_time_).ToSecond() >
          control_conf_.control_test_duration()) {
    AERROR << "Control finished testing. exit";
    return;
  }

  ControlCommand control_command;

  Status status;
  if (local_view_.chassis().driving_mode() ==
      apollo::canbus::Chassis::COMPLETE_AUTO_DRIVE) {
    status = ProduceControlCommand(&control_command);
  } else {
    ResetAndProduceZeroControlCommand(&control_command);
  }

  AERROR_IF(!status.ok()) << "Failed to produce control command:"
                          << status.error_message();

  if (pad_received_) {
    control_command.mutable_pad_msg()->CopyFrom(*pad_msg_);
    pad_received_ = false;
  }

  // forward estop reason among following control frames.
  if (estop_) {
    control_command.mutable_header()->mutable_status()->set_msg(estop_reason_);
  }

  // set header
  control_command.mutable_header()->set_lidar_timestamp(
      local_view_.trajectory().header().lidar_timestamp());
  control_command.mutable_header()->set_camera_timestamp(
      local_view_.trajectory().header().camera_timestamp());
  control_command.mutable_header()->set_radar_timestamp(
      local_view_.trajectory().header().radar_timestamp());

  common::util::FillHeader(Name(), &control_command);

  ADEBUG << control_command.ShortDebugString();
  if (control_conf_.is_control_test_mode()) {
    ADEBUG << "Skip publish control command in test mode";
    return;
  }

  const auto end_time = Clock::Now();
  const double time_diff_ms = (end_time - start_time).ToSecond() * 1e3;
  ADEBUG << "total control time spend: " << time_diff_ms << " ms.";

  control_command.mutable_latency_stats()->set_total_time_ms(time_diff_ms);
  control_command.mutable_latency_stats()->set_total_time_exceeded(
      time_diff_ms > control_conf_.control_period() * 1e3);
  ADEBUG << "control cycle time is: " << time_diff_ms << " ms.";
  status.Save(control_command.mutable_header()->mutable_status());

  std_msgs::String control_cmd_msg;
  control_cmd_msg.data = control_command.SerializeAsString();
  control_cmd_writer_.publish(control_cmd_msg); 
}

Status ControlComponent::CheckInput(LocalView *local_view) {
  ADEBUG << "Received localization:"
         << local_view->localization().ShortDebugString();
  ADEBUG << "Received chassis:" << local_view->chassis().ShortDebugString();

  if (!local_view->trajectory().estop().is_estop() &&
      local_view->trajectory().trajectory_point().empty()) {
    AWARN_EVERY(100) << "planning has no trajectory point. ";
    const std::string msg =
        absl::StrCat("planning has no trajectory point. planning_seq_num:",
                     local_view->trajectory().header().sequence_num());
    return Status(ErrorCode::CONTROL_COMPUTE_ERROR, msg);
  }

  for (auto &trajectory_point :
       *local_view->mutable_trajectory()->mutable_trajectory_point()) {
    if (std::abs(trajectory_point.v()) <
            control_conf_.minimum_speed_resolution() &&
        std::abs(trajectory_point.a()) <
            control_conf_.max_acceleration_when_stopped()) {
      trajectory_point.set_v(0.0);
      trajectory_point.set_a(0.0);
    }
  }

  injector_->vehicle_state()->Update(local_view->localization(),
                                     local_view->chassis());

  return Status::OK();
}

Status ControlComponent::CheckTimestamp(const LocalView &local_view) {
  if (!control_conf_.enable_input_timestamp_check() ||
      control_conf_.is_control_test_mode()) {
    ADEBUG << "Skip input timestamp check by gflags.";
    return Status::OK();
  }
  double current_timestamp = Clock::NowInSeconds();
  double localization_diff =
      current_timestamp - local_view.localization().header().timestamp_sec();
  if (localization_diff > (control_conf_.max_localization_miss_num() *
                           control_conf_.localization_period())) {
    AERROR << "Localization msg lost for " << std::setprecision(6)
           << localization_diff << "s";
    monitor_logger_buffer_.ERROR("Localization msg lost");
    return Status(ErrorCode::CONTROL_COMPUTE_ERROR, "Localization msg timeout");
  }

  double chassis_diff =
      current_timestamp - local_view.chassis().header().timestamp_sec();
  if (chassis_diff >
      (control_conf_.max_chassis_miss_num() * control_conf_.chassis_period())) {
    AERROR << "Chassis msg lost for " << std::setprecision(6) << chassis_diff
           << "s";
    monitor_logger_buffer_.ERROR("Chassis msg lost");
    return Status(ErrorCode::CONTROL_COMPUTE_ERROR, "Chassis msg timeout");
  }

  double trajectory_diff =
      current_timestamp - local_view.trajectory().header().timestamp_sec();
  if (trajectory_diff > (control_conf_.max_planning_miss_num() *
                         control_conf_.trajectory_period())) {
    AERROR << "Trajectory msg lost for " << std::setprecision(6)
           << trajectory_diff << "s";
    monitor_logger_buffer_.ERROR("Trajectory msg lost");
    return Status(ErrorCode::CONTROL_COMPUTE_ERROR, "Trajectory msg timeout");
  }
  return Status::OK();
}

void ControlComponent::ResetAndProduceZeroControlCommand(
    ControlCommand *control_command) {
  control_command->set_throttle(0.0);
  control_command->set_steering_target(0.0);
  control_command->set_steering_rate(0.0);
  control_command->set_speed(0.0);
  control_command->set_brake(0.0);
  control_command->set_gear_location(Chassis::GEAR_DRIVE);
  controller_agent_.Reset();
  latest_trajectory_->mutable_trajectory_point()->Clear();
  latest_trajectory_->mutable_path_point()->Clear();
}

}  // namespace control
}  // namespace apollo

APOLLO_COMPONENT(apollo::control::ControlComponent);
