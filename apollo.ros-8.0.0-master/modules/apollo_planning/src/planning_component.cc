/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include "apollo_planning/planning_component.h"

#include "apollo_common/file.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_common/configs/config_gflags.h"
#include "apollo_common/util/message_util.h"
#include "apollo_common/util/util.h"
#include "apollo_map/hdmap/hdmap_util.h"
#include "apollo_map/pnc_map/pnc_map.h"
#include "apollo_planning/common/history.h"
#include "apollo_planning/common/planning_context.h"
#include "apollo_planning/navi_planning.h"
#include "apollo_planning/on_lane_planning.h"

namespace apollo {
namespace planning {

using apollo::hdmap::HDMapUtil;
using apollo::perception::TrafficLightDetection;
using apollo::relative_map::MapMsg;
using apollo::routing::RoutingRequest;
using apollo::routing::RoutingResponse;
using apollo::storytelling::Stories;

std::string PlanningComponent::Name() const { return "planning"; }

bool PlanningComponent::Init() {
  injector_ = std::make_shared<DependencyInjector>();

  if (FLAGS_use_navigation_mode) {
    planning_base_ = std::make_unique<NaviPlanning>(injector_);
  } else {
    planning_base_ = std::make_unique<OnLanePlanning>(injector_);
  }

  ACHECK(
      apollo::common::GetProtoFromFile(FLAGS_planning_config_file, &config_))
      << "failed to load planning conf file: " + FLAGS_planning_config_file;

  planning_base_->Init(config_);

  ros::NodeHandle nh;

  prediction_reader_ = nh.subscribe(config_.topic_config().prediction_topic(), 5, 
      &PlanningComponent::OnPrediction, this);
  chassis_reader_ = nh.subscribe(config_.topic_config().chassis_topic(), 5, 
      &PlanningComponent::OnChassis, this);
  localization_reader_ = nh.subscribe(config_.topic_config().localization_topic(), 5, 
      &PlanningComponent::OnLocalization, this);
  routing_reader_ = nh.subscribe(config_.topic_config().routing_response_topic(), 5, 
      &PlanningComponent::OnRouting, this);
  traffic_light_reader_ = nh.subscribe(config_.topic_config().traffic_light_detection_topic(), 5, 
      &PlanningComponent::OnTrafficLight, this);
  pad_msg_reader_ = nh.subscribe(config_.topic_config().planning_pad_topic(), 5, 
      &PlanningComponent::OnPad, this);
  story_telling_reader_ = nh.subscribe(config_.topic_config().story_telling_topic(), 5, 
      &PlanningComponent::OnStorytelling, this);
  if (FLAGS_use_navigation_mode) {
    relative_map_reader_ = nh.subscribe(config_.topic_config().relative_map_topic(), 5, 
        &PlanningComponent::OnRelativeMap, this);
  }

  planning_writer_ = nh.advertise<std_msgs::String>(
      config_.topic_config().planning_trajectory_topic(), 1);
  rerouting_writer_ = nh.advertise<std_msgs::String>(
      config_.topic_config().routing_request_topic(), 1);

  proc_timer_ = nh.createTimer(ros::Duration(1.0 / FLAGS_planning_loop_rate), 
      &PlanningComponent::Proc, this);

  return true;
}

void PlanningComponent::Proc(const ros::TimerEvent &) {
  // check and process possible rerouting request
  CheckRerouting();

  // process fused input data
  local_view_.prediction_obstacles = prediction_obstacles_;
  local_view_.chassis = chassis_;
  local_view_.localization_estimate = localization_estimate_;
  {
    std::lock_guard<std::mutex> lock(mutex_);
    if (!local_view_.routing ||
        hdmap::PncMap::IsNewRouting(*local_view_.routing, routing_)) {
      local_view_.routing =
          std::make_shared<routing::RoutingResponse>(routing_);
    }
  }
  {
    std::lock_guard<std::mutex> lock(mutex_);
    local_view_.traffic_light =
        std::make_shared<TrafficLightDetection>(traffic_light_);
    local_view_.relative_map = std::make_shared<MapMsg>(relative_map_);
  }
  {
    std::lock_guard<std::mutex> lock(mutex_);
    local_view_.pad_msg = std::make_shared<PadMessage>(pad_msg_);
  }
  {
    std::lock_guard<std::mutex> lock(mutex_);
    local_view_.stories = std::make_shared<Stories>(stories_);
  }

  if (!CheckInput()) {
    AERROR << "Input check failed";
    return;
  }

  ADCTrajectory adc_trajectory_pb;
  planning_base_->RunOnce(local_view_, &adc_trajectory_pb);
  auto start_time = adc_trajectory_pb.header().timestamp_sec();
  common::util::FillHeader(Name(), &adc_trajectory_pb);

  // modify trajectory relative time due to the timestamp change in header
  const double dt = start_time - adc_trajectory_pb.header().timestamp_sec();
  for (auto& p : *adc_trajectory_pb.mutable_trajectory_point()) {
    p.set_relative_time(p.relative_time() + dt);
  }
  std_msgs::String ros_msg;
  ros_msg.data = adc_trajectory_pb.SerializeAsString();
  planning_writer_.publish(ros_msg);

  // record in history
  auto* history = injector_->history();
  history->Add(adc_trajectory_pb);
}

void PlanningComponent::CheckRerouting() {
  auto* rerouting = injector_->planning_context()
                        ->mutable_planning_status()
                        ->mutable_rerouting();
  if (!rerouting->need_rerouting()) {
    return;
  }
  common::util::FillHeader(Name(), rerouting->mutable_routing_request());
  rerouting->set_need_rerouting(false);
  std_msgs::String ros_msg;
  ros_msg.data = rerouting->routing_request().SerializeAsString();
  rerouting_writer_.publish(ros_msg); 
}

bool PlanningComponent::CheckInput() {
  ADCTrajectory trajectory_pb;
  auto* not_ready = trajectory_pb.mutable_decision()
                        ->mutable_main_decision()
                        ->mutable_not_ready();

  if (local_view_.prediction_obstacles == nullptr) {
    not_ready->set_reason("prediction not ready");
  } else if (local_view_.localization_estimate == nullptr) {
    not_ready->set_reason("localization not ready");
  } else if (local_view_.chassis == nullptr) {
    not_ready->set_reason("chassis not ready");
  } else if (HDMapUtil::BaseMapPtr() == nullptr) {
    not_ready->set_reason("map not ready");
  } else {
    // nothing
  }

  if (FLAGS_use_navigation_mode) {
    if (!local_view_.relative_map->has_header()) {
      not_ready->set_reason("relative map not ready");
    }
  } else {
    if (!local_view_.routing->has_header()) {
      not_ready->set_reason("routing not ready");
    }
  }

  if (not_ready->has_reason()) {
    AERROR << not_ready->reason() << "; skip the planning cycle.";
    common::util::FillHeader(Name(), &trajectory_pb);
    std_msgs::String ros_msg;
    ros_msg.data = trajectory_pb.SerializeAsString();
    planning_writer_.publish(ros_msg);
    return false;
  }
  return true;
}

void PlanningComponent::OnPrediction(const std_msgs::String::ConstPtr &msg) {
  auto prediction_obstacles = std::make_shared<prediction::PredictionObstacles>();
  if (!prediction_obstacles->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  if (prediction_obstacles_ == nullptr) {
    prediction_obstacles_ = std::make_shared<prediction::PredictionObstacles>();
  }
  prediction_obstacles_->CopyFrom(*prediction_obstacles);
}

void PlanningComponent::OnChassis(const std_msgs::String::ConstPtr &msg) {
  auto chassis = std::make_shared<canbus::Chassis>();
  if (!chassis->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  if (chassis_ == nullptr) {
    chassis_ = std::make_shared<canbus::Chassis>();
  }
  chassis_->CopyFrom(*chassis);
}

void PlanningComponent::OnLocalization(const std_msgs::String::ConstPtr &msg) {
  auto localization_estimate = std::make_shared<localization::LocalizationEstimate>();
  if (!localization_estimate->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  if (localization_estimate_ == nullptr) {
    localization_estimate_ = std::make_shared<localization::LocalizationEstimate>();
  }
  localization_estimate_->CopyFrom(*localization_estimate);
}

void PlanningComponent::OnTrafficLight(const std_msgs::String::ConstPtr &msg) {
  auto traffic_light = std::make_shared<TrafficLightDetection>();
  if (!traffic_light->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  traffic_light_.CopyFrom(*traffic_light);
}

void PlanningComponent::OnRouting(const std_msgs::String::ConstPtr &msg) {
  auto routing = std::make_shared<RoutingResponse>();
  if (!routing->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  routing_.CopyFrom(*routing);
}

void PlanningComponent::OnPad(const std_msgs::String::ConstPtr &msg) {
  auto pad_msg = std::make_shared<PadMessage>();
  if (!pad_msg->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  pad_msg_.CopyFrom(*pad_msg);
}

void PlanningComponent::OnRelativeMap(const std_msgs::String::ConstPtr &msg) {
  auto map_message = std::make_shared<MapMsg>();
  if (!map_message->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  relative_map_.CopyFrom(*map_message);
}

void PlanningComponent::OnStorytelling(const std_msgs::String::ConstPtr &msg) {
  auto stories = std::make_shared<Stories>();
  if (!stories->ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
  std::lock_guard<std::mutex> lock(mutex_);
  stories_.CopyFrom(*stories);
}

}  // namespace planning
}  // namespace apollo

APOLLO_COMPONENT(apollo::planning::PlanningComponent);
