/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/**
 * @file
 **/

#pragma once

#include <memory>

#include "apollo_msgs/basic_msgs/pnc_point.pb.h"
#include "apollo_common/status/status.h"
#include "apollo_common/util/factory.h"
#include "apollo_planning/common/frame.h"
#include "apollo_planning/common/reference_line_info.h"
#include "apollo_planning/common/speed_profile_generator.h"
#include "apollo_msgs/planning_msgs/planning.pb.h"
#include "apollo_planning/reference_line/reference_line.h"
#include "apollo_planning/reference_line/reference_point.h"
#include "apollo_planning/scenarios/scenario.h"
#include "apollo_planning/scenarios/stage.h"
#include "apollo_planning/tasks/task.h"

namespace apollo {
namespace planning {
namespace scenario {
namespace lane_follow {

class LaneFollowScenario : public Scenario {
 public:
  LaneFollowScenario(const ScenarioConfig& config,
                     const ScenarioContext* context,
                     const std::shared_ptr<DependencyInjector>& injector)
      : Scenario(config, context, injector) {}

  std::unique_ptr<Stage> CreateStage(
      const ScenarioConfig::StageConfig& stage_config,
      const std::shared_ptr<DependencyInjector>& injector) override;
};

}  // namespace lane_follow
}  // namespace scenario
}  // namespace planning
}  // namespace apollo
