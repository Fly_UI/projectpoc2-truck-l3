/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>

#include "apollo_common/log.h"
#include "apollo_msgs/config_msgs/vehicle_config.pb.h"
#include "apollo_common/configs/vehicle_config_helper.h"
#include "apollo_common/math/vec2d.h"
#include "apollo_msgs/common_msgs/vehicle_state.pb.h"
#include "apollo_common/vehicle_state/vehicle_state_provider.h"
#include "apollo_planning/common/frame.h"
#include "apollo_planning/common/planning_context.h"
#include "apollo_planning/common/util/common.h"
#include "apollo_msgs/planning_msgs/planning_config.pb.h"
#include "apollo_planning/scenarios/park_and_go/park_and_go_scenario.h"
#include "apollo_planning/scenarios/stage.h"
#include "apollo_planning/scenarios/util/util.h"

namespace apollo {
namespace planning {
namespace scenario {
namespace park_and_go {

struct ParkAndGoContext;

class ParkAndGoStageCheck : public Stage {
 public:
  ParkAndGoStageCheck(const ScenarioConfig::StageConfig& config,
                      const std::shared_ptr<DependencyInjector>& injector)
      : Stage(config, injector) {}

  Stage::StageStatus Process(const common::TrajectoryPoint& planning_init_point,
                             Frame* frame) override;

  ParkAndGoContext* GetContext() {
    return Stage::GetContextAs<ParkAndGoContext>();
  }

  Stage::StageStatus FinishStage(const bool success);

 private:
  bool CheckObstacle(const ReferenceLineInfo& reference_line_info);
  void ADCInitStatus();

 private:
  ScenarioParkAndGoConfig scenario_config_;
};

}  // namespace park_and_go
}  // namespace scenario
}  // namespace planning
}  // namespace apollo
