/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>

#include "apollo_common/component.h"
#include "apollo_msgs/chassis_msgs/chassis.pb.h"
#include "apollo_msgs/localization_msgs/localization.pb.h"
#include "apollo_msgs/planning_msgs/learning_data.pb.h"
#include "apollo_msgs/planning_msgs/pad_msg.pb.h"
#include "apollo_msgs/planning_msgs/planning.pb.h"
#include "apollo_msgs/planning_msgs/planning_config.pb.h"
#include "apollo_msgs/prediction_msgs/prediction_obstacle.pb.h"
#include "apollo_msgs/routing_msgs/routing.pb.h"
#include "apollo_msgs/storytelling_msgs/story.pb.h"
#include "apollo_msgs/perception_msgs/traffic_light_detection.pb.h"
#include "apollo_planning/common/message_process.h"
#include "apollo_planning/common/planning_gflags.h"
#include "apollo_planning/planning_base.h"

namespace apollo {
namespace planning {

class PlanningComponent final
    : public apollo::common::Component {

 public:
  PlanningComponent() = default;

  ~PlanningComponent() = default;

 public:
  std::string Name() const override;

  bool Init() override;

  void Proc(const ros::TimerEvent &);

 private:
  void CheckRerouting();
  bool CheckInput();

  void OnPrediction(const std_msgs::String::ConstPtr &);
  void OnChassis(const std_msgs::String::ConstPtr &);
  void OnLocalization(const std_msgs::String::ConstPtr &);
  void OnTrafficLight(const std_msgs::String::ConstPtr &);
  void OnRouting(const std_msgs::String::ConstPtr &);
  void OnPad(const std_msgs::String::ConstPtr &);
  void OnRelativeMap(const std_msgs::String::ConstPtr &);
  void OnStorytelling(const std_msgs::String::ConstPtr &);

 private:
  ros::Timer proc_timer_;
  ros::Subscriber prediction_reader_;
  ros::Subscriber chassis_reader_;
  ros::Subscriber localization_reader_;
  ros::Subscriber traffic_light_reader_;
  ros::Subscriber routing_reader_;
  ros::Subscriber pad_msg_reader_;
  ros::Subscriber relative_map_reader_;
  ros::Subscriber story_telling_reader_;
  ros::Publisher planning_writer_;
  ros::Publisher rerouting_writer_;

  std::mutex mutex_;
  std::shared_ptr<prediction::PredictionObstacles> prediction_obstacles_;
  std::shared_ptr<canbus::Chassis> chassis_;
  std::shared_ptr<localization::LocalizationEstimate> localization_estimate_;
  perception::TrafficLightDetection traffic_light_;
  routing::RoutingResponse routing_;
  planning::PadMessage pad_msg_;
  relative_map::MapMsg relative_map_;
  storytelling::Stories stories_;

  LocalView local_view_;

  std::unique_ptr<PlanningBase> planning_base_;
  std::shared_ptr<DependencyInjector> injector_;

  PlanningConfig config_;
  MessageProcess message_process_;
};

}  // namespace planning
}  // namespace apollo
