#include <ros/ros.h> 
//#include <serial/serial.h>  //ROS已经内置了的串口包 
#include <std_msgs/String.h>
#include <std_msgs/Empty.h> 
#include <string>
#include <vector>
#include <sstream>
#include <cmath>
#include <cstdlib>//string转化为double
#include <iomanip>//保留有效小数
#include <robot_gps/integratedNavigationMsg.h>
#include <sensor_msgs/NavSatFix.h>
#include <CoorConv.h>
#include <PubSub.h>
using namespace std;
int main(int argc, char** argv)
{
    //初始化节点
    ros::init(argc, argv, "Gps2Apollo");
    //声明节点句柄
    ros::NodeHandle nh;
    //注册Publisher到话题GPS
    PubSub PubSub;
    ROS_INFO_STREAM("Subscriber & Publisher start work");
    std::cout<<"line 162"<<std::endl;
    ros::Rate loop_rate(100); 
    std::string strRece;
    int count=0;
    while (ros::ok())
    {
    ros::spinOnce();   
    ROS_INFO_STREAM("ROS is working\n");
    loop_rate.sleep();
    ++count;
    }
    return 0;
}