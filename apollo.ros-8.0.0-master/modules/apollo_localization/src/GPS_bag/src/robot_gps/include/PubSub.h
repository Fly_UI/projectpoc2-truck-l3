//PubSub.hpp
#ifndef __PubSub_H__
#define __PubSub_H__
#include <ros/ros.h>
//#include <serial/serial.h>  //ROS已经内置了的串口包 
#include <std_msgs/String.h>
#include <std_msgs/Empty.h> 
#include <string>
#include <vector>
#include <sstream>
#include <cmath>
#include <cstdlib>//string转化为double
#include <iomanip>//保留有效小数
#include <robot_gps/integratedNavigationMsg.h>
#include <sensor_msgs/NavSatFix.h>
#include <CoorConv.h>
#include <fstream>
#include <iostream>

class PubSub
{
private:
  ros::NodeHandle nh;
  ros::Publisher GPS_pub;
  ros::Subscriber GPS_sub;	
  std::ofstream data_file_;
public:
  PubSub()
//解析GPS
//example1 data: "#BYINSA,ICOM1,0,100.0,UNKNOWN,1356,4299.591,00000000,0000,780;100835322697,,4299.591,0.000000000,0.000000000,0.000,-0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,0,0,0.000,0,0.000,0.000,-0.000,0.0000000,0.0000000,0.0000000,0,010100,0.000,0.000,0.000,*8e9114d2\r\
//  \n"    ; NMEA title
//example2:data: "#BYINSA,ICOM1,0,100.0,UNKNOWN,1356,4300.415,00000000,0000,780;100835322697,,4300.415,0.000000000,0.000000000,0.000,-0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,0,0,0.000,0,0.000,0.000,-0.000,0.0000000,0.0000000,0.0000000,0,010100,0.000,0.000,0.000,*602f1693\r\
  \n<INSPVAX ICOM1 0 100.0 UNKNOWN 1356 4301.000 00000000 0000 780\r\n<    INS_INACTIVE\
  \ NONE 0.00000000000 0.00000000000 0.0000 0.0000 0.0000 0.0000 0.0000 0.000000000\
  \ 0.000000000 -0.000000000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000\
  \ 0.0000 00000000 0 <INSSTDEV ICOM1 0 100.0 UNKNOWN 1356 4301.000 00000000 0000\
  \ 780\r\n<    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 00000000\
  \ 0 0 00bffbbf 0"
  {
    
    GPS_pub = nh.advertise<robot_gps::integratedNavigationMsg>("Gps2Apollo",100);
    GPS_sub = nh.subscribe<std_msgs::String>("d_gps_BYINS",10,&PubSub::Gps_subCallback,this);
    std::string file_name = "/home/apollo_ros/apollo_ros_ws/src/apollo.ros-8.0.0/rosbag/data.csv";
    data_file_.open(file_name.c_str());
    data_file_ << "UTM_x,UTM_y,UTM_z,Pitch,Roll,Heading,V_imu_Foward,acc_imu_Right,V_imu_Z,acc_imu_Foward,acc_imu_Right,angular_velocity_Foward,angular_velocity_Right,angular_velocity_Z" << std::endl;
 } 
~PubSub(){}
inline void RecePro(std::string s , double& lat , double& lon , double& alt,double& Heading,double& Pitch,double& Roll,double& V_imu_Foward,double& V_imu_Right,double& V_imu_Z,double& acc_imu_Foward,double& acc_imu_Right,double& acc_imu_Z,double& angular_velocity_Right,double& angular_velocity_Foward,double& angular_velocity_Z)
   {
    //截取数据、解析数据：
         //GPS起始标志
   //分割有效数据，存入vector中
    std::vector<std::string> v(0, "");
    std::size_t colonPos = s.find(";");
    if (colonPos != std::string::npos)
    {
      std::string substring = s.substr(colonPos + 1);
      std::size_t commaPos = substring.find(",");
      while (commaPos != std::string::npos)
      {
        std::string value = substring.substr(0, commaPos);
        if (value.empty())
          return;
        v.push_back(value);
        substring = substring.substr(commaPos + 1);
        commaPos = substring.find(",");
        
      }
    // Add the last value after the last comma
    std::cout << "substring= " <<  substring << std::endl;
    if (substring.empty() || substring.find("*") == std::string::npos)
          return;
    v.push_back(substring);
    }
    else
    {
        std::cout << "No colon found in the message. Exiting." << std::endl;
        return;
    }
    // for (const auto& element : v) {
    //     if (element.empty())
    //       return;
    //     std::cout << element << std::endl;
    // }
    //modify

    #if 0
    pos3=s.find(";");
    pos1 = 0;
    if (std::string::npos!=pos3)
    {
    pos5=s.find("BYINSA");
    v.push_back(s.substr(pos5));
    pos1=pos3;
    }
    else
    {
    throw("ERRO BYINS type NMEA input,please check GPS singal");
    // exit(100);
    return;
    }
    pos2 = s.find(",",pos3);
    while ( std::string::npos !=pos2 )
    {
        v.push_back( s.substr( pos1, pos2-pos1 ) );
        pos1 = pos2 + 1;
        pos2 = s.find(",",pos1);
    }
    if ( pos1 != s.length() )
        v.push_back( s.substr( pos1 ));
        
    #endif
    //解析出经纬度
    
    if (v.max_size() >= 6 && !v[1].empty() && !v[2].empty() && !v[3].empty() && !v[4].empty()
    && !v[5].empty() && !v[6].empty() && !v[7].empty() && !v[8].empty()
    && !v[5].empty() && !v[6].empty() && !v[7].empty() && !v[8].empty()
    && !v[9].empty() && !v[10].empty() && !v[11].empty() && !v[12].empty()
    && !v[13].empty() && !v[14].empty() && !v[15].empty() && !v[16].empty()
    )
    {
        //纬度
        std::cout << "v[15]" << v[15];
        if (v[3] != "") lat = std::atof(v[3].c_str()); // 100;
        //int ilat = (int)floor(lat) % 100;
        //ROS_INFO("LAT   :    %d",ilat);
        //lat = ilat + (lat - ilat) * 100 / 60;
        //经度
        if (v[4] != "") lon = std::atof(v[4].c_str());// 100;
        //int ilon = (int)floor(lon) % 1000;
        //lon = ilon + (lon - ilon) * 100 / 60;
	    //if (v[2] != "") lat = std::atof(v[2].c_str());
        //if (v[4] != "") lon = std::atof(v[4].c_str());
        if (v[5] != "") alt = std::atof(v[5].c_str());  
        if (v[6] != "") Heading = std::atof(v[6].c_str());
        if (v[7] != "") Pitch = std::atof(v[7].c_str());
        if (v[8] != "") Roll = std::atof(v[8].c_str());
        if (v[9] != "") V_imu_Foward = std::atof(v[9].c_str());
        if (v[10] != "") V_imu_Right = std::atof(v[10].c_str());
        if (v[11] != "") V_imu_Z = std::atof(v[11].c_str());
        if (v[12] != "") acc_imu_Foward = std::atof(v[12].c_str());
        if (v[13] != "") acc_imu_Right = std::atof(v[13].c_str());
        if (v[14] != "") acc_imu_Z = std::atof(v[14].c_str());
        if (v[15] != "") angular_velocity_Right = std::atof(v[15].c_str());
        if (v[16] != "") angular_velocity_Foward = std::atof(v[16].c_str());
        if (v[17] != "") angular_velocity_Z = std::atof(v[17].c_str());
        v.clear();
         // 设置输出的小数位数为9位并使用固定的小数格式
    std::cout << std::setprecision(9) << std::fixed;

    std::cout << "lat=" << lat << " lon=" << lon << " alt=" << alt << " Heading=" << Heading
              << " Pitch=" << Pitch << " Roll=" << Roll << " V_imu_Foward=" << V_imu_Foward
              << " V_imu_Right=" << V_imu_Right << " V_imu_Z=" << V_imu_Z
              << " acc_imu_Foward=" << acc_imu_Foward << " acc_imu_Right=" << acc_imu_Right
              << " acc_imu_Z=" << acc_imu_Z << std::endl;
       }
   }
inline void Gps_subCallback(const std_msgs::String::ConstPtr &msg)
  { 
    std::string strRece=msg->data.c_str();
     std::string gstart = "BYINSA";
    //GPS终止标志
    std::string gend = "*";
    int i = 0, start = -1, end = -1;
    while ( i < strRece.length() )
    {
       //找起始标志

        start = strRece.find(gstart);

        //如果没找到，丢弃这部分数据，但要留下最后2位,避免遗漏掉起始标志
        if ( start == -1)
           {

             if (strRece.length() > 2)   
                 strRece = strRece.substr(strRece.length()-3);
                  break;

            }

         //如果找到了起始标志，开始找终止标志
        else
           {
            //找终止标志
            end = strRece.find(gend);
                        
            //如果没找到，把起始标志开始的数据留下，前面的数据丢弃，然后跳出循环
            if (end == -1)
            {
             if (end != 0)
                strRece = strRece.substr(start);
                break;
            }
            //如果找到了终止标志，把这段有效的数据剪切给解析的函数，剩下的继续开始寻找
            else
            {
            i = end;
            //把有效的数据给解析的函数以获取经纬度
            double lat, lon, alt,Heading,Pitch,Roll,V_imu_Foward,V_imu_Right,V_imu_Z,acc_imu_Foward,acc_imu_Right,acc_imu_Z,angular_velocity_Right,angular_velocity_Foward,angular_velocity_Z;
            RecePro(strRece.substr(start,end+2-start),lat,lon,alt,Heading,Pitch,Roll,V_imu_Foward,V_imu_Right,V_imu_Z,acc_imu_Foward,acc_imu_Right,acc_imu_Z,angular_velocity_Right,angular_velocity_Foward,angular_velocity_Z);
            //std::cout << std::setiosflags(std::ios::fixed)<<std::setprecision(7)<< "lat:" << lat << " lon:"<< lon << "\n";
            //UTM->WS84
            WGS84Corr t;
            UTMCoor test;
            t.lat=lat;
            t.log=lon;
            // BYINS is degree not need to cov.
            //t.lat = (d2*0.01-int(d2*0.01))*100/60+int(d2*0.01);
            //t.log= (d3*0.01-int(d3*0.01))*100/60+int(d3*0.01);
			      ROS_INFO_STREAM("LatLonToUTMXY is working\n");
            LatLonToUTMXY(DegToRad(t.lat), DegToRad(t.log),51,test);
            //if (test.x > 1 || test.y>1 )   
	         //{
		        //cout.precision(16);
               //发布消息到话题
               robot_gps::integratedNavigationMsg gps_msg;
               gps_msg.header.stamp = ros::Time::now();
               gps_msg.Pitch=Pitch;
               gps_msg.Roll=Roll;
               gps_msg.Heading=Heading;
			         gps_msg.Vx=V_imu_Foward;
			         gps_msg.Vy=V_imu_Right;
			         gps_msg.Vz=V_imu_Z;
               gps_msg.accX=acc_imu_Foward;
               gps_msg.accY=acc_imu_Right;
               gps_msg.angular_velocity_X=angular_velocity_Foward;//x-right   y-foward
               gps_msg.angular_velocity_Y=angular_velocity_Right;
               gps_msg.angular_velocity_Z=angular_velocity_Z;
			    ROS_INFO_STREAM("Gps_pub is working\n");
                gps_msg.Lattitude = _Float64(test.x);
                gps_msg.Longitude = _Float64(test.y);
                gps_msg.Altitude =_Float64(alt);
              //增加保存csv的操作by travis
              data_file_ << std::setprecision(15) << test.x << ", " \
                                      << test.y << ", " \
                                      << alt << ", " \
                                      << Pitch << ", " \
                                      << Roll << ", " \
                                      << Heading << ", " \
                                      << V_imu_Foward << ", " \
                                      << V_imu_Right << ", " \
                                      << V_imu_Z << ", " \
                                      << acc_imu_Foward << ", " \
                                      << acc_imu_Right << ", " \
                                      << angular_velocity_Foward << ", " \
                                      << angular_velocity_Right << ", " \
                                      << angular_velocity_Z << std::endl;
              //增加保存csv的操作by travis
                GPS_pub.publish(gps_msg);
                if ( i+5 < strRece.length())
                strRece = strRece.substr(end+2);
                else
                {   strRece = strRece.substr(end+2);
                    break;
                }
            }
        }
    }
	 ROS_INFO_STREAM("ROS_callback is working\n");
  }
};
#endif //__PubSub_H__
