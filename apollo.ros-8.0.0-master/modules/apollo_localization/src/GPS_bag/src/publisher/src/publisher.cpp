#include <ros/ros.h>
#include <std_msgs/String.h>
#include <iostream>
using namespace std;
int main(int argc, char **argv)
{
        ros::init(argc, argv,"pub");
        ros::NodeHandle nd;
        ros::Publisher publish = nd.advertise<std_msgs::String>("d_gps_BYINS", 10);
        ros::Rate loop_rate(10);
        int count = 0;
        while (ros::ok())
        {
                std_msgs::String msg;
                std::stringstream ss;
                ss<<" #BYINSA,ICOM1,0,100.0,UNKNOWN,1356,4300.415,00000000,0000,780;100835322697,,4300.415,0.000000000,0.000000000,0.000,-0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,0,0,0.000,0,0.000,0.000,-0.000,0.0000000,0.0000000,0.0000000,0,010100,0.000,0.000,0.000,*602f1693\r "<<count;
                msg.data=ss.str();
                publish.publish(msg);
                ROS_INFO("data:#BYINSA,ICOM1,0,100.0,UNKNOWN,1356,4299.591,00000000,0000,780;100835322697,,4299.591,0.000000000,0.000000000,0.000,-0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,0,0,0.000,0,0.000,0.000,-0.000,0.0000000,0.0000000,0.0000000,0,010100,0.000,0.000,0.000,*8e9114d2");
                ros::spinOnce();
                loop_rate.sleep();
                ++count;
        }

        return 0;
}