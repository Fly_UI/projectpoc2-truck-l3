#include "gps/Socket.hpp"
#include "iostream"
/// @brief Constructor for the Socket
Socket::Socket()
: sock{ socket(AF_INET, SOCK_STREAM, 0) }
{}

/// @brief Closing the socket
Socket::~Socket()
{
    close(sock);
}

/// @brief Connects to a socket
/// @param ip 
/// @param port 
/// @return 
///  CANNOT_CREATE: Could not create socket
///  INVALID_ADDRESS: Could not convert ip to binary
///  CANNOT_CONNECT: Could not connect to socket
///  OK: Socket connected
SOCKET_STATUS Socket::connectSocket(const std::string& ip, const std::size_t port)
{
    // TODO: should loop through if it's disconnected to connect to any port in future
    struct sockaddr_in serverAddress;
    std::cout<<"Socket_start_26line"<<std::endl;
    if (sock < 0) 
    {
        return SOCKET_STATUS::CANNOT_CREATE;
    }
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);

    // Convert from text to binary
    if (inet_pton(AF_INET, ip.c_str(), &serverAddress.sin_addr) <= 0) 
    {
        return SOCKET_STATUS::INAVLID_ADDRESS;
    }
    if (connect(sock, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("connect");
        return SOCKET_STATUS::CANNOT_CONNECT;
    }
    return SOCKET_STATUS::OK;
    std::cout<<"OK45"<<std::endl;
}
 
/// @brief Receives data from the socket
/// @return
///  CANNOT_READ: Could not read from socket
///  OK: Data received
std::pair<SOCKET_STATUS, std::string> Socket::receive()
{
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(sock, &readfds);
    struct timeval tv;
    // set timeout to 0 for non-blocking mode
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    // use select to check if data is available to read
    if (select(sock + 1, &readfds, NULL, NULL, &tv) > 0)
    {
        char buffer[1024]{};
        std::size_t bytesRead = read(sock, buffer, sizeof(buffer));
        if (bytesRead < 0)
        {
            // perror("Cannot read");
            return { SOCKET_STATUS::CANNOT_READ, {} };
        }
        return { SOCKET_STATUS::OK, std::string(buffer, bytesRead) };
    }
    // perror("Cannot read");
    return { SOCKET_STATUS::CANNOT_READ, {} };
}

/// @brief Sends data to the socket
/// @param data 
/// @return 
///  CANNOT_WRITE: Could not write to socket
///  OK: Data sent
SOCKET_STATUS Socket::send(std::string_view data)
{
    // TODO: Implement this
    if (write(sock, data.data(), data.size()) < 0)
    {
        return SOCKET_STATUS::CANNOT_WRITE;
    }
    return SOCKET_STATUS::OK;
}