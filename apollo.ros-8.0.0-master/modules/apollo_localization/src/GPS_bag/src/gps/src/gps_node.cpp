#include "gps/Communicators.hpp"

#include "std_msgs/String.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "GPS");
    Communicators<std_msgs::String> gps({ "d_gps_BYINS", 
        "d_gps_INSPVAX" ,
        "d_gps_INSSTDEV"
    });
    ros::spin();
    return 0;
}