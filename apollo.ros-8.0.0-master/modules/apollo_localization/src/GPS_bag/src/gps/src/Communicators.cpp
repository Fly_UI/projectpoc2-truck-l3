#include "gps/Communicators.hpp"

/// @breif Constructor for just publisher topic
/// @param publishTopicName The name of the topic to publish to
template<typename T>
Communicators<T>::Communicators(const std::vector<std::string>& publishTopicNames) 
    : Communicators{ publishTopicNames, "_DUMMY_", NULL }
{}

/// @brief Constructor for just subscriber topic 
/// @param subscribeTopicName The name of the topic to subscribe to
/// @param callback The callback function to be executed when a message is received
template<typename T>
Communicators<T>::Communicators(const std::string& subscribeTopicName, std::function<void(const typename T::ConstPtr&)> callback)
    : Communicators{ { "_DUMMY_" }, subscribeTopicName, callback }
{}


/// @brief The Communicators class is handling the publishing and subscribing
///     data to coressponding topic name given. 
/// @param publishTopicName The name of the topic to publish to
/// @param subscribeTopicName The name of the topic to subscribe to 
/// @param callback The callback function to be executed when a message is received
template<typename T>
Communicators<T>::Communicators(const std::vector<std::string>& publishTopicNames, const std::string& subscribeTopicName, std::function<void(const typename T::ConstPtr&)> callback)
    : pTopicNames{ publishTopicNames }, sTopicName{ subscribeTopicName }, nh{ ros::NodeHandle() },
    publishers{ (pTopicNames[0] != "_DUMMY_") ? std::make_unique<Publishers>(initialPublishers()) : nullptr },
    subscriber{ (sTopicName != "_DUMMY_") ? std::make_unique<ros::Subscriber>(nh.subscribe<T>(sTopicName.c_str(), 100, callback)) : nullptr },
    timer{ (publishers) ? std::make_unique<ros::Timer>(nh.createTimer(ros::Duration(0.01), &Communicators::signalTimer, this)) : nullptr },
    sock{}, status { sock.connectSocket(IP, PORT) }
    {}

/// @brief Publishes the given data to the 
///     ROS topic if there are the specified number of subscribers.
/// @param data The data to be published.
/// @param numSubs The number of subscribers that should be present before publishing.
/// @param pTopicName Name of the topic to publish to.
/// @return true if data published, otherwise false
template<typename T>
bool Communicators<T>::publish(const T& data, const uint8_t numSubs, const std::string& pTopicName)
{
    const std::string typeData(pTopicName.begin() + 6, pTopicName.end());
    if (!publishers->count(typeData))
        return false;

    ros::Rate poll_rate(1);
    while (publishers->at(typeData).getNumSubscribers() != numSubs)
        poll_rate.sleep();
    
    publishers->at(typeData).publish(data);
    ros::spinOnce();
    poll_rate.sleep();
    ros::spinOnce();
    return true;
}

/// @brief Initialize the ros::publisher to the map with corresponding name
/// @return map of publishers with corresponding name tag and publishe with that name as topic name
template<typename T>
Publishers Communicators<T>::initialPublishers()
{
    Publishers temp;
    // Pattern: d_gps_TypeData
    for (const std::string& pTopicName : pTopicNames)
    {
        std::string typeData(pTopicName.begin() + 6, pTopicName.end());
        temp[typeData] = nh.advertise<T>(pTopicName.data(), 100);
    }
    return temp;
}

/// @brief Callback function for timer
/// @tparam T 
/// @param e 
template<typename T>
void Communicators<T>::signalTimer(const ros::TimerEvent& e)
{
    //std::cout<<"code here 78 %d\n"<< "status "<<std::endl;
    if (!publishers)
        return;
    // if connection was established
    if (status == SOCKET_STATUS::OK)
        {
        // receive data
        std::pair<SOCKET_STATUS, std::string> packet = sock.receive();
        // if data is not empty and received successfully
        if (packet.first == SOCKET_STATUS::OK && !packet.second.empty())
        {
            const std::regex flagRegex(R"((\$|#)[A-Za-z]+)");
            std::smatch matches;
            std::string typeData{};
            // find any match for (#|$)AnyChar, e.g.: $BYINS, #INSTD
            if (std::regex_search(packet.second, matches, flagRegex))
            {
                //std::cout<<"code here 94\n"<<std::endl;
                typeData = matches.str();
                // if it starts with '#', it does have an extra 'A' at the end.
                if (typeData[0] == '#')
                    typeData.erase(typeData.end() - 1);
                // erase the first character which is either $ or #
                typeData.erase(typeData.begin());
                //std::cout<<"code here 100\n"<<std::endl;
            }
            else 
            {
                // std::cerr << "Cannot find the flag in the data received from socket" << std::endl;
                return;
            }
            if (!typeData.empty() && publishers->count(typeData))
            {
                data_.data = packet.second;
                publishers->at(typeData).publish(data_);
                 std::cout<<"code here 111"<<std::endl;
            }
            else
            {
                // std::cerr << "There is no flag in the data received from socket" << std::endl;
                return;
            }
        }
        else
        {
            // perror("CANNOT READ\n");
            return;
        }
    }
    else if (status == SOCKET_STATUS::CANNOT_CONNECT)
    {
        // perror("CANNOT CONNECT TO SOCKET\n");
       return;
    }
    else if (status == SOCKET_STATUS::CANNOT_READ)
    {
        // perror("CANNOT READ FROM SOCKET\n");
        return;
    }
    else
    {
        // perror("UNKNOWN ERROR FROM SOCKET\n");
        return;
    }
}

template class Communicators<std_msgs::String>;