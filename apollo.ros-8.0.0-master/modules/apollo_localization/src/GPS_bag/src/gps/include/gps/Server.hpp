#pragma once

#include <iostream>
#include <string>
#include <utility>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

enum class SERVER_STATUS
{
    OK,
    CANNOT_CREATE,
    CANNOT_ATTACH,
    INAVLID_ADDRESS,
    CANNOT_BIND,
    CANNOT_LISTEN,
    CANNOT_ACCEPT,
    CANNOT_SEND,
    CANNOT_RECEIVE,
    ERROR,
};

class Server
{
public:
    Server();
    ~Server();
    SERVER_STATUS start(const std::string& ip, const std::size_t port);
    SERVER_STATUS sent(const std::string& msg);
    std::pair<SERVER_STATUS, std::string> receive();

private:
	int server, new_socket, valread;
};
