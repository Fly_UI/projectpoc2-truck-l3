#pragma once

#include <iostream>
#include <string>
#include <utility>
#include <string_view>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

constexpr const char* IP{ "192.168.8.151" }; //IP address:192.168.8.151
constexpr std::size_t PORT{ 1111 };  //port:1111

enum class SOCKET_STATUS
{
    OK,
    INAVLID_ADDRESS,
    CANNOT_CREATE,
    CANNOT_READ,
    CANNOT_WRITE,
    CANNOT_CONNECT,
    ERROR
};
class Socket
{

private:
    int sock;

public:
    /// @brief Constructor for the Socket
    Socket();
    /// @brief Closing the socket
    ~Socket();
    /// @brief Connects to a socket
    /// @param ip 
    /// @param port 
    /// @return 
    ///  CANNOT_CREATE: Could not create socket
    ///  INVALID_ADDRESS: Could not convert ip to binary
    ///  CANNOT_CONNECT: Could not connect to socket
    ///  OK: Socket connected
    SOCKET_STATUS connectSocket(const std::string& ip = IP, const std::size_t port = PORT);
    /// @brief Receives data from the socket
    /// @return
    ///  CANNOT_READ: Could not read from socket
    ///  OK: Data received
    std::pair<SOCKET_STATUS, std::string> receive();
    /// @brief Sends data to the socket
    /// @param data 
    /// @return 
    ///  CANNOT_WRITE: Could not write to socket
    ///  OK: Data sent
    SOCKET_STATUS send(std::string_view data);
};