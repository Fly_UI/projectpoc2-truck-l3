#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <regex>
#include <string_view>

#include "ros/ros.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"

#include "gps/Socket.hpp"


using Publishers = std::map<std::string, ros::Publisher>;

// using std::pair<std::string, std_msgs::String> buffer;

// ROS Communicators for publishing data and subscribing to data
template<class T>
class Communicators
{
#ifdef CATKIN_ENABLE_TESTING
public:
#else
private:
#endif // CATKIN_ENABLE_TESTING
    std::vector<std::string> pTopicNames; // The name of the topic to publish to
    std::string sTopicName; // The name of the topic to subscribe to
    ros::NodeHandle nh; // The ROS node handle
    std::unique_ptr<Publishers> publishers; 
    std::unique_ptr<ros::Subscriber> subscriber;
    std::unique_ptr<ros::Timer> timer; // 
    Socket sock;
    SOCKET_STATUS status;
    
    // TODO: Should get publish data from caller or should handle within class?
    T data_;

    /// @brief callback function for timer 
    /// @param e 
    void signalTimer(const ros::TimerEvent& e);

    /// @brief Initialize the ros::publisher to the map with corresponding name
    /// @return map of publishers with corresponding name tag and publishe with that name as topic name
    Publishers initialPublishers();


    /// @brief publish data
    /// @param data The data to be published
    /// @param numSubs The number of subscribers to publish the data to
    /// @param pTopicName Name of the topic to publish to.
    /// @return true if data published, otherwise false
    bool publish(const T& data, const uint8_t numSubs, const std::string& pTopicName);

public:

    /// @brief The Communicators class is handling the publishing and subscribing
    ///     data to coressponding topic name given. 
    /// @param publishTopicName The name of the topic to publish to
    /// @param subscribeTopicName The name of the topic to subscribe to 
    /// @param callback The callback function to be executed when a message is received
    Communicators(const std::vector<std::string>& publishTopicNames, const std::string& subscribeTopicName, std::function<void(const typename T::ConstPtr&)> callback);
    
    /// @brief Constructor for just subscriber topic 
    /// @param subscribeTopicName The name of the topic to subscribe to
    /// @param callback The callback function to be executed when a message is received
    Communicators(const std::string& subscribeTopicName, std::function<void(const typename T::ConstPtr&)> callback);
    
    /// @breif Constructor for just publisher topic
    /// @param publishTopicName The name of the topic to publish to
    Communicators(const std::vector<std::string>& publishTopicNames);
    
    /// @brief shutdown ros and any callback and topic 
    ~Communicators() { ros::shutdown(); };

};

