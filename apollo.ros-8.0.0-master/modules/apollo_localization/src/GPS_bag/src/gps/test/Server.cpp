#include "gps/Server.hpp"

Server::Server()
    : server{ socket(AF_INET, SOCK_STREAM, 0) }
{}

Server::~Server()
{
    close(server);
	shutdown(server, SHUT_RDWR);
}
SERVER_STATUS Server::start(const std::string& ip, const std::size_t port)
{
    if (server == -1) {
        perror("Cannot create");
        return SERVER_STATUS::CANNOT_CREATE;
    }

	struct sockaddr_in address;
	int addrlen = sizeof(address);
    int opt = 1;
    // Forcefully attaching socket to the port 8080
    if (setsockopt(server, SOL_SOCKET,
                SO_REUSEADDR | SO_REUSEPORT, &opt,
                sizeof(opt))) {
        perror("Cannot attach");
        return SERVER_STATUS::CANNOT_ATTACH;
    }
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    
    if (inet_pton(AF_INET, ip.c_str(), &address.sin_addr) <= 0) 
    {
        perror("Invalid address");
        return SERVER_STATUS::INAVLID_ADDRESS;
    }
    // Forcefully attaching socket to the port 8080
    if (bind(server, (struct sockaddr*)&address,
            sizeof(address)) < 0) {
        perror("Cannot bind");
        return SERVER_STATUS::CANNOT_BIND;
    }
    if (listen(server, 3) < 0) {
        perror("Cannot listen");
        return SERVER_STATUS::CANNOT_LISTEN;
    }
    if ((new_socket
		= accept(server, (struct sockaddr*)&address,
				(socklen_t*)&addrlen))
		< 0) {
		perror("Cannot accept");
        return SERVER_STATUS::CANNOT_ACCEPT;
	}

    return SERVER_STATUS::OK;
}
SERVER_STATUS Server::sent(const std::string& msg)
{
	return SERVER_STATUS(
        send(new_socket, msg.c_str(), msg.size(), 0) == msg.size() 
        ? SERVER_STATUS::OK 
        : SERVER_STATUS::ERROR);
}
std::pair<SERVER_STATUS, std::string> Server::receive()
{
    char buffer[1024] = { 0 };
	valread = read(new_socket, buffer, 1024);
    return { SERVER_STATUS(valread), std::string(buffer) };
}
