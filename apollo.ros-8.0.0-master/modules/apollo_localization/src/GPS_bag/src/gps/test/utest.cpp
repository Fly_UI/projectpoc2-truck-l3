#include <iostream>
#include <thread>
#include <algorithm>
#include <gtest/gtest.h>
#include <future>

#include "gps/Communicators.hpp"
#include "gps/Socket.hpp"
#include "gps/Server.hpp"

#define DISABLE

using RosString = std_msgs::String;
using SubscriberCallbackFunc = std::function<void(const RosString::ConstPtr& msg)>;

const std::string IP_TEST{ "127.0.0.1" };
const std::size_t PORT_TEST { 8080 };

#ifndef DISABLE

TEST(COMMUNICATORS, constructionPublishers)
{
    /**
     *  Description: 
     *   check to see if the right constructor called,
     *   the map publishers withing class fill correct.
    */
    std::vector<std::string> topicNames { "d_gps_firstPublisher", "d_gps_secondPublisher" };
    Communicators<RosString> pubs(topicNames);
    
    EXPECT_EQ(pubs.pTopicNames, topicNames);
    EXPECT_EQ(pubs.sTopicName, "_DUMMY_");
    EXPECT_EQ(pubs.data_, RosString{});
    
    EXPECT_EQ(pubs.subscriber, nullptr);
    
    EXPECT_NE(pubs.publishers, nullptr);
    EXPECT_NE(pubs.timer, nullptr);

    /**
     *   Key value should be the name of the topic without 'd_gps_',
     *   as a pattern.
    */
    std::vector<std::string> typeDataNames{ "firstPublisher", "secondPublisher" };
    std::vector<std::string> publishersName;
    // Get name of the keys
    for (const auto& publisher : *(pubs.publishers.get()))
        publishersName.push_back(publisher.first);
    EXPECT_EQ(typeDataNames, publishersName);
    
    EXPECT_TRUE(pubs.publishers->count(publishersName[0])); // it should exist based on the name pattern
    EXPECT_TRUE(pubs.publishers->count(publishersName[0])); // it should exist based on the name pattern
    EXPECT_FALSE(pubs.publishers->count(topicNames[0])); // it should NOT exist based on the name pattern
    EXPECT_THROW(pubs.publishers->at("SOMETHING"), std::out_of_range);
    EXPECT_THROW(pubs.publishers->at("d_gps_SOMETHING"), std::out_of_range);
}
TEST(COMMUNICATORS, constructionSubscriber)
{
    /**
     *  Description: 
     *   check to see if the right constructor called,
     *   the callback function assisnged.
    */
    std::function<void(const RosString::ConstPtr&)> callback{
        [](const RosString::ConstPtr& msg)
        {

            std::cout << "This is a callback\n";
        }
    };
    std::string topicName{ "singleSubscriber" };
    Communicators<RosString> subs(topicName, callback);
    
    EXPECT_EQ(subs.sTopicName, topicName);
    EXPECT_EQ(subs.pTopicNames[0], "_DUMMY_");
    EXPECT_EQ(subs.data_, RosString{});
    
    EXPECT_NE(subs.subscriber, nullptr);
    
    EXPECT_EQ(subs.publishers, nullptr);
    EXPECT_EQ(subs.timer, nullptr);
}
TEST(COMMUNICATORS, constructionPublishersSubscriber)
{
    /**
     *  Description: 
     *   check to see if the right constructor called,
     *   the map publishers withing class fill correct,
     *   the callback function assisnged.
    */
    std::function<void(const RosString::ConstPtr&)> callback{
        [](const RosString::ConstPtr& msg)
        {

            std::cerr << "This is a callback\n";
        }
    };
    std::string sTopicName{ "singleSubscriber" };
    std::vector<std::string> pTopicNames { "d_gps_firstPublisher", "d_gps_secondPublisher" };
    Communicators<RosString> communicator(pTopicNames, sTopicName, callback);
    
    EXPECT_EQ(communicator.pTopicNames, pTopicNames);
    EXPECT_EQ(communicator.sTopicName, sTopicName);
    EXPECT_EQ(communicator.data_, RosString{});
    
    EXPECT_NE(communicator.subscriber, nullptr);
    
    EXPECT_NE(communicator.publishers, nullptr);
    EXPECT_NE(communicator.timer, nullptr);

    /**
     *   Key value should be the name of the topic without 'd_gps_',
     *   as a pattern.
    */
    std::vector<std::string> typeDataNames{ "firstPublisher", "secondPublisher" };
    std::vector<std::string> publishersName;
    // Get name of the keys
    for (const auto& publisher : *(communicator.publishers.get()))
        publishersName.push_back(publisher.first);
    EXPECT_EQ(typeDataNames, publishersName);
    
    EXPECT_TRUE(communicator.publishers->count(publishersName[0])); // it should exist based on the name pattern
    EXPECT_TRUE(communicator.publishers->count(publishersName[0])); // it should exist based on the name pattern
    EXPECT_FALSE(communicator.publishers->count(pTopicNames[0])); // it should NOT exist based on the name pattern
    EXPECT_THROW(communicator.publishers->at("SOMETHING"), std::out_of_range);
    EXPECT_THROW(communicator.publishers->at("d_gps_SOMETHING"), std::out_of_range);
}
TEST(COMMUNICATORS, twoMessagePublish)
{
    /**
     * Description:
     * Construct a publisher and send a simple message on 
     * the pattern of:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * use a subscriber to see if the data is arrived.
    */
    std::function<void(const RosString::ConstPtr& msg)> printSubscribedData{
        [](const RosString::ConstPtr& msg)
        {
            static bool executed{ false };
            if (executed)
                EXPECT_EQ(msg->data, "$singlePublisher, 123, 456, asd, 1s2, f_1ws");
            else
                EXPECT_EQ(msg->data, "$singlePublisher, 123, 456, asd, 1s2, f_1w");
            executed = true;
        }
    };
    const std::vector<std::string> pTopicName{ "d_gps_singlePublisher" };
    const std::string sTopicName{ pTopicName[0] };
    const std::string typeData(pTopicName[0].begin() + 6, pTopicName[0].end());
    
    RosString msg;
    msg.data = "$singlePublisher, 123, 456, asd, 1s2, f_1w";

    Communicators<RosString> communicator(pTopicName, sTopicName, printSubscribedData);
    communicator.timer->stop();
    communicator.publish(msg, 1, pTopicName[0]);
    msg.data += "s"; 
    communicator.publish(msg, 1, pTopicName[0]);
}
TEST(COMMUNICATORS, twoMessageMultiPublish)
{
   /**
     * Description:
     * Construct 3 publishers and send two simple message on 
     * the pattern of:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * use 3 subscriber to see if the data is arrived.
    */
    
    const std::vector<std::string> pTopicNames{ "d_gps_firstPublisher", "d_gps_secondPublisher", "d_gps_thirdPublisher" };
    const std::vector<std::string> sTopicNames{ pTopicNames };

    std::vector<std::string> expecteds {
        { "$" + std::string(pTopicNames[0].begin() + 6, pTopicNames[0].end()) + ", 123, 456, 789, 1wd1, s10s, ))_)@!" },
        { "$" + std::string(pTopicNames[1].begin() + 6, pTopicNames[1].end()) + ", 123, 456, 789, s10s" },
        { "$" + std::string(pTopicNames[2].begin() + 6, pTopicNames[2].end()) + ", 1asd, 4256, zxc521, s10523s" }
    };

    SubscriberCallbackFunc firstCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            static bool executed{ false };
            if (executed)
                EXPECT_EQ(msg->data, expecteds[0]);
            else
                EXPECT_EQ(msg->data, expecteds[0]);
            executed = true;
            expecteds[0] += ", _sqwe";
        }
    };
    SubscriberCallbackFunc secondCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            static bool executed{ false };
            if (executed)
                EXPECT_EQ(msg->data, expecteds[1]);
            else
                EXPECT_EQ(msg->data, expecteds[1]);
            executed = true;
            expecteds[1] += ", ))_)@!";
        }
    };
    SubscriberCallbackFunc thirdCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            static bool executed{ false };
            if (executed)
                EXPECT_EQ(msg->data, expecteds[2]);
            else
                EXPECT_EQ(msg->data, expecteds[2]);
            executed = true;
            expecteds[2] += "ddd";
        }
    };

    std::vector<RosString> msgs(expecteds.size());
    for (std::size_t i{}; i != msgs.size(); ++i)
        msgs[i].data = expecteds[i];
    
    Communicators<RosString> pubs(pTopicNames);
    pubs.timer->stop();
    Communicators<RosString> firstSub(sTopicNames[0], firstCallback);
    Communicators<RosString> secondSub(sTopicNames[1], secondCallback);
    Communicators<RosString> thirdSub(sTopicNames[2], thirdCallback);

    pubs.timer->stop();
    pubs.publish(msgs[0], 1, pTopicNames[0]);
    msgs[0].data += ", _sqwe"; 
    pubs.publish(msgs[0], 1, pTopicNames[0]);

    
    pubs.publish(msgs[1], 1, pTopicNames[1]);
    msgs[1].data += ", ))_)@!"; 
    pubs.publish(msgs[1], 1, pTopicNames[1]);

    pubs.publish(msgs[2], 1, pTopicNames[2]);
    msgs[2].data += "ddd"; 
    pubs.publish(msgs[2], 1, pTopicNames[2]);

}
TEST(COMMUNICATORS, sixMessageMultiPublish)
{
   /**
     * Description:
     * Construct 5 publishers and send 6 simple message for each publisher on 
     * the pattern of:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * use 5 subscriber to see if the data is arrived.
     * In the while loop i send messages in not any order,
     * to test of callbacks.
    */
    
    const std::vector<std::string> pTopicNames{ 
        "d_gps_id", 
        "d_gps_lidar", 
        "d_gps_frequency",
        "d_gps_timeStamp",
        "d_gps_xyz" };
    const std::vector<std::string> sTopicNames{ pTopicNames };

    std::vector<std::string> expecteds {
        { "$" + std::string(pTopicNames[0].begin() + 6, pTopicNames[0].end()) + ", 123, 456, 789, s10s" },
        { "$" + std::string(pTopicNames[1].begin() + 6, pTopicNames[1].end()) + ", 1asd, 4256, zxc521, s10523s" },
        { "$" + std::string(pTopicNames[2].begin() + 6, pTopicNames[2].end()) + ", 9000000" },
        { "$" + std::string(pTopicNames[3].begin() + 6, pTopicNames[3].end()) + ", 123, 456, 789, 1wd1, s10s, ))_)@!" },
        { "$" + std::string(pTopicNames[4].begin() + 6, pTopicNames[4].end()) + ", 123, 456, 923, 523, 294, 135" }
    };

    SubscriberCallbackFunc idCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[0]);
        }
    };
    SubscriberCallbackFunc lidarCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[1]);
        }
    };
    SubscriberCallbackFunc frequencyCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[2]);
        }
    };
    SubscriberCallbackFunc timeStampCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[3]);
        }
    };
    SubscriberCallbackFunc xyzCallback{
        [&expecteds](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[4]);
        }
    };


    std::vector<RosString> msgs(expecteds.size());
    for (std::size_t i{}; i != msgs.size(); ++i)
        msgs[i].data = expecteds[i];
    
    Communicators<RosString> pubs(pTopicNames);
    pubs.timer->stop();

    Communicators<RosString> idSub(sTopicNames[0], idCallback);
    Communicators<RosString> lidarSub(sTopicNames[1], lidarCallback);
    Communicators<RosString> frequencySub(sTopicNames[2], frequencyCallback);
    Communicators<RosString> timeStampSub(sTopicNames[3], timeStampCallback);
    Communicators<RosString> xyzSub(sTopicNames[4], xyzCallback);

    int i{ 0 };
    std::string valueFreq{ "9000000" };
    std::string timeStampSecondValue{ "456" };
    while (i++ != 3)
    {
        // first publish of frequency
        pubs.publish(msgs[2], 1, pTopicNames[2]);
        
        // first and second publish of id
        pubs.publish(msgs[0], 1, pTopicNames[0]);
        msgs[0].data += ", _sqwe" + std::to_string(i); 
        expecteds[0] += ", _sqwe" + std::to_string(i); 
        pubs.publish(msgs[0], 1, pTopicNames[0]);

        
        // first and second publish of lidar
        pubs.publish(msgs[1], 1, pTopicNames[1]);
        msgs[1].data += ", ))_)@!"; 
        expecteds[1] += ", ))_)@!"; 
        pubs.publish(msgs[1], 1, pTopicNames[1]);

        // second publish of frequency
        size_t pos = msgs[2].data.find(valueFreq);
        if (pos != std::string::npos) {
            valueFreq = std::to_string(i * 200000);
            msgs[2].data.replace(pos, valueFreq.size(), valueFreq);
            expecteds[2].replace(pos, valueFreq.size(), valueFreq);
        }
        pubs.publish(msgs[2], 1, pTopicNames[2]);
        
        // first publish of timeStamp
        pubs.publish(msgs[3], 1, pTopicNames[3]);
        
        // first and second publish of xyz
        pubs.publish(msgs[4], 1, pTopicNames[4]);
        pubs.publish(msgs[4], 1, pTopicNames[4]);

        // second publish of timeStamp
        pos = msgs[3].data.find(timeStampSecondValue);
        if (pos != std::string::npos) {
            timeStampSecondValue = std::to_string(i * 40);
            msgs[3].data.replace(pos, timeStampSecondValue.size(), timeStampSecondValue);
            expecteds[3].replace(pos, timeStampSecondValue.size(), timeStampSecondValue);
        }
        pubs.publish(msgs[3], 1, pTopicNames[3]);

        // third publish of frequency
        pos = msgs[2].data.find(valueFreq);
        if (pos != std::string::npos) {
            valueFreq = std::to_string(std::stoi(valueFreq) * 2);
            msgs[2].data.replace(pos, valueFreq.size(), valueFreq);
            expecteds[2].replace(pos, valueFreq.length(), valueFreq);
        }
        pubs.publish(msgs[2], 1, pTopicNames[2]);
    }
}
TEST(COMMUNICATORS, twoMessageSinglePublishMultipleSubscribe)
{
      /**
     * Description:
     * Construct a publishers and send two simple message on 
     * the pattern of:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * use 6 subscriber to see if the data is arrived.
     * 4 subscriber have the same callback, and 2 subscriber have different callback,
     * check if the callback execute correctly.
    */
    
    const std::vector<std::string> pTopicNames{ "d_gps_singlePublisher" };
    const std::vector<std::string> sTopicNames{ pTopicNames };
    std::size_t numberOfFirstCallbackExpectedExecute{ 8 }; // 4 subscriber, two message publish
    std::size_t numberOfFirstCallbackExecute{};
    std::size_t numberOfSecondCallbackExpectedExecute{ 4 }; // 2 subscriber, two message publish
    std::size_t numberOfSecondCallbackExecute{};

    std::string expected{ 
        "$" + std::string(pTopicNames[0].begin() + 6, pTopicNames[0].end()) + ", 123, 456, 789, s10s" };

    SubscriberCallbackFunc firstCallback{
        [&expected, &numberOfFirstCallbackExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expected);
            numberOfFirstCallbackExecute++;
        }
    };
    SubscriberCallbackFunc secondCallback{
        [&expected, &numberOfSecondCallbackExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expected);
            numberOfSecondCallbackExecute++;
        }
    };

    RosString msg;
    msg.data = expected;
    
    Communicators<RosString> pub(pTopicNames);
    pub.timer->stop();
    Communicators<RosString> firstSub(sTopicNames[0], firstCallback);
    Communicators<RosString> secondSub(sTopicNames[0], firstCallback);
    Communicators<RosString> thirdSub(sTopicNames[0], firstCallback);
    Communicators<RosString> fourthSub(sTopicNames[0], firstCallback);

    Communicators<RosString> fifthSub(sTopicNames[0], secondCallback);
    Communicators<RosString> sixthSub(sTopicNames[0], secondCallback);

    pub.publish(msg, 1, pTopicNames[0]);

    msg.data += ", _sqwe"; 
    expected += ", _sqwe";
    pub.publish(msg, 1, pTopicNames[0]);

    EXPECT_EQ(numberOfFirstCallbackExecute, numberOfFirstCallbackExpectedExecute);
    EXPECT_EQ(numberOfSecondCallbackExecute, numberOfSecondCallbackExpectedExecute);
}
TEST(COMMUNICATORS, twoMessageMultiplePublishMultipleSubscribe)
{
      /**
     * Description:
     * Construct 4 publishers and 9 subscribers.
     * the pattern of:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * 4 subscriber have same callback and data will publishe 1 time for them.
     * 1 subscriber have a callback and data will publishe 2 times for it.
     * 2 subscriber have same callback and data will publishe 3 times for them.
     * 2 subscriber have same callback and data will publishe 5 times for them.
    */
    
    const std::vector<std::string> pTopicNames{ 
        "d_gps_lidar", "d_gps_frequency", "d_gps_timeStamp", "d_gps_xyz" };
    const std::vector<std::string> sTopicNames{ pTopicNames };
    std::vector<std::size_t> numbersOfExpectedExecute{ 
        4, // 4 subscriber, 1 message publish
        2, // 1 subscriber, 2 message publish
        6, // 2 subscriber, 3 message publish
        10 // 2 subscriber, 5 message publish
        };
    std::vector<std::size_t> numbersOfExecute(pTopicNames.size());

    std::vector<std::string> expecteds{
        { "$" + std::string(pTopicNames[0].begin() + 6, pTopicNames[0].end()) + ", 123, 456, 789, s10s" },
        { "$" + std::string(pTopicNames[1].begin() + 6, pTopicNames[1].end()) + ", 1asdc, cxac, 123_+02" },
        { "$" + std::string(pTopicNames[2].begin() + 6, pTopicNames[2].end()) + ", 13213124, 12" },
        { "$" + std::string(pTopicNames[3].begin() + 6, pTopicNames[3].end()) + ", COM4_fasd123, 1sca" }
    };

    SubscriberCallbackFunc lidarCallback{
        [&expecteds, &numbersOfExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[0]);
            numbersOfExecute[0]++;
        }
    };
    SubscriberCallbackFunc frequencyCallback{
        [&expecteds, &numbersOfExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[1]);
            numbersOfExecute[1]++;
        }
    };
    SubscriberCallbackFunc timeStampCallback{
        [&expecteds, &numbersOfExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[2]);
            numbersOfExecute[2]++;
        }
    };    
    SubscriberCallbackFunc xyzCallback{
        [&expecteds, &numbersOfExecute](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[3]);
            numbersOfExecute[3]++;
        }
    };


    std::vector<RosString> msgs(expecteds.size());
    for (std::size_t i{}; i != msgs.size(); ++i)
        msgs[i].data = expecteds[i];
    
    Communicators<RosString> pubs(pTopicNames);
    pubs.timer->stop();
    Communicators<RosString> firstSubsciberLidar(sTopicNames[0], lidarCallback);
    Communicators<RosString> secondSubscriberLidar(sTopicNames[0], lidarCallback);
    Communicators<RosString> thirdSubsciberLidar(sTopicNames[0], lidarCallback);
    Communicators<RosString> fourthSubsciberLidar(sTopicNames[0], lidarCallback);

    pubs.publish(msgs[0], 1, pTopicNames[0]);
    

    Communicators<RosString> subsciberFrequency(sTopicNames[1], frequencyCallback);

    pubs.publish(msgs[1], 1, pTopicNames[1]);

    msgs[1].data += ", _sqwe"; 
    expecteds[1] += ", _sqwe";
    pubs.publish(msgs[1], 1, pTopicNames[1]);


    Communicators<RosString> firstSubsciberTimeStamp(sTopicNames[2], timeStampCallback);
    Communicators<RosString> secondSubscriberTimeStamp(sTopicNames[2], timeStampCallback);

    pubs.publish(msgs[2], 1, pTopicNames[2]);

    msgs[2].data += ", FOO"; 
    expecteds[2] += ", FOO";
    pubs.publish(msgs[2], 1, pTopicNames[2]);


    msgs[2].data += ", asdqwe"; 
    expecteds[2] += ", asdqwe";
    pubs.publish(msgs[2], 1, pTopicNames[2]);

    Communicators<RosString> firstSubsciberXYZ(sTopicNames[3], xyzCallback);
    Communicators<RosString> secondSubscriberXYZ(sTopicNames[3], xyzCallback);

    pubs.publish(msgs[3], 1, pTopicNames[3]);

    msgs[3].data += ", BOO"; 
    expecteds[3] += ", BOO";
    pubs.publish(msgs[3], 1, pTopicNames[3]);

    msgs[3].data += ", xsw"; 
    expecteds[3] += ", xsw";
    pubs.publish(msgs[3], 1, pTopicNames[3]);

    std::size_t pos{ msgs[3].data.find("1sca") };
    if (pos != std::string::npos)
    {
        msgs[3].data.replace(pos, 4, "sca");
        expecteds[3].replace(pos, 4, "sca");
    }
    pubs.publish(msgs[3], 1, pTopicNames[3]);

    msgs[3].data += ", asdqwe"; 
    expecteds[3] += ", asdqwe";
    pubs.publish(msgs[3], 1, pTopicNames[3]);


    EXPECT_EQ(numbersOfExecute, numbersOfExpectedExecute);
}

TEST(SOCKET, connection)
{
    /**
     * Description:
     * Construct a server and a client.
     * check if the client can connect to the server.
     * Should use thread becuase both of them should run separately
     * or one of them just block the function.
    */
    std::thread serverTh{
        []()
        {
            Server srv;
            SERVER_STATUS statusSrv{ srv.start(IP_TEST, PORT_TEST) };
            EXPECT_EQ(statusSrv, SERVER_STATUS::OK);
        }
    };
    
    std::thread clientTh{
        []()
        {
            Socket cnt;
            SOCKET_STATUS statusCnt{ cnt.connectSocket(IP_TEST, PORT_TEST) };
            EXPECT_EQ(statusCnt, SOCKET_STATUS::OK);
        }
    };
    serverTh.join();
    clientTh.join();
}
TEST(SOCKET, singleMessageReceive)
{
    /**
     * Description:
     * Send a message from Server object to Socket object (which here is a client),
     * use promise/future mechanism to set the value to true when data is sent from server,
     * then use future to starting the receiving data from server.
    */
    std::promise<bool> prom;
    std::future<bool> fut{ prom.get_future() };
    const std::string expected{ "Hello from the server" };
    std::thread serverTh{
        [&prom, expected]()
        {
            Server srv;
            SERVER_STATUS statusSrv{ srv.start(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusSrv, SERVER_STATUS::OK);
            statusSrv = srv.sent(expected);
            EXPECT_EQ(statusSrv, SERVER_STATUS::OK);
            prom.set_value(statusSrv == SERVER_STATUS::OK);
        }
    };
    
    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::thread clientTh{
        [&fut, expected]()
        {
            Socket cnt;
            SOCKET_STATUS statusCnt{ cnt.connectSocket(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusCnt, SOCKET_STATUS::OK);
            std::string data;
            if (fut.get())
            {
                std::tie(statusCnt, data) = cnt.receive();
                EXPECT_EQ(statusCnt, SOCKET_STATUS::OK);
                EXPECT_EQ(data, expected);
            }
            else
            {
                FAIL() << "Server doesn't send any data!";
            }
        }
    };

    serverTh.join();
    clientTh.join();
}
TEST(SOCKET, multiMessageReceive)
{

   /**
     * Description:
     * Send 100 messages from Server object to Socket object (which here is a client),
     * use promise/future mechanism to set the value to true when data is sent from server,
     * then use future to starting the receiving data from server.
     * Note for main loops: each message send (waitForServerProms.set_value called),
     * and then server will wait (waitForClientFuts.wait will called)
     * to client receive the message (waitForClientProms.set_value called),
     * then client waits for the new message to receive (waitForServerFuts.set_value called). 
     * Also note that client should wait first because the server should send a message first.
    */
    std::size_t numberOfMessages{ 100 };
    std::vector<std::promise<void>> waitForServerProms(numberOfMessages);
    std::vector<std::future<void>> waitForServerFuts(numberOfMessages);
    std::vector<std::promise<void>> waitForClientProms(numberOfMessages);
    std::vector<std::future<void>> waitForClientFuts(numberOfMessages);
    std::transform(waitForServerProms.begin(), waitForServerProms.end(), waitForServerFuts.begin(), 
        [](std::promise<void>& p) { return p.get_future(); });
    std::transform(waitForClientProms.begin(), waitForClientProms.end(), waitForClientFuts.begin(), 
        [](std::promise<void>& p) { return p.get_future(); });

    const std::string expected{ "Hello from the server for " };
    std::thread serverTh{
        [&]()
        {
            Server srv;
            SERVER_STATUS statusSrv{ srv.start(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusSrv, SERVER_STATUS::OK);
            
            for (int i = 0; i < numberOfMessages; ++i) 
            {
                statusSrv = srv.sent(expected + std::to_string(i) + "th.");
                EXPECT_EQ(statusSrv, SERVER_STATUS::OK);
                waitForServerProms[i].set_value();
                waitForClientFuts[i].wait();
            }
        }
    };
    
    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::thread clientTh{
        [&]()
        {
            Socket cnt;
            SOCKET_STATUS statusCnt{ cnt.connectSocket(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusCnt, SOCKET_STATUS::OK);
            std::string data;
           
            for (int i = 0; i < numberOfMessages; ++i) 
            {
                waitForServerFuts[i].wait();
                std::string data;
                std::tie(statusCnt, data) = cnt.receive();
                EXPECT_EQ(statusCnt, SOCKET_STATUS::OK);
                EXPECT_EQ(data, expected + std::to_string(i) + "th.");
                waitForClientProms[i].set_value();
            }

        }
    };

    serverTh.join();
    clientTh.join();

}

TEST(GPS, receiveSingleMessageFromSocketWithSubscriber)
{
    /**
     * Description:
     * Send a message from Server to 127.0.0.1:8080,
     * receive the message with Socket object within Communicators class.
     * The Communicators object will run a ros::Timer which have a callback,
     * in that callback every 0.01 second a callback-function is called,
     * in the callback-function we get data using Socket object member variable,
     * and then search for the pattern via regex and publish it if it is correct,
     * if it was correct, callback function passed to Communicators object should called.
     * The pattern:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
    */
   
    const std::vector<std::string> pTopicName{ "d_gps_singlePublisher" };
    const std::string sTopicName{ pTopicName[0] };
    const std::string typeData(pTopicName[0].begin() + 6, pTopicName[0].end());
    std::string expected{ { "$" + typeData + ", 123, asd, 1dsq, asfg3w2, gvr32" } };

    std::thread serverTh{
        [expected]()
        {
            Server srv;
            SERVER_STATUS statusSrv{ srv.start(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusSrv, SERVER_STATUS::OK);
            statusSrv = srv.sent(expected);
            EXPECT_EQ(statusSrv, SERVER_STATUS::OK);
        }
    };

    std::this_thread::sleep_for(std::chrono::milliseconds(100)); // server be ready to bind

    bool callbackCalled{ false };
    SubscriberCallbackFunc callback{
        [expected, &callbackCalled](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expected);
            callbackCalled = true;
        }
    };
    Communicators<RosString> communicator(pTopicName, sTopicName, callback);
    EXPECT_EQ(communicator.status, SOCKET_STATUS::OK);

    std::this_thread::sleep_for(std::chrono::seconds(1)); // wait to timer to call callback function

    EXPECT_EQ(callbackCalled, true);

    serverTh.join();
}
TEST(GPS, multipleMessagesFromSocketMultiplePublisherMultipleSubscribers)
{
    /**
     * Description:
     * Send multiple message from Server to 127.0.0.1:8080,
     * receive the message with Socket object within Communicators class.
     * The Communicators object will run a ros::Timer which have a callback,
     * in that callback every 0.01 second a callback-function is called,
     * in the callback-function we get data using Socket object member variable,
     * and then search for the pattern via regex and publish it if it is correct,
     * if it was correct, callback function passed to Communicators object should called.
     * The pattern:
     * topic name: d_gsp_DesiredName
     * data: $DesiredName, xx ... or #DesiredNameA, xx ...
     * Note: in this test case, we have multiple publishers and multiple subscribers.
     * We use promise/future mechanism to send each message individually and prevent
     * messages to concatenated.
    */
   
    std::vector<std::promise<void>> received(3);
    std::vector<std::future<void>> waitForRecevied(received.size());
    std::transform(received.begin(), received.end(), waitForRecevied.begin(),
        [](std::promise<void>& rec) { return rec.get_future(); }
    );
    const std::vector<std::string> pTopicNames{ 
        "d_gps_BYINS", "d_gps_INSPVAX", "d_gps_INSSTDEV" };
    const std::vector<std::string> sTopicNames{ pTopicNames };
    std::vector<std::string> typeDatas(pTopicNames.size());
    std::transform(pTopicNames.begin(), pTopicNames.end(), std::back_inserter(typeDatas),
        [](const auto& pTopicName) { return std::string(pTopicName.begin() + 6, pTopicName.end()); }
    );
    std::vector<std::string> expecteds{ 
        "$BYINS,SN101133140136,021938.17,94796.165,28.232455223,112.874930648,71.093,10.127, -0.040,1.424,0.002,0.003,-0.001,-0.247,0.019,9.817,0.016,0.084,0.158,-0.010,-0.006,-0.010,6,4,54,1000000,0,0.000,0,0.003,0.010,0.001,112.8749301,28.2324561,69.22,1,000000,0.003,0.002,-0.001*57",
        "#INSPVAXA,ICOM4,0,0.0,FINESTEERING,2107,35489.000,00000000,03de,68;INS_ALIGNMENT_COMPLETE,INS_RTKFIXED,28.23316396165,112.87713086609,82.7966,-17.0382,0.0020,-0.0191,0.0006,179.789714292,-0.387541550,1.405962922,0.0240,0.0168,0.0218,0.0047,0.0049,0.0054,0.0553,0.0553,1.0818,00000000,0*fd6e3a89", 
        "#INSSTDEVA,ICOM4,0,0.0,FINESTEERING,2107,37213.000,00000000,0000,68;0.0239,0.0168,  0.0220,0.0068,0.0067,0.0057,0.0497,0.0497,1.0741,00000000,0,0,00bffbbf,0*c607c0d6" };

    std::thread serverTh{
        [expecteds, &waitForRecevied]()
        {
            Server srv;
            SERVER_STATUS statusSrv{ srv.start(IP_TEST, PORT_TEST) };
            ASSERT_EQ(statusSrv, SERVER_STATUS::OK);
            for (std::size_t i{}; i != expecteds.size(); ++i)
            {
                statusSrv = srv.sent(expecteds[i]);
                EXPECT_EQ(statusSrv, SERVER_STATUS::OK);
                waitForRecevied[i].wait();
            }
        }
    };

    std::this_thread::sleep_for(std::chrono::milliseconds(100)); // server be ready to bind

    bool callbackBYINSCalled{ false };
    SubscriberCallbackFunc callbackBYINS{
        [expecteds, &callbackBYINSCalled, &received](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[0]);
            callbackBYINSCalled = true;
            received[0].set_value();
        }
    };
    bool callbackINSPVAXCalled{ false };
    SubscriberCallbackFunc callbackINSPVAX{
        [expecteds, &callbackINSPVAXCalled, &received](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[1]);
            callbackINSPVAXCalled = true;
            received[1].set_value();
        }
    };
    bool callbackINSSTDEVCalled{ false };
    SubscriberCallbackFunc callbackINSSTDEV{
        [expecteds, &callbackINSSTDEVCalled, &received](const RosString::ConstPtr& msg)
        {
            EXPECT_EQ(msg->data, expecteds[2]);
            callbackINSSTDEVCalled = true;
            received[2].set_value();
        }
    };
    Communicators<RosString> communicators(pTopicNames);
    EXPECT_EQ(communicators.status, SOCKET_STATUS::OK);

    Communicators<RosString> subscriberBYINS(sTopicNames[0], callbackBYINS);
    Communicators<RosString> subscriberINSPVAX(sTopicNames[1], callbackINSPVAX);
    Communicators<RosString> subscriberINSSTDEV(sTopicNames[2], callbackINSSTDEV);

    std::this_thread::sleep_for(std::chrono::seconds(1)); // wait to timer to call callback function

    EXPECT_EQ(callbackBYINSCalled, true);
    EXPECT_EQ(callbackINSPVAXCalled, true);
    EXPECT_EQ(callbackINSSTDEVCalled, true);

    serverTh.join();
}

#endif // DISABLE

// TEST(SOCKET, Send)
// {
// }

// TEST(COMMUNICATORS, multipleSubscribersMultiplePublisher)
// {
// }


int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    ros::init(argc, argv, "Unit_Test");
    
    std::thread t{ [](){ while(ros::ok()) ros::spin();} };

    auto res = RUN_ALL_TESTS();

    ros::shutdown();
    
    return res;
}
