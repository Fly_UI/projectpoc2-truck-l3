// Auto-generated. Do not edit!

// (in-package robot_gps.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class integratedNavigationMsg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.GPSWeek = null;
      this.GPSTime = null;
      this.Heading = null;
      this.Pitch = null;
      this.Roll = null;
      this.gyroX = null;
      this.gyroY = null;
      this.gyroZ = null;
      this.accX = null;
      this.accY = null;
      this.accZ = null;
      this.Lattitude = null;
      this.Longitude = null;
      this.Altitude = null;
      this.Vx = null;
      this.Vy = null;
      this.Vz = null;
      this.angular_velocity_X = null;
      this.angular_velocity_Y = null;
      this.angular_velocity_Z = null;
      this.Baseline = null;
      this.NSV1 = null;
      this.NSV2 = null;
      this.Status = null;
      this.Age = null;
      this.warning = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('GPSWeek')) {
        this.GPSWeek = initObj.GPSWeek
      }
      else {
        this.GPSWeek = 0;
      }
      if (initObj.hasOwnProperty('GPSTime')) {
        this.GPSTime = initObj.GPSTime
      }
      else {
        this.GPSTime = 0.0;
      }
      if (initObj.hasOwnProperty('Heading')) {
        this.Heading = initObj.Heading
      }
      else {
        this.Heading = 0.0;
      }
      if (initObj.hasOwnProperty('Pitch')) {
        this.Pitch = initObj.Pitch
      }
      else {
        this.Pitch = 0.0;
      }
      if (initObj.hasOwnProperty('Roll')) {
        this.Roll = initObj.Roll
      }
      else {
        this.Roll = 0.0;
      }
      if (initObj.hasOwnProperty('gyroX')) {
        this.gyroX = initObj.gyroX
      }
      else {
        this.gyroX = 0.0;
      }
      if (initObj.hasOwnProperty('gyroY')) {
        this.gyroY = initObj.gyroY
      }
      else {
        this.gyroY = 0.0;
      }
      if (initObj.hasOwnProperty('gyroZ')) {
        this.gyroZ = initObj.gyroZ
      }
      else {
        this.gyroZ = 0.0;
      }
      if (initObj.hasOwnProperty('accX')) {
        this.accX = initObj.accX
      }
      else {
        this.accX = 0.0;
      }
      if (initObj.hasOwnProperty('accY')) {
        this.accY = initObj.accY
      }
      else {
        this.accY = 0.0;
      }
      if (initObj.hasOwnProperty('accZ')) {
        this.accZ = initObj.accZ
      }
      else {
        this.accZ = 0.0;
      }
      if (initObj.hasOwnProperty('Lattitude')) {
        this.Lattitude = initObj.Lattitude
      }
      else {
        this.Lattitude = 0.0;
      }
      if (initObj.hasOwnProperty('Longitude')) {
        this.Longitude = initObj.Longitude
      }
      else {
        this.Longitude = 0.0;
      }
      if (initObj.hasOwnProperty('Altitude')) {
        this.Altitude = initObj.Altitude
      }
      else {
        this.Altitude = 0.0;
      }
      if (initObj.hasOwnProperty('Vx')) {
        this.Vx = initObj.Vx
      }
      else {
        this.Vx = 0.0;
      }
      if (initObj.hasOwnProperty('Vy')) {
        this.Vy = initObj.Vy
      }
      else {
        this.Vy = 0.0;
      }
      if (initObj.hasOwnProperty('Vz')) {
        this.Vz = initObj.Vz
      }
      else {
        this.Vz = 0.0;
      }
      if (initObj.hasOwnProperty('angular_velocity_X')) {
        this.angular_velocity_X = initObj.angular_velocity_X
      }
      else {
        this.angular_velocity_X = 0.0;
      }
      if (initObj.hasOwnProperty('angular_velocity_Y')) {
        this.angular_velocity_Y = initObj.angular_velocity_Y
      }
      else {
        this.angular_velocity_Y = 0.0;
      }
      if (initObj.hasOwnProperty('angular_velocity_Z')) {
        this.angular_velocity_Z = initObj.angular_velocity_Z
      }
      else {
        this.angular_velocity_Z = 0.0;
      }
      if (initObj.hasOwnProperty('Baseline')) {
        this.Baseline = initObj.Baseline
      }
      else {
        this.Baseline = 0.0;
      }
      if (initObj.hasOwnProperty('NSV1')) {
        this.NSV1 = initObj.NSV1
      }
      else {
        this.NSV1 = 0;
      }
      if (initObj.hasOwnProperty('NSV2')) {
        this.NSV2 = initObj.NSV2
      }
      else {
        this.NSV2 = 0;
      }
      if (initObj.hasOwnProperty('Status')) {
        this.Status = initObj.Status
      }
      else {
        this.Status = 0;
      }
      if (initObj.hasOwnProperty('Age')) {
        this.Age = initObj.Age
      }
      else {
        this.Age = 0.0;
      }
      if (initObj.hasOwnProperty('warning')) {
        this.warning = initObj.warning
      }
      else {
        this.warning = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type integratedNavigationMsg
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [GPSWeek]
    bufferOffset = _serializer.int32(obj.GPSWeek, buffer, bufferOffset);
    // Serialize message field [GPSTime]
    bufferOffset = _serializer.float64(obj.GPSTime, buffer, bufferOffset);
    // Serialize message field [Heading]
    bufferOffset = _serializer.float64(obj.Heading, buffer, bufferOffset);
    // Serialize message field [Pitch]
    bufferOffset = _serializer.float64(obj.Pitch, buffer, bufferOffset);
    // Serialize message field [Roll]
    bufferOffset = _serializer.float64(obj.Roll, buffer, bufferOffset);
    // Serialize message field [gyroX]
    bufferOffset = _serializer.float64(obj.gyroX, buffer, bufferOffset);
    // Serialize message field [gyroY]
    bufferOffset = _serializer.float64(obj.gyroY, buffer, bufferOffset);
    // Serialize message field [gyroZ]
    bufferOffset = _serializer.float64(obj.gyroZ, buffer, bufferOffset);
    // Serialize message field [accX]
    bufferOffset = _serializer.float64(obj.accX, buffer, bufferOffset);
    // Serialize message field [accY]
    bufferOffset = _serializer.float64(obj.accY, buffer, bufferOffset);
    // Serialize message field [accZ]
    bufferOffset = _serializer.float64(obj.accZ, buffer, bufferOffset);
    // Serialize message field [Lattitude]
    bufferOffset = _serializer.float64(obj.Lattitude, buffer, bufferOffset);
    // Serialize message field [Longitude]
    bufferOffset = _serializer.float64(obj.Longitude, buffer, bufferOffset);
    // Serialize message field [Altitude]
    bufferOffset = _serializer.float64(obj.Altitude, buffer, bufferOffset);
    // Serialize message field [Vx]
    bufferOffset = _serializer.float64(obj.Vx, buffer, bufferOffset);
    // Serialize message field [Vy]
    bufferOffset = _serializer.float64(obj.Vy, buffer, bufferOffset);
    // Serialize message field [Vz]
    bufferOffset = _serializer.float64(obj.Vz, buffer, bufferOffset);
    // Serialize message field [angular_velocity_X]
    bufferOffset = _serializer.float64(obj.angular_velocity_X, buffer, bufferOffset);
    // Serialize message field [angular_velocity_Y]
    bufferOffset = _serializer.float64(obj.angular_velocity_Y, buffer, bufferOffset);
    // Serialize message field [angular_velocity_Z]
    bufferOffset = _serializer.float64(obj.angular_velocity_Z, buffer, bufferOffset);
    // Serialize message field [Baseline]
    bufferOffset = _serializer.float64(obj.Baseline, buffer, bufferOffset);
    // Serialize message field [NSV1]
    bufferOffset = _serializer.int32(obj.NSV1, buffer, bufferOffset);
    // Serialize message field [NSV2]
    bufferOffset = _serializer.int32(obj.NSV2, buffer, bufferOffset);
    // Serialize message field [Status]
    bufferOffset = _serializer.int8(obj.Status, buffer, bufferOffset);
    // Serialize message field [Age]
    bufferOffset = _serializer.float32(obj.Age, buffer, bufferOffset);
    // Serialize message field [warning]
    bufferOffset = _serializer.int8(obj.warning, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type integratedNavigationMsg
    let len;
    let data = new integratedNavigationMsg(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [GPSWeek]
    data.GPSWeek = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [GPSTime]
    data.GPSTime = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Heading]
    data.Heading = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Pitch]
    data.Pitch = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Roll]
    data.Roll = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [gyroX]
    data.gyroX = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [gyroY]
    data.gyroY = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [gyroZ]
    data.gyroZ = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [accX]
    data.accX = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [accY]
    data.accY = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [accZ]
    data.accZ = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Lattitude]
    data.Lattitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Longitude]
    data.Longitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Altitude]
    data.Altitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Vx]
    data.Vx = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Vy]
    data.Vy = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Vz]
    data.Vz = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [angular_velocity_X]
    data.angular_velocity_X = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [angular_velocity_Y]
    data.angular_velocity_Y = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [angular_velocity_Z]
    data.angular_velocity_Z = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [Baseline]
    data.Baseline = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [NSV1]
    data.NSV1 = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [NSV2]
    data.NSV2 = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [Status]
    data.Status = _deserializer.int8(buffer, bufferOffset);
    // Deserialize message field [Age]
    data.Age = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [warning]
    data.warning = _deserializer.int8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 178;
  }

  static datatype() {
    // Returns string type for a message object
    return 'robot_gps/integratedNavigationMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ce22ad639e3f707d496653c9369e2ae8';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    int32 GPSWeek
    float64 GPSTime
    float64 Heading
    float64 Pitch
    float64 Roll
    float64 gyroX
    float64 gyroY
    float64 gyroZ
    float64 accX
    float64 accY
    float64 accZ
    float64 Lattitude
    float64 Longitude
    float64 Altitude
    float64 Vx # velocity towards east
    float64 Vy # velocity towards north
    float64 Vz
    float64 angular_velocity_X
    float64 angular_velocity_Y
    float64 angular_velocity_Z
    float64 Baseline # Velocity of vehicle
    int32 NSV1 # number of the settlelites
    int32 NSV2
    int8 Status
    float32 Age
    int8 warning
    
    
    
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new integratedNavigationMsg(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.GPSWeek !== undefined) {
      resolved.GPSWeek = msg.GPSWeek;
    }
    else {
      resolved.GPSWeek = 0
    }

    if (msg.GPSTime !== undefined) {
      resolved.GPSTime = msg.GPSTime;
    }
    else {
      resolved.GPSTime = 0.0
    }

    if (msg.Heading !== undefined) {
      resolved.Heading = msg.Heading;
    }
    else {
      resolved.Heading = 0.0
    }

    if (msg.Pitch !== undefined) {
      resolved.Pitch = msg.Pitch;
    }
    else {
      resolved.Pitch = 0.0
    }

    if (msg.Roll !== undefined) {
      resolved.Roll = msg.Roll;
    }
    else {
      resolved.Roll = 0.0
    }

    if (msg.gyroX !== undefined) {
      resolved.gyroX = msg.gyroX;
    }
    else {
      resolved.gyroX = 0.0
    }

    if (msg.gyroY !== undefined) {
      resolved.gyroY = msg.gyroY;
    }
    else {
      resolved.gyroY = 0.0
    }

    if (msg.gyroZ !== undefined) {
      resolved.gyroZ = msg.gyroZ;
    }
    else {
      resolved.gyroZ = 0.0
    }

    if (msg.accX !== undefined) {
      resolved.accX = msg.accX;
    }
    else {
      resolved.accX = 0.0
    }

    if (msg.accY !== undefined) {
      resolved.accY = msg.accY;
    }
    else {
      resolved.accY = 0.0
    }

    if (msg.accZ !== undefined) {
      resolved.accZ = msg.accZ;
    }
    else {
      resolved.accZ = 0.0
    }

    if (msg.Lattitude !== undefined) {
      resolved.Lattitude = msg.Lattitude;
    }
    else {
      resolved.Lattitude = 0.0
    }

    if (msg.Longitude !== undefined) {
      resolved.Longitude = msg.Longitude;
    }
    else {
      resolved.Longitude = 0.0
    }

    if (msg.Altitude !== undefined) {
      resolved.Altitude = msg.Altitude;
    }
    else {
      resolved.Altitude = 0.0
    }

    if (msg.Vx !== undefined) {
      resolved.Vx = msg.Vx;
    }
    else {
      resolved.Vx = 0.0
    }

    if (msg.Vy !== undefined) {
      resolved.Vy = msg.Vy;
    }
    else {
      resolved.Vy = 0.0
    }

    if (msg.Vz !== undefined) {
      resolved.Vz = msg.Vz;
    }
    else {
      resolved.Vz = 0.0
    }

    if (msg.angular_velocity_X !== undefined) {
      resolved.angular_velocity_X = msg.angular_velocity_X;
    }
    else {
      resolved.angular_velocity_X = 0.0
    }

    if (msg.angular_velocity_Y !== undefined) {
      resolved.angular_velocity_Y = msg.angular_velocity_Y;
    }
    else {
      resolved.angular_velocity_Y = 0.0
    }

    if (msg.angular_velocity_Z !== undefined) {
      resolved.angular_velocity_Z = msg.angular_velocity_Z;
    }
    else {
      resolved.angular_velocity_Z = 0.0
    }

    if (msg.Baseline !== undefined) {
      resolved.Baseline = msg.Baseline;
    }
    else {
      resolved.Baseline = 0.0
    }

    if (msg.NSV1 !== undefined) {
      resolved.NSV1 = msg.NSV1;
    }
    else {
      resolved.NSV1 = 0
    }

    if (msg.NSV2 !== undefined) {
      resolved.NSV2 = msg.NSV2;
    }
    else {
      resolved.NSV2 = 0
    }

    if (msg.Status !== undefined) {
      resolved.Status = msg.Status;
    }
    else {
      resolved.Status = 0
    }

    if (msg.Age !== undefined) {
      resolved.Age = msg.Age;
    }
    else {
      resolved.Age = 0.0
    }

    if (msg.warning !== undefined) {
      resolved.warning = msg.warning;
    }
    else {
      resolved.warning = 0
    }

    return resolved;
    }
};

module.exports = integratedNavigationMsg;
