
"use strict";

let gpsmsg = require('./gpsmsg.js');
let integratedNavigationMsg = require('./integratedNavigationMsg.js');

module.exports = {
  gpsmsg: gpsmsg,
  integratedNavigationMsg: integratedNavigationMsg,
};
