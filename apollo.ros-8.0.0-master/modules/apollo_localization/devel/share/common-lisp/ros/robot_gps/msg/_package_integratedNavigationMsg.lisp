(cl:in-package robot_gps-msg)
(cl:export '(HEADER-VAL
          HEADER
          GPSWEEK-VAL
          GPSWEEK
          GPSTIME-VAL
          GPSTIME
          HEADING-VAL
          HEADING
          PITCH-VAL
          PITCH
          ROLL-VAL
          ROLL
          GYROX-VAL
          GYROX
          GYROY-VAL
          GYROY
          GYROZ-VAL
          GYROZ
          ACCX-VAL
          ACCX
          ACCY-VAL
          ACCY
          ACCZ-VAL
          ACCZ
          LATTITUDE-VAL
          LATTITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
          VX-VAL
          VX
          VY-VAL
          VY
          VZ-VAL
          VZ
          ANGULAR_VELOCITY_X-VAL
          ANGULAR_VELOCITY_X
          ANGULAR_VELOCITY_Y-VAL
          ANGULAR_VELOCITY_Y
          ANGULAR_VELOCITY_Z-VAL
          ANGULAR_VELOCITY_Z
          BASELINE-VAL
          BASELINE
          NSV1-VAL
          NSV1
          NSV2-VAL
          NSV2
          STATUS-VAL
          STATUS
          AGE-VAL
          AGE
          WARNING-VAL
          WARNING
))