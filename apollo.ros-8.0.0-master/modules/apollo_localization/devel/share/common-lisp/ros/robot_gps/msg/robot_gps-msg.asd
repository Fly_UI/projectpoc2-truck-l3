
(cl:in-package :asdf)

(defsystem "robot_gps-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "gpsmsg" :depends-on ("_package_gpsmsg"))
    (:file "_package_gpsmsg" :depends-on ("_package"))
    (:file "integratedNavigationMsg" :depends-on ("_package_integratedNavigationMsg"))
    (:file "_package_integratedNavigationMsg" :depends-on ("_package"))
  ))