# CMake generated Testfile for 
# Source directory: /home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/src
# Build directory: /home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("GPS_bag/src/gps")
subdirs("GPS_bag/src/publisher")
subdirs("GPS_bag/src/robot_gps")
