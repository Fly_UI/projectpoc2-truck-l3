# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;std_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrobot_gps".split(';') if "-lrobot_gps" != "" else []
PROJECT_NAME = "robot_gps"
PROJECT_SPACE_DIR = "/home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/install"
PROJECT_VERSION = "0.0.0"
