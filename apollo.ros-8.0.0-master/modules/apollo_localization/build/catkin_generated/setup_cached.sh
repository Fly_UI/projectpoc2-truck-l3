#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH='/opt/ros/noetic/lib:/home/hui/mpfr-4.0.2/lib'
export PWD='/home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/hui/code/apollo.ros-8.0.0-master/modules/apollo_localization/src:$ROS_PACKAGE_PATH"