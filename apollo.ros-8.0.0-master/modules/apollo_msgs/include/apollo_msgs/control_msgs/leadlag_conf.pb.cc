// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: apollo_msgs/control_msgs/leadlag_conf.proto

#include "apollo_msgs/control_msgs/leadlag_conf.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// This is a temporary google only hack
#ifdef GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
#include "third_party/protobuf/version.h"
#endif
// @@protoc_insertion_point(includes)

namespace apollo {
namespace control {
class LeadlagConfDefaultTypeInternal {
 public:
  ::google::protobuf::internal::ExplicitlyConstructed<LeadlagConf>
      _instance;
} _LeadlagConf_default_instance_;
}  // namespace control
}  // namespace apollo
namespace protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto {
static void InitDefaultsLeadlagConf() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::apollo::control::_LeadlagConf_default_instance_;
    new (ptr) ::apollo::control::LeadlagConf();
    ::google::protobuf::internal::OnShutdownDestroyMessage(ptr);
  }
  ::apollo::control::LeadlagConf::InitAsDefaultInstance();
}

::google::protobuf::internal::SCCInfo<0> scc_info_LeadlagConf =
    {{ATOMIC_VAR_INIT(::google::protobuf::internal::SCCInfoBase::kUninitialized), 0, InitDefaultsLeadlagConf}, {}};

void InitDefaults() {
  ::google::protobuf::internal::InitSCC(&scc_info_LeadlagConf.base);
}

::google::protobuf::Metadata file_level_metadata[1];

const ::google::protobuf::uint32 TableStruct::offsets[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, _has_bits_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, innerstate_saturation_level_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, alpha_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, beta_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::apollo::control::LeadlagConf, tau_),
  1,
  2,
  3,
  0,
};
static const ::google::protobuf::internal::MigrationSchema schemas[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 9, sizeof(::apollo::control::LeadlagConf)},
};

static ::google::protobuf::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::google::protobuf::Message*>(&::apollo::control::_LeadlagConf_default_instance_),
};

void protobuf_AssignDescriptors() {
  AddDescriptors();
  AssignDescriptors(
      "apollo_msgs/control_msgs/leadlag_conf.proto", schemas, file_default_instances, TableStruct::offsets,
      file_level_metadata, NULL, NULL);
}

void protobuf_AssignDescriptorsOnce() {
  static ::google::protobuf::internal::once_flag once;
  ::google::protobuf::internal::call_once(once, protobuf_AssignDescriptors);
}

void protobuf_RegisterTypes(const ::std::string&) GOOGLE_PROTOBUF_ATTRIBUTE_COLD;
void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::internal::RegisterAllTypes(file_level_metadata, 1);
}

void AddDescriptorsImpl() {
  InitDefaults();
  static const char descriptor[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
      "\n+apollo_msgs/control_msgs/leadlag_conf."
      "proto\022\016apollo.control\"l\n\013LeadlagConf\022(\n\033"
      "innerstate_saturation_level\030\001 \001(\001:\003300\022\022"
      "\n\005alpha\030\002 \001(\001:\0030.1\022\017\n\004beta\030\003 \001(\001:\0011\022\016\n\003t"
      "au\030\004 \001(\001:\0010"
  };
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
      descriptor, 171);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "apollo_msgs/control_msgs/leadlag_conf.proto", &protobuf_RegisterTypes);
}

void AddDescriptors() {
  static ::google::protobuf::internal::once_flag once;
  ::google::protobuf::internal::call_once(once, AddDescriptorsImpl);
}
// Force AddDescriptors() to be called at dynamic initialization time.
struct StaticDescriptorInitializer {
  StaticDescriptorInitializer() {
    AddDescriptors();
  }
} static_descriptor_initializer;
}  // namespace protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto
namespace apollo {
namespace control {

// ===================================================================

void LeadlagConf::InitAsDefaultInstance() {
}
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int LeadlagConf::kInnerstateSaturationLevelFieldNumber;
const int LeadlagConf::kAlphaFieldNumber;
const int LeadlagConf::kBetaFieldNumber;
const int LeadlagConf::kTauFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

LeadlagConf::LeadlagConf()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  ::google::protobuf::internal::InitSCC(
      &protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::scc_info_LeadlagConf.base);
  SharedCtor();
  // @@protoc_insertion_point(constructor:apollo.control.LeadlagConf)
}
LeadlagConf::LeadlagConf(const LeadlagConf& from)
  : ::google::protobuf::Message(),
      _internal_metadata_(NULL),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::memcpy(&tau_, &from.tau_,
    static_cast<size_t>(reinterpret_cast<char*>(&beta_) -
    reinterpret_cast<char*>(&tau_)) + sizeof(beta_));
  // @@protoc_insertion_point(copy_constructor:apollo.control.LeadlagConf)
}

void LeadlagConf::SharedCtor() {
  tau_ = 0;
  innerstate_saturation_level_ = 300;
  alpha_ = 0.1;
  beta_ = 1;
}

LeadlagConf::~LeadlagConf() {
  // @@protoc_insertion_point(destructor:apollo.control.LeadlagConf)
  SharedDtor();
}

void LeadlagConf::SharedDtor() {
}

void LeadlagConf::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ::google::protobuf::Descriptor* LeadlagConf::descriptor() {
  ::protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::file_level_metadata[kIndexInFileMessages].descriptor;
}

const LeadlagConf& LeadlagConf::default_instance() {
  ::google::protobuf::internal::InitSCC(&protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::scc_info_LeadlagConf.base);
  return *internal_default_instance();
}


void LeadlagConf::Clear() {
// @@protoc_insertion_point(message_clear_start:apollo.control.LeadlagConf)
  ::google::protobuf::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 15u) {
    tau_ = 0;
    innerstate_saturation_level_ = 300;
    alpha_ = 0.1;
    beta_ = 1;
  }
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

bool LeadlagConf::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:apollo.control.LeadlagConf)
  for (;;) {
    ::std::pair<::google::protobuf::uint32, bool> p = input->ReadTagWithCutoffNoLastTag(127u);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional double innerstate_saturation_level = 1 [default = 300];
      case 1: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(9u /* 9 & 0xFF */)) {
          set_has_innerstate_saturation_level();
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &innerstate_saturation_level_)));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional double alpha = 2 [default = 0.1];
      case 2: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(17u /* 17 & 0xFF */)) {
          set_has_alpha();
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &alpha_)));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional double beta = 3 [default = 1];
      case 3: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(25u /* 25 & 0xFF */)) {
          set_has_beta();
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &beta_)));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional double tau = 4 [default = 0];
      case 4: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(33u /* 33 & 0xFF */)) {
          set_has_tau();
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &tau_)));
        } else {
          goto handle_unusual;
        }
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, _internal_metadata_.mutable_unknown_fields()));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:apollo.control.LeadlagConf)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:apollo.control.LeadlagConf)
  return false;
#undef DO_
}

void LeadlagConf::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:apollo.control.LeadlagConf)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional double innerstate_saturation_level = 1 [default = 300];
  if (cached_has_bits & 0x00000002u) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(1, this->innerstate_saturation_level(), output);
  }

  // optional double alpha = 2 [default = 0.1];
  if (cached_has_bits & 0x00000004u) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(2, this->alpha(), output);
  }

  // optional double beta = 3 [default = 1];
  if (cached_has_bits & 0x00000008u) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(3, this->beta(), output);
  }

  // optional double tau = 4 [default = 0];
  if (cached_has_bits & 0x00000001u) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(4, this->tau(), output);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        _internal_metadata_.unknown_fields(), output);
  }
  // @@protoc_insertion_point(serialize_end:apollo.control.LeadlagConf)
}

::google::protobuf::uint8* LeadlagConf::InternalSerializeWithCachedSizesToArray(
    bool deterministic, ::google::protobuf::uint8* target) const {
  (void)deterministic; // Unused
  // @@protoc_insertion_point(serialize_to_array_start:apollo.control.LeadlagConf)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional double innerstate_saturation_level = 1 [default = 300];
  if (cached_has_bits & 0x00000002u) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(1, this->innerstate_saturation_level(), target);
  }

  // optional double alpha = 2 [default = 0.1];
  if (cached_has_bits & 0x00000004u) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(2, this->alpha(), target);
  }

  // optional double beta = 3 [default = 1];
  if (cached_has_bits & 0x00000008u) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(3, this->beta(), target);
  }

  // optional double tau = 4 [default = 0];
  if (cached_has_bits & 0x00000001u) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(4, this->tau(), target);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:apollo.control.LeadlagConf)
  return target;
}

size_t LeadlagConf::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:apollo.control.LeadlagConf)
  size_t total_size = 0;

  if (_internal_metadata_.have_unknown_fields()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        _internal_metadata_.unknown_fields());
  }
  if (_has_bits_[0 / 32] & 15u) {
    // optional double tau = 4 [default = 0];
    if (has_tau()) {
      total_size += 1 + 8;
    }

    // optional double innerstate_saturation_level = 1 [default = 300];
    if (has_innerstate_saturation_level()) {
      total_size += 1 + 8;
    }

    // optional double alpha = 2 [default = 0.1];
    if (has_alpha()) {
      total_size += 1 + 8;
    }

    // optional double beta = 3 [default = 1];
    if (has_beta()) {
      total_size += 1 + 8;
    }

  }
  int cached_size = ::google::protobuf::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void LeadlagConf::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:apollo.control.LeadlagConf)
  GOOGLE_DCHECK_NE(&from, this);
  const LeadlagConf* source =
      ::google::protobuf::internal::DynamicCastToGenerated<const LeadlagConf>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:apollo.control.LeadlagConf)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:apollo.control.LeadlagConf)
    MergeFrom(*source);
  }
}

void LeadlagConf::MergeFrom(const LeadlagConf& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:apollo.control.LeadlagConf)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 15u) {
    if (cached_has_bits & 0x00000001u) {
      tau_ = from.tau_;
    }
    if (cached_has_bits & 0x00000002u) {
      innerstate_saturation_level_ = from.innerstate_saturation_level_;
    }
    if (cached_has_bits & 0x00000004u) {
      alpha_ = from.alpha_;
    }
    if (cached_has_bits & 0x00000008u) {
      beta_ = from.beta_;
    }
    _has_bits_[0] |= cached_has_bits;
  }
}

void LeadlagConf::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:apollo.control.LeadlagConf)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void LeadlagConf::CopyFrom(const LeadlagConf& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:apollo.control.LeadlagConf)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool LeadlagConf::IsInitialized() const {
  return true;
}

void LeadlagConf::Swap(LeadlagConf* other) {
  if (other == this) return;
  InternalSwap(other);
}
void LeadlagConf::InternalSwap(LeadlagConf* other) {
  using std::swap;
  swap(tau_, other->tau_);
  swap(innerstate_saturation_level_, other->innerstate_saturation_level_);
  swap(alpha_, other->alpha_);
  swap(beta_, other->beta_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  _internal_metadata_.Swap(&other->_internal_metadata_);
}

::google::protobuf::Metadata LeadlagConf::GetMetadata() const {
  protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_apollo_5fmsgs_2fcontrol_5fmsgs_2fleadlag_5fconf_2eproto::file_level_metadata[kIndexInFileMessages];
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace control
}  // namespace apollo
namespace google {
namespace protobuf {
template<> GOOGLE_PROTOBUF_ATTRIBUTE_NOINLINE ::apollo::control::LeadlagConf* Arena::CreateMaybeMessage< ::apollo::control::LeadlagConf >(Arena* arena) {
  return Arena::CreateInternal< ::apollo::control::LeadlagConf >(arena);
}
}  // namespace protobuf
}  // namespace google

// @@protoc_insertion_point(global_scope)
