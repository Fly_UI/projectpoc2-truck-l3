/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_simulation/vehicle_sim_model/sim_model_ideal.h"

SimModelIdealTwist::SimModelIdealTwist()
  : SimModelInterface(3 /* dim x */, 2 /* dim u */) {}

double SimModelIdealTwist::GetX() {
  return state_(IDX::X);
}
double SimModelIdealTwist::GetY() {
  return state_(IDX::Y);
}
double SimModelIdealTwist::GetYaw() {
  return state_(IDX::YAW);
}
double SimModelIdealTwist::GetVx() {
  return input_(IDX_U::VX_DES);
}
double SimModelIdealTwist::GetWz() {
  return input_(IDX_U::WZ_DES);
}
double SimModelIdealTwist::GetACCx() {
  return 0.0;
}
double SimModelIdealTwist::GetSteer() {
  return 0.0;
}
void SimModelIdealTwist::Update(const double &dt) {
  UpdateRungeKutta(dt, input_);
}
Eigen::VectorXd SimModelIdealTwist::CalcModel(
  const Eigen::VectorXd &state, const Eigen::VectorXd &input) {
  const double yaw = state(IDX::YAW);
  const double vx = input(IDX_U::VX_DES);
  const double wz = input(IDX_U::WZ_DES);

  Eigen::VectorXd d_state = Eigen::VectorXd::Zero(dim_x_);
  d_state(IDX::X) = vx * cos(yaw);
  d_state(IDX::Y) = vx * sin(yaw);
  d_state(IDX::YAW) = wz;

  return d_state;
}

SimModelIdealSteer::SimModelIdealSteer(double wheelbase)
  : SimModelInterface(3 /* dim x */, 2 /* dim u */), wheelbase_(wheelbase) {}

double SimModelIdealSteer::GetX() {
  return state_(IDX::X);
}
double SimModelIdealSteer::GetY() {
  return state_(IDX::Y);
}
double SimModelIdealSteer::GetYaw() {
  return state_(IDX::YAW);
}
double SimModelIdealSteer::GetVx() {
  return input_(IDX_U::VX_DES);
}
double SimModelIdealSteer::GetWz() {
  return input_(IDX_U::VX_DES) * std::tan(input_(IDX_U::STEER_DES)) / wheelbase_;
}
double SimModelIdealSteer::GetACCx() {
  return 0.0;
}
double SimModelIdealSteer::GetSteer() {
  return input_(IDX_U::STEER_DES);
}
void SimModelIdealSteer::Update(const double &dt) {
  UpdateRungeKutta(dt, input_);
}
Eigen::VectorXd SimModelIdealSteer::CalcModel(
  const Eigen::VectorXd &state, const Eigen::VectorXd &input) {
  const double yaw = state(IDX::YAW);
  const double vx = input(IDX_U::VX_DES);
  const double steer = input(IDX_U::STEER_DES);

  Eigen::VectorXd d_state = Eigen::VectorXd::Zero(dim_x_);
  d_state(IDX::X) = vx * cos(yaw);
  d_state(IDX::Y) = vx * sin(yaw);
  d_state(IDX::YAW) = vx * std::tan(steer) / wheelbase_;

  return d_state;
}

SimModelIdealAccel::SimModelIdealAccel(double wheelbase)
  : SimModelInterface(4 /* dim x */, 2 /* dim u */), wheelbase_(wheelbase) {}

double SimModelIdealAccel::GetX() {
  return state_(IDX::X);
}
double SimModelIdealAccel::GetY() {
  return state_(IDX::Y);
}
double SimModelIdealAccel::GetYaw() {
  return state_(IDX::YAW);
}
double SimModelIdealAccel::GetVx() {
  return state_(IDX::VX);
}
double SimModelIdealAccel::GetWz() {
  return state_(IDX::VX) * std::tan(input_(IDX_U::STEER_DES)) / wheelbase_;
}
double SimModelIdealAccel::GetACCx() {
  return 0.0;
}
double SimModelIdealAccel::GetSteer() {
  return input_(IDX_U::STEER_DES);
}
void SimModelIdealAccel::Update(const double &dt) {
  UpdateRungeKutta(dt, input_);
  if (state_(IDX::VX) < 0) {
    state_(IDX::VX) = 0;
  }
}

Eigen::VectorXd SimModelIdealAccel::CalcModel(
  const Eigen::VectorXd &state, const Eigen::VectorXd &input) {
  const double vx = state(IDX::VX);
  const double yaw = state(IDX::YAW);
  const double ax = input(IDX_U::AX_DES);
  const double steer = input(IDX_U::STEER_DES);

  Eigen::VectorXd d_state = Eigen::VectorXd::Zero(dim_x_);
  d_state(IDX::X) = vx * cos(yaw);
  d_state(IDX::Y) = vx * sin(yaw);
  d_state(IDX::VX) = ax;
  d_state(IDX::YAW) = vx * std::tan(steer) / wheelbase_;

  return d_state;
}
