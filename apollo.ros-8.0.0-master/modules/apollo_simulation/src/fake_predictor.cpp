/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_simulation/fake_predictor.h"

#include <cmath>

#include "apollo_common/log.h"
#include "apollo_common/math/linear_interpolation.h"
#include "apollo_common/math/math_utils.h"
#include "apollo_common/math/quaternion.h"
#include "apollo_common/time/time.h"
#include "apollo_common/time/clock.h"
#include "apollo_common/file.h"
#include "apollo_common/util/util.h"
#include "apollo_common/util/message_util.h"
#include "apollo_common/util/point_factory.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_map/hdmap/hdmap_util.h"
#include "apollo_map/pnc_map/pnc_map.h"
#include "apollo_msgs/perception_msgs/perception_obstacle.pb.h"
#include "apollo_msgs/prediction_msgs/prediction_obstacle.pb.h"
#include "apollo_msgs/basic_msgs/geometry.pb.h"

using apollo::common::Header;
using apollo::common::Point3D;
using apollo::common::Quaternion;
using apollo::common::TrajectoryPoint;
using apollo::common::PointENU;
using apollo::common::math::HeadingToQuaternion;
using apollo::common::math::InverseQuaternionRotate;
using apollo::common::math::InterpolateUsingLinearApproximation;
using apollo::common::math::NormalizeAngle;
using apollo::common::math::QuaternionToHeading;
using apollo::common::time::Clock;
using apollo::perception::PerceptionObstacle;
using apollo::prediction::PredictionObstacle;
using apollo::prediction::PredictionObstacles;
using apollo::hdmap::LaneInfoConstPtr;
using apollo::hdmap::RouteSegments;
using apollo::hdmap::HDMapUtil;
using apollo::hdmap::Path;
using apollo::hdmap::PncMap;

//add by travis begin
//坐标转换的函数 --begin
    // 自车在UTM坐标系中的位置
UTMCoordinate vehicleUTM;
// 摄像头相对于自车的位置
RelativeCoordinate cameraRelative;
// 自车的Heading角度（以度为单位
double vehicleHeading = 0;
// 障碍物目标相对于摄像头的位置
RelativeCoordinate obstacleRelative;
//坐标转换的函数 --end
//障碍物的向量   32是因为摄像头每次最多同时识别32个障碍物

struct Obstacle {
    int ID;
    double age;
    double position_x;
    double position_y;
    double vx;
    double vy;
    double ax;
    double ay;
    int type;
};    

std::vector<Obstacle> g_obstacle= {{0}};
//add by travis end

FakePredictor::FakePredictor(HDMapVisualizer &hdmap_visualizer)
  : hdmap_visualizer_(hdmap_visualizer) {
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

    //add by travis begin
    vehicleUTM.x = 0.0;
    vehicleUTM.y = 0.0;
   
    cameraRelative.x = 5;
    cameraRelative.y = 0;
    
    obstacleRelative.x = 0;
    obstacleRelative.y = 0;

  // Project operation mode
  // "SIMULATION" 和 "REAL_RTK"
  if (ros::param::get("/apollo_simulation/running_mode", running_mode_)) {
    ROS_INFO_STREAM("=====================> Got parameter: " << running_mode_);
  } else {
    ROS_INFO_STREAM("=====================> Failed to get parameter");
    return;
  }
  //add by travis end

  prediction_writer_ = nh.advertise<std_msgs::String>(FLAGS_prediction_topic, 1);
  //以下是rviz里拖出来的虚拟障碍物
  sub_fake_prediction_ = nh.subscribe("/fake_prediction", 10, &FakePredictor::CallbackFakePrediction, this);
  
  #if 1
  sub_fake_prediction_ = nh.subscribe("obstacle", 10, &FakePredictor::CallbackObstacle, this);
  if(running_mode_ == "REAL_RTK")
  sub_self_carLocation_ = nh.subscribe("gps_data", 10, &FakePredictor::GpsDataCallback, this);
  else if(running_mode_ == "SIMULATOR")
  sub_self_carLocation_ = nh.subscribe("simulation_gps", 10, &FakePredictor::GpsDataCallback, this);
  
  #endif
  //modify by travis end
  pub_prediction_obstacle_ = private_nh.advertise<visualization_msgs::MarkerArray>("obstacle", 1);
  // pub_prediction_obstacle_ = private_nh.advertise<geometry_msgs::PolygonStamped>("obstacle", 1);

  private_nh.param<std::string>("car_frame_id", car_frame_id_, std::string("base_link"));
  private_nh.param<std::string>("map_frame_id", map_frame_id_, std::string("map"));

  dsrv_ = new dynamic_reconfigure::Server<simulation::FakePredictorConfig>(private_nh);
  dynamic_reconfigure::Server<simulation::FakePredictorConfig>::CallbackType cb =
    boost::bind(&FakePredictor::ReconfigureCB, this, _1, _2);
  dsrv_->setCallback(cb);

  sim_prediction_timer_ = nh.createTimer(ros::Duration(kSimPredictionIntervalMs), 
      &FakePredictor::PublishFakePrediction, this);
}

FakePredictor::~FakePredictor() {
  if (dsrv_ != nullptr) {
    delete dsrv_;
  }
}

void FakePredictor::ReconfigureCB(simulation::FakePredictorConfig &config, uint32_t level) {
  if (setup_ && config.restore_defaults) {
    config = default_config_;
    config.restore_defaults = false;
  }
  if (!setup_) {
    default_config_ = config;
    setup_ = true;
  }
  // update params
  if (config.obstacle_clear != obstacle_clear_) {
    obstacle_clear_ = config.obstacle_clear;
    if (obstacle_clear_) {
      AINFO << "clear prediction obstacle.";
      std::lock_guard<std::mutex> lg(prediction_mutex_);
      prediction_.clear_prediction_obstacle();
      prediction_count_ = 0;
    }
  }
  obstacle_speed_ = config.obstacle_speed;
  obstacle_max_size_ = config.obstacle_max_size;
}

void FakePredictor::PublishFakePrediction(const ros::TimerEvent &event) {
  UpdatePrediction();

  apollo::common::util::FillHeader("SimPrediction", &prediction_);
  std_msgs::String ros_msg;
  ros_msg.data = prediction_.SerializeAsString();
  prediction_writer_.publish(ros_msg); 
  // AINFO << "prediction: " << prediction_.ShortDebugString();

  // pub for ros rviz
  PubPredictionObstacle();
}

void FakePredictor::UpdatePrediction() {
  prediction_.set_start_timestamp(apollo::common::time::Clock::NowInSeconds());

  std::lock_guard<std::mutex> lg(prediction_mutex_);

  // update obstacle pose and trajectory
  auto *prediction_obstacles = prediction_.mutable_prediction_obstacle();
  for (auto obstacle = prediction_obstacles->begin(); obstacle != prediction_obstacles->end(); ++obstacle) {
    obstacle->clear_trajectory();
    if (fabs(obstacle->perception_obstacle().velocity().x()) > 1e-3) {
      // update pose by constant velocity model
      {
        tf::Transform cpose;
        tf::Quaternion cquat;
        cpose.setOrigin(tf::Vector3(obstacle->perception_obstacle().position().x(),
            obstacle->perception_obstacle().position().y(),
            obstacle->perception_obstacle().position().z()));
        cquat.setRPY(0.0, 0.0, obstacle->perception_obstacle().theta());
        cpose.setRotation(cquat);

        tf::Transform dpose;
        tf::Quaternion dquat;
        dpose.setOrigin(tf::Vector3(obstacle->perception_obstacle().velocity().x() * kSimPredictionIntervalMs,
            0.0, 0.0));
        dquat.setRPY(0.0, 0.0, 0.0);
        dpose.setRotation(dquat);

        cpose *= dpose;

        obstacle->mutable_perception_obstacle()->mutable_position()->set_x(cpose.getOrigin().x());
        obstacle->mutable_perception_obstacle()->mutable_position()->set_y(cpose.getOrigin().y());
        obstacle->mutable_perception_obstacle()->mutable_position()->set_z(cpose.getOrigin().z());
      }
      // update trajectory
      {
        auto *trajectory = obstacle->add_trajectory();
        for (double dt = 0; dt <= 8.0; dt += 1.0) {
          tf::Transform cpose;
          tf::Quaternion cquat;
          cpose.setOrigin(tf::Vector3(obstacle->perception_obstacle().position().x(),
              obstacle->perception_obstacle().position().y(),
              obstacle->perception_obstacle().position().z()));
          cquat.setRPY(0.0, 0.0, obstacle->perception_obstacle().theta());
          cpose.setRotation(cquat);

          tf::Transform dpose;
          tf::Quaternion dquat;
          dpose.setOrigin(tf::Vector3(obstacle->perception_obstacle().velocity().x() * dt,
              0.0, 0.0));
          dquat.setRPY(0.0, 0.0, 0.0);
          dpose.setRotation(dquat);

          cpose *= dpose;

          auto *trajectory_point = trajectory->add_trajectory_point();
          trajectory_point->mutable_path_point()->set_x(cpose.getOrigin().x());
          trajectory_point->mutable_path_point()->set_y(cpose.getOrigin().y());
          trajectory_point->mutable_path_point()->set_z(cpose.getOrigin().z());
          trajectory_point->set_v(obstacle->perception_obstacle().velocity().x());
          trajectory_point->set_a(0.0);
          trajectory_point->set_relative_time(dt);
        }
      }
    }
  }

  prediction_.set_end_timestamp(apollo::common::time::Clock::NowInSeconds());
}
//mark by travis
void FakePredictor::CallbackFakePrediction(const geometry_msgs::PoseStamped::ConstPtr &msg) {
  auto obstacle_point = hdmap_visualizer_.VizPointToHDMapPoint(apollo::common::util::PointFactory::ToPointENU(
        msg->pose.position.x, msg->pose.position.y));
  double s, l;
  LaneInfoConstPtr nearest_lane;
  if (!GetNearestLane(obstacle_point.x(), obstacle_point.y(), &nearest_lane, &s, &l)) {
    return;
  }
  // double obstacle_theta = nearest_lane->Heading(s);
  double roll, pitch, yaw;
  tf2::getEulerYPR(msg->pose.orientation, yaw, pitch, roll);
  double obstacle_theta = yaw;

  std::lock_guard<std::mutex> lg(prediction_mutex_);

  auto *prediction_obstacle = prediction_.add_prediction_obstacle();
  prediction_obstacle->set_timestamp(apollo::common::time::Clock::NowInSeconds());
  prediction_obstacle->set_predicted_period(0.01);
  auto *obstacle_intent = prediction_obstacle->mutable_intent();
  if (fabs(obstacle_speed_) < 1e-3) {
    obstacle_intent->set_type(apollo::prediction::ObstacleIntent_Type::ObstacleIntent_Type_STOP);
    prediction_obstacle->set_is_static(true);
  } else {
    obstacle_intent->set_type(apollo::prediction::ObstacleIntent_Type::ObstacleIntent_Type_MOVING);
    prediction_obstacle->set_is_static(false);
  }

  auto *perception_obstacle = prediction_obstacle->mutable_perception_obstacle();
  perception_obstacle->set_id(prediction_count_++);
  perception_obstacle->mutable_position()->set_x(obstacle_point.x());
  perception_obstacle->mutable_position()->set_y(obstacle_point.y());
  perception_obstacle->mutable_position()->set_z(0.0);
  perception_obstacle->set_theta(obstacle_theta);
  perception_obstacle->mutable_velocity()->set_x(obstacle_speed_);
  perception_obstacle->mutable_velocity()->set_y(0.0);
  perception_obstacle->mutable_velocity()->set_z(0.0);
  perception_obstacle->set_length(20);
  perception_obstacle->set_width(10);
  perception_obstacle->set_height(5);

  FillPerceptionPolygon(perception_obstacle, perception_obstacle->position().x(),
    perception_obstacle->position().y(), perception_obstacle->position().z(),
    perception_obstacle->length(), perception_obstacle->width(),
    perception_obstacle->height(), perception_obstacle->theta());

  perception_obstacle->set_tracking_time(apollo::common::time::Clock::NowInSeconds());
  perception_obstacle->set_type(apollo::perception::PerceptionObstacle_Type_VEHICLE);
  perception_obstacle->set_timestamp(apollo::common::time::Clock::NowInSeconds());

  // check obstacle max size
  if (prediction_.prediction_obstacle_size() > obstacle_max_size_) {
    auto *prediction_obstacles = prediction_.mutable_prediction_obstacle();
    auto erase_it = prediction_obstacles->begin();
    prediction_obstacles->erase(erase_it);
  }
}
//mark by travis

// ===========================================travis====================================================begin
// GpsDataCallback 函数用于处理订阅到的 gps_data
void FakePredictor::GpsDataCallback(const geometry_msgs::Pose::ConstPtr& msg) {
    // 自车UTM坐标和角度
    
    vehicleUTM.x = msg->position.x;
    vehicleUTM.y = msg->position.y;
    vehicleHeading = msg->position.z;
    std::cout << "vehicleUTM.x=" << vehicleUTM.x << "vehicleUTM.y=" << vehicleUTM.y << "vehicleHeading=" << vehicleHeading<< std::endl;
    std::cout << __LINE__ << std::endl;
}

void FakePredictor::CallbackObstacle(const vaeb_node_msgs::vaeb::ConstPtr &msg) {

  bool isNewObstacle = true;
  
  if (msg->ID <= 0 ) return;
  // 遍历已有的障碍物信息
  for (int i = 1; i < g_obstacle.size(); i++) {
    if (g_obstacle[i].ID == msg->ID && g_obstacle[i].ID != 0) {
        isNewObstacle = false;
        index = i;
        break;
    }
  }            


  if (isNewObstacle && msg->ID > 0) {
    // 创建新的障碍物信息
    Obstacle newObstacle;
    newObstacle.ID = msg->ID;
    newObstacle.age = msg->age;
    newObstacle.position_x = msg->PosnLgt;
    newObstacle.position_y = msg->PosnLat;
    newObstacle.vx = msg->VLgt;
    newObstacle.vy = msg->VLat;
    newObstacle.ax = msg->ALgt;
    newObstacle.ay = msg->ALat;
    newObstacle.type = msg->Typ;

    // 判断是否需要删除最早的障碍物信息
    if (g_obstacle.size() >= 32) {
      g_obstacle.erase(g_obstacle.begin());
    }

    // 添加新的障碍物信息到向量末尾
    g_obstacle.push_back(newObstacle);
    } else if (msg->ID > 0 && !isNewObstacle){
    // 更新已有障碍物信息
    g_obstacle[index].age = msg->age;
    g_obstacle[index].position_x = msg->PosnLgt;
    g_obstacle[index].position_y = msg->PosnLat;
    g_obstacle[index].vx = msg->VLgt;
    g_obstacle[index].vy = msg->VLat;
    g_obstacle[index].ax = msg->ALgt;
    g_obstacle[index].ay = msg->ALat;
    g_obstacle[index].type = msg->Typ;
    }

     // 判断障碍物是否已经消失
    bool obstacleDisappeared = true;
    for (int i = 0; i < g_obstacle.size(); i++) {
        if (g_obstacle[i].ID == msg->ID) {
            obstacleDisappeared = false;
            break;
        }
    }

    if (obstacleDisappeared) {
        // 障碍物已经消失，从g_obstacle中删除对应的障碍物信息
        for (int i = 0; i < g_obstacle.size(); i++) {
            if (g_obstacle[i].ID == msg->ID) {
                g_obstacle.erase(g_obstacle.begin() + i);
                std::cout  << "clear prediction obstacle." << std::endl;
                std::lock_guard<std::mutex> lg(prediction_mutex_);
                prediction_.clear_prediction_obstacle();
                prediction_count_ = 0;
                break;
            }
        }
    }

  for (int i = 1;i < g_obstacle.size(); i++) {
    // 访问 g_vector 的每个元素
    // 进行相应的操作
    if(g_obstacle[i].ID == 0) break;

    obstacleRelative.x = g_obstacle[i].position_x;
    obstacleRelative.y = g_obstacle[i].position_y;

    UTMCoordinate obstacleUTM =calculateObstacleUTMCoordinate(vehicleUTM,vehicleHeading,cameraRelative,obstacleRelative);
    std::cout << "ID=" << g_obstacle[i].ID << "vehicleUTM.x=" << vehicleUTM.x << "vehicleUTM.y=" << vehicleUTM.y << "vehicleHeading" << vehicleHeading << "obstacleRelative.x" << obstacleRelative.x << "obstacleRelative.y" << obstacleRelative.y << std::endl; 
    // std::cout << "obstacleUTM.x=" << obstacleUTM.x << "obstacleUTM.y" << obstacleUTM.y <<

    // 

    auto obstacle_point = hdmap_visualizer_.VizPointToHDMapPoint(apollo::common::util::PointFactory::ToPointENU(
      obstacleUTM.x,obstacleUTM.y));
      double s, l;
    LaneInfoConstPtr nearest_lane;
    if (!GetNearestLane(obstacle_point.x(), obstacle_point.y(), &nearest_lane, &s, &l)) {
      return;
    }
    double obstacle_velocity_x = g_obstacle[i].vx;
    double obstacle_velocity_y = g_obstacle[i].vy;

    std::lock_guard<std::mutex> lg(prediction_mutex_);
    double roll, pitch, yaw;
    // tf2::getEulerYPR(msg->pose.orientation, yaw, pitch, roll);
    double obstacle_theta = yaw = 2;  //方位角对应msg中怎么获取
    auto *prediction_obstacle = prediction_.add_prediction_obstacle();
    //clear_prediction_obstacle  如果到达标志位i=0   执行清除
    prediction_obstacle->set_timestamp(apollo::common::time::Clock::NowInSeconds());
    prediction_obstacle->set_predicted_period(0.01);
    auto *obstacle_intent = prediction_obstacle->mutable_intent();  
    if (sqrt(obstacle_velocity_x * obstacle_velocity_x + obstacle_velocity_y* obstacle_velocity_y) < 1e-3) {
      obstacle_intent->set_type(apollo::prediction::ObstacleIntent_Type::ObstacleIntent_Type_STOP);
      prediction_obstacle->set_is_static(true);
    } else {
      obstacle_intent->set_type(apollo::prediction::ObstacleIntent_Type::ObstacleIntent_Type_MOVING);
      prediction_obstacle->set_is_static(false);
    }

    auto *perception_obstacle = prediction_obstacle->mutable_perception_obstacle();
    perception_obstacle->set_id(g_obstacle[i].ID);
    perception_obstacle->mutable_position()->set_x(obstacle_point.x());
    perception_obstacle->mutable_position()->set_y(obstacle_point.y());
    perception_obstacle->mutable_position()->set_z(0.0);
    perception_obstacle->set_theta(obstacle_theta);
    perception_obstacle->mutable_velocity()->set_x(obstacle_velocity_x);
    perception_obstacle->mutable_velocity()->set_y(obstacle_velocity_y);
    perception_obstacle->mutable_velocity()->set_z(0.0);
    perception_obstacle->set_length(2);
    perception_obstacle->set_width(2);
    perception_obstacle->set_height(0);

    FillPerceptionPolygon(perception_obstacle, perception_obstacle->position().x(),
      perception_obstacle->position().y(), perception_obstacle->position().z(),
      perception_obstacle->length(), perception_obstacle->width(),
      perception_obstacle->height(), perception_obstacle->theta());

    perception_obstacle->set_tracking_time(apollo::common::time::Clock::NowInSeconds());
    if(msg->Typ == 0 || msg->Typ == 1)
      perception_obstacle->set_type(apollo::perception::PerceptionObstacle_Type_VEHICLE);
    else if (msg->Typ == 2 )
      perception_obstacle->set_type(apollo::perception::PerceptionObstacle_Type_PEDESTRIAN);
    else if (msg->Typ == 16 )
     perception_obstacle->set_type(apollo::perception::PerceptionObstacle_Type_BICYCLE);
    perception_obstacle->set_timestamp(apollo::common::time::Clock::NowInSeconds());

    // check obstacle max size
    if (prediction_.prediction_obstacle_size() > obstacle_max_size_) {
      auto *prediction_obstacles = prediction_.mutable_prediction_obstacle();
      auto erase_it = prediction_obstacles->begin();
      prediction_obstacles->erase(erase_it);
    }
  }
}

  
  

void FakePredictor::PubPredictionObstacle() {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  std::lock_guard<std::mutex> lg(prediction_mutex_);
  visualization_msgs::MarkerArray obstacle_markers;
  for (int i = 1; i < g_obstacle.size() ; i++) {
    // if(g_obstacle[i].ID == 0) break;
  
  size_t obstacle_index = 0;
  for (const auto &prediction_obstacle : prediction_.prediction_obstacle()) {
    if(g_obstacle[i].ID == 0) break;
    auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(apollo::common::util::PointFactory::ToPointENU(
          prediction_obstacle.perception_obstacle().position().x(),
          prediction_obstacle.perception_obstacle().position().y(), 0.0));
    // box
    {
      visualization_msgs::Marker box;
      box.header.frame_id = map_frame_id_;
      box.header.stamp = ros::Time::now();
      box.type = visualization_msgs::Marker::CUBE;
      box.action = visualization_msgs::Marker::ADD;
      box.ns = "/apollo_simulation/sim_prediction";
      box.id = obstacle_index++;
      box.color.a = 0.8;
      box.color.r = 0.0;
      box.color.g = 0.0;
      box.color.b = 1.0;
      box.scale.x = prediction_obstacle.perception_obstacle().length();
      box.scale.y = prediction_obstacle.perception_obstacle().width();
      box.scale.z = prediction_obstacle.perception_obstacle().height();
      box.pose.position.x = viz_point.x();
      box.pose.position.y = viz_point.y();
      box.pose.position.z = prediction_obstacle.perception_obstacle().height() / 2.0;
      box.pose.orientation = GetQuaternionFromRPY(0.0, 0.0, prediction_obstacle.perception_obstacle().theta());

      obstacle_markers.markers.push_back(box);
    }

    // id
    {
      visualization_msgs::Marker box_id_text;
      box_id_text.header.frame_id = map_frame_id_;
      box_id_text.header.stamp = ros::Time::now();
      box_id_text.ns = "/apollo_simulation/sim_prediction";
      box_id_text.action = visualization_msgs::Marker::ADD;
      box_id_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
      box_id_text.scale.x = 0.5;
      box_id_text.scale.y = 0.5;
      box_id_text.scale.z = 0.5;
      box_id_text.color.a = 1.0;
      box_id_text.color.r = 1.0;
      box_id_text.color.g = 1.0;
      box_id_text.color.b = 1.0;
      box_id_text.id = obstacle_index++;
      box_id_text.pose.position.x = viz_point.x();
      box_id_text.pose.position.y = viz_point.y();
      box_id_text.pose.position.z = prediction_obstacle.perception_obstacle().height() + 0.5;
      box_id_text.pose.orientation.w = 1.0;
      box_id_text.text = std::to_string(prediction_obstacle.perception_obstacle().id());

      obstacle_markers.markers.push_back(box_id_text);
    }

    // trajectory
    if (prediction_obstacle.trajectory_size() > 0) {
      visualization_msgs::Marker lineStripMain;
      lineStripMain.header.frame_id = map_frame_id_;
      lineStripMain.header.stamp = ros::Time::now();
      lineStripMain.ns = "/apollo_simulation/sim_prediction";
      lineStripMain.action = visualization_msgs::Marker::ADD;
      lineStripMain.pose.orientation.w = 1.0;
      lineStripMain.id = obstacle_index++;
      lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
      lineStripMain.scale.x = 0.1;
      lineStripMain.color.a = 0.8;
      lineStripMain.color.r = 0.0;
      lineStripMain.color.g = 0.0;
      lineStripMain.color.b = 1.0;

      for (const auto &point : prediction_obstacle.trajectory().begin()->trajectory_point()) {
        auto vp = hdmap_visualizer_.HDMapPointToVizPoint(apollo::common::util::PointFactory::ToPointENU
            (point.path_point().x(), point.path_point().y(), 0.0));
        geometry_msgs::Point p;
        p.x = vp.x();
        p.y = vp.y();
        p.z = 0;
        lineStripMain.points.push_back(p);
      }

      obstacle_markers.markers.push_back(lineStripMain);
    }
  }
  if (prediction_.prediction_obstacle_size() == 0) {
    std::cout << __LINE__ << "into marker.DELETEALL" << std::endl;
    visualization_msgs::Marker marker;
    marker.action = marker.DELETEALL;
    obstacle_markers.markers.push_back(marker);
  }
  

  // geometry_msgs::PolygonStamped obstacle;
  // obstacle.header.frame_id = map_frame_id_;
  // obstacle.header.stamp = ros::Time::now();
  // for (const auto &point : prediction_.prediction_obstacle(0).perception_obstacle().polygon_point())  {
  //   auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(apollo::common::util::PointFactory::ToPointENU(
  //         point.x(), point.y(), 0.0));

  //   geometry_msgs::Point32 new_pt;
  //   new_pt.x = viz_point.x();
  //   new_pt.y = viz_point.y();
  //   obstacle.polygon.points.push_back(new_pt);
  // }
  // pub_prediction_obstacle_.publish(obstacle);
  }
  pub_prediction_obstacle_.publish(obstacle_markers);
}

bool FakePredictor::GetNearestLane(const double x, const double y,
  LaneInfoConstPtr *nearest_lane, double *nearest_s, double *nearest_l) const {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return false;
  }

  PointENU point;
  point.set_x(x);
  point.set_y(y);
  if (hdmap_visualizer_.hdmap()->GetNearestLane(point, nearest_lane, nearest_s, nearest_l) < 0) {
    AERROR << "Failed to get nearest lane!";
    return false;
  }
  return true;
}

void FakePredictor::FillPerceptionPolygon(PerceptionObstacle *const perception_obstacle,
  const double mid_x, const double mid_y, const double mid_z,
  const double length, const double width, const double height, const double heading) {
  // Generate a 2D cube whose vertices are given in counter-clock order when
  // viewed from top
  const int sign_l[4] = {1, 1, -1, -1};
  const int sign_w[4] = {1, -1, -1, 1};
  for (int i = 0; i < 4; ++i) {
    perception_obstacle->add_polygon_point();
    perception_obstacle->mutable_polygon_point(i)->set_x(
      mid_x + sign_l[i] * length * std::cos(heading) / 2.0 +
      sign_w[i] * width * std::sin(heading) / 2.0);
    perception_obstacle->mutable_polygon_point(i)->set_y(
      mid_y + sign_l[i] * length * std::sin(heading) / 2.0 -
      sign_w[i] * width * std::cos(heading) / 2.0);
    perception_obstacle->mutable_polygon_point(i)->set_z(mid_z);
  }
}