/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_simulation/hdmap_visualizer.h"

#include "apollo_common/log.h"
#include "apollo_common/util/util.h"
#include "apollo_common/util/point_factory.h"
#include "apollo_map/hdmap/hdmap_util.h"
#include "apollo_map/hdmap/hdmap_common.h"

// for sim_map_generator
#include "apollo_msgs/map_msgs/map.pb.h"
#include "apollo_common/file.h"
#include "apollo_common/configs/config_gflags.h"
#include "apollo_common/util/points_downsampler.h"
#include "apollo_map/hdmap/adapter/opendrive_adapter.h"

using apollo::common::util::PointFactory;
using apollo::hdmap::HDMapUtil;
using apollo::hdmap::LaneInfoConstPtr;
using apollo::hdmap::RoadInfoConstPtr;
using apollo::hdmap::ParkingSpaceInfoConstPtr;
using apollo::hdmap::Lane;

/**
 * A tool to generate a downsampled map to displayed
 */
namespace sim_map_generator {

DEFINE_double(angle_threshold, 1. / 180 * M_PI, /* 1 degree */
              "Points are sampled when the accumulated direction change "
              "exceeds the threshold");
DEFINE_int32(downsample_distance, 5, "downsample rate for a normal path");
DEFINE_int32(steep_turn_downsample_distance, 1,
             "downsample rate for a steep turn path");

using apollo::common::PointENU;
using apollo::common::util::DownsampleByAngle;
using apollo::common::util::DownsampleByDistance;
using apollo::hdmap::Curve;
using apollo::hdmap::Map;
using apollo::hdmap::adapter::OpendriveAdapter;

static void DownsampleCurve(Curve* curve) {
  auto* line_segment = curve->mutable_segment(0)->mutable_line_segment();
  std::vector<PointENU> points(line_segment->point().begin(),
                               line_segment->point().end());
  line_segment->clear_point();

  // NOTE: this not the most efficient implementation, but since this map tool
  // is only run once for each, we can probably live with that.

  // Downsample points by angle then by distance.
  auto sampled_indices = DownsampleByAngle(points, FLAGS_angle_threshold);
  std::vector<PointENU> downsampled_points;
  for (const size_t index : sampled_indices) {
    downsampled_points.push_back(points[index]);
  }

  sampled_indices =
      DownsampleByDistance(downsampled_points, FLAGS_downsample_distance,
                           FLAGS_steep_turn_downsample_distance);

  for (const size_t index : sampled_indices) {
    *line_segment->add_point() = downsampled_points[index];
  }
  size_t new_size = line_segment->point_size();
  CHECK_GT(new_size, 1U);

  // AINFO << "Lane curve downsampled from " << points.size() << " points to "
  //       << new_size << " points.";
}

static void DownsampleMap(Map* map_pb) {
  for (int i = 0; i < map_pb->lane_size(); ++i) {
    auto* lane = map_pb->mutable_lane(i);
    lane->clear_left_sample();
    lane->clear_right_sample();
    lane->clear_left_road_sample();
    lane->clear_right_road_sample();

    AINFO << "Downsampling lane " << lane->id().id();
    DownsampleCurve(lane->mutable_central_curve());
    DownsampleCurve(lane->mutable_left_boundary()->mutable_curve());
    DownsampleCurve(lane->mutable_right_boundary()->mutable_curve());
  }
}

} // namespace sim_map_generator

HDMapVisualizer::HDMapVisualizer() {
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  pub_hdmap_ = private_nh.advertise<visualization_msgs::MarkerArray>("hdmap", 1, true);
  timer_hdmap_visualizer_ = nh.createTimer(ros::Duration(1.0 / viz_rate_),
      &HDMapVisualizer::TimerCallbackForVisualizing, this);

  hdmap_ = HDMapUtil::BaseMapPtr();
  CHECK(hdmap_) << "Failed to load map";

  viz_offset_point_.set_x(0.0);
  viz_offset_point_.set_y(0.0);
  viz_offset_point_.set_z(0.0);
  viz_center_point_.CopyFrom(viz_offset_point_);
}

HDMapVisualizer::~HDMapVisualizer() {
  delete hdmap_;
  hdmap_ = nullptr;
}

void HDMapVisualizer::TimerCallbackForVisualizing(const ros::TimerEvent &event) {
  if (!hdmap_) {
    AERROR << "hdmap is not ready";
    return;
  }

  visualization_msgs::MarkerArray hdmap_markers;
  size_t marker_index = 0;

  // lanes
  {
    // get lanes
    std::vector<LaneInfoConstPtr> lanes;
    const int status = hdmap_->GetLanes(viz_center_point_, viz_distance_, &lanes);
    if (status < 0) {
      AWARN << "failed to get lanes from point " << viz_center_point_.ShortDebugString();
    } else if (lanes.empty()) {
      AWARN << "No valid lanes found within " << viz_distance_ << " meters";
    } else {
      // viz lanes
      for (const auto &lane : lanes) {
        // lanes downsample
        Lane pd_lane = lane->lane();
        sim_map_generator::DownsampleCurve(pd_lane.mutable_central_curve());
        sim_map_generator::DownsampleCurve(pd_lane.mutable_left_boundary()->mutable_curve());
        sim_map_generator::DownsampleCurve(pd_lane.mutable_right_boundary()->mutable_curve());

        // viz lanes boundary
        for(const auto &segment : pd_lane.left_boundary().curve().segment()) {
          visualization_msgs::Marker lineStripMain;
          lineStripMain.header.frame_id = "map";
          lineStripMain.header.stamp = ros::Time::now();
          lineStripMain.ns = "/apollo_simulation/hdmap_visualize";
          lineStripMain.action = visualization_msgs::Marker::ADD;
          lineStripMain.pose.orientation.w = 1.0;
          lineStripMain.id = marker_index;
          lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
          // STRIP markers use only the x component of scale, for the line width
          lineStripMain.scale.x = 0.12;
          // // STRIP is yello
          // lineStripMain.color.a = 1.0;
          // lineStripMain.color.r = 1.0;
          // lineStripMain.color.g = 1.0;
          // lineStripMain.color.b = 0.0;
          // STRIP is white
          lineStripMain.color.a = 1.0;
          lineStripMain.color.r = 1.0;
          lineStripMain.color.g = 1.0;
          lineStripMain.color.b = 1.0;
          marker_index ++;

          for (const auto &point : segment.line_segment().point()) {
            auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(point.x(), point.y(), 0.0));
            geometry_msgs::Point p;
            p.x = viz_point.x();
            p.y = viz_point.y();
            p.z = 0;
            lineStripMain.points.push_back(p);
          }

          hdmap_markers.markers.push_back(lineStripMain);
        }
        for(const auto &segment : pd_lane.right_boundary().curve().segment()) {
          visualization_msgs::Marker lineStripMain;
          lineStripMain.header.frame_id = "map";
          lineStripMain.header.stamp = ros::Time::now();
          lineStripMain.ns = "/apollo_simulation/hdmap_visualize";
          lineStripMain.action = visualization_msgs::Marker::ADD;
          lineStripMain.pose.orientation.w = 1.0;
          lineStripMain.id = marker_index;
          lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
          // STRIP markers use only the x component of scale, for the line width
          lineStripMain.scale.x = 0.12;
          // // STRIP is yello
          // lineStripMain.color.a = 1.0;
          // lineStripMain.color.r = 1.0;
          // lineStripMain.color.g = 1.0;
          // lineStripMain.color.b = 0.0;
          // STRIP is white
          lineStripMain.color.a = 1.0;
          lineStripMain.color.r = 1.0;
          lineStripMain.color.g = 1.0;
          lineStripMain.color.b = 1.0;
          marker_index ++;

          for (const auto &point : segment.line_segment().point()) {
            auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(point.x(), point.y(), 0.0));
            geometry_msgs::Point p;
            p.x = viz_point.x();
            p.y = viz_point.y();
            p.z = 0;
            lineStripMain.points.push_back(p);
          }

          hdmap_markers.markers.push_back(lineStripMain);
        }

        // viz lanes central
        for(const auto &segment : pd_lane.central_curve().segment()) {
          visualization_msgs::Marker lineStripMain;
          lineStripMain.header.frame_id = "map";
          lineStripMain.header.stamp = ros::Time::now();
          lineStripMain.ns = "/apollo_simulation/hdmap_visualize";
          lineStripMain.action = visualization_msgs::Marker::ADD;
          lineStripMain.pose.orientation.w = 1.0;
          lineStripMain.id = marker_index;
          lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
          // STRIP markers use only the x component of scale, for the line width
          lineStripMain.scale.x = 0.08;
          // STRIP is green
          lineStripMain.color.a = 0.5;
          lineStripMain.color.r = 0.0;
          lineStripMain.color.g = 1.0;
          lineStripMain.color.b = 0.0;
          marker_index ++;

          for (const auto &point : segment.line_segment().point()) {
            auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(point.x(), point.y(), 0.0));
            geometry_msgs::Point p;
            p.x = viz_point.x();
            p.y = viz_point.y();
            p.z = 0;
            lineStripMain.points.push_back(p);
          }

          hdmap_markers.markers.push_back(lineStripMain);
        }

        // viz lanes id
        {
          visualization_msgs::Marker lane_id_text;
          lane_id_text.header.frame_id = "map";
          lane_id_text.header.stamp = ros::Time::now();
          lane_id_text.ns = "/apollo_simulation/hdmap_visualize";
          lane_id_text.action = visualization_msgs::Marker::ADD;
          lane_id_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
          // STRIP markers use only the x component of scale, for the line width
          lane_id_text.scale.x = 0.5;
          lane_id_text.scale.y = 0.5;
          lane_id_text.scale.z = 0.5;
          // STRIP is white
          lane_id_text.color.a = 1.0;
          lane_id_text.color.r = 1.0;
          lane_id_text.color.g = 1.0;
          lane_id_text.color.b = 1.0;
          lane_id_text.id = marker_index;
          marker_index ++;

          auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(lane->points().front().x(),
                lane->points().front().y(), 0.0));

          lane_id_text.pose.position.x = viz_point.x();
          lane_id_text.pose.position.y = viz_point.y();
          lane_id_text.pose.orientation.w = 1.0;

          lane_id_text.text = lane->id().id();
          hdmap_markers.markers.push_back(lane_id_text);
        }
      }
    }
  }

  // roads
  if (0) {
    // get roads
    std::vector<RoadInfoConstPtr> roads;
    const int status = hdmap_->GetRoads(viz_center_point_, viz_distance_, &roads);
    if (status < 0) {
      AWARN << "failed to get roads from point " << viz_center_point_.ShortDebugString();
    } else if (roads.empty()) {
      AWARN << "No valid roads found within " << viz_distance_ << " meters";
    } else {
      // viz roads boundary
      for (const auto &road : roads) {
        for (const auto &road_boundary : road->GetBoundaries()) {
          for (const auto &boundary_edge : road_boundary.outer_polygon().edge()) {
            for (const auto &segment : boundary_edge.curve().segment()) {
              visualization_msgs::Marker left_lineStripMain;
              left_lineStripMain.header.frame_id = "map";
              left_lineStripMain.header.stamp = ros::Time::now();
              left_lineStripMain.ns = "/apollo_simulation/hdmap_visualize";
              left_lineStripMain.action = visualization_msgs::Marker::ADD;
              left_lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
              left_lineStripMain.pose.orientation.w = 1.0;
              // STRIP markers use only the x component of scale, for the line width
              left_lineStripMain.scale.x = 0.12;
              // STRIP is white
              left_lineStripMain.color.a = 1.0;
              left_lineStripMain.color.r = 1.0;
              left_lineStripMain.color.g = 1.0;
              left_lineStripMain.color.b = 1.0;
              left_lineStripMain.id = marker_index;
              marker_index ++;

              visualization_msgs::Marker right_lineStripMain;
              right_lineStripMain = left_lineStripMain;
              right_lineStripMain.id = marker_index;
              marker_index ++;

              visualization_msgs::Marker normal_lineStripMain;
              normal_lineStripMain = left_lineStripMain;
              normal_lineStripMain.id = marker_index;
              marker_index ++;

              for (const auto &point : segment.line_segment().point()) {
                auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(point.x(), point.y(), 0.0));
                geometry_msgs::Point p;
                p.x = viz_point.x();
                p.y = viz_point.y();
                p.z = 0;
                if (boundary_edge.type() == apollo::hdmap::BoundaryEdge::LEFT_BOUNDARY) {
                  left_lineStripMain.points.push_back(p);
                } else if (boundary_edge.type() == apollo::hdmap::BoundaryEdge::RIGHT_BOUNDARY) {
                  right_lineStripMain.points.push_back(p);
                } else if (boundary_edge.type() == apollo::hdmap::BoundaryEdge::NORMAL) {
                  normal_lineStripMain.points.push_back(p);
                }
              }

              if (!left_lineStripMain.points.empty()) {
                hdmap_markers.markers.push_back(left_lineStripMain);
              }
              if (!right_lineStripMain.points.empty()) {
                hdmap_markers.markers.push_back(right_lineStripMain);
              }
              if (!normal_lineStripMain.points.empty()) {
                hdmap_markers.markers.push_back(normal_lineStripMain);
              }
            }
          }
        }
      }
    }
  }

  // parking spaces
  {
    // get parking spaces
    std::vector<ParkingSpaceInfoConstPtr> parking_spaces;
    const int status = hdmap_->GetParkingSpaces(viz_center_point_, viz_distance_, &parking_spaces);
    if (status < 0) {
      AWARN << "failed to get parking spaces from point " << viz_center_point_.ShortDebugString();
    } else {
      for (const auto &parking_space : parking_spaces) {
        // viz parking space id
        {
          visualization_msgs::Marker parking_space_id_text;
          parking_space_id_text.header.frame_id = "map";
          parking_space_id_text.header.stamp = ros::Time::now();
          parking_space_id_text.ns = "/apollo_simulation/hdmap_visualize";
          parking_space_id_text.action = visualization_msgs::Marker::ADD;
          parking_space_id_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
          // STRIP markers use only the x component of scale, for the line width
          parking_space_id_text.scale.x = 0.5;
          parking_space_id_text.scale.y = 0.5;
          parking_space_id_text.scale.z = 0.5;
          // parking_space_id_text is white
          parking_space_id_text.color.a = 1.0;
          parking_space_id_text.color.r = 1.0;
          parking_space_id_text.color.g = 1.0;
          parking_space_id_text.color.b = 1.0;
          parking_space_id_text.id = marker_index;
          marker_index ++;

          double center_point_x = 0.5 * (parking_space->polygon().max_x() + parking_space->polygon().min_x());
          double center_point_y = 0.5 * (parking_space->polygon().max_y() + parking_space->polygon().min_y());
          auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(center_point_x, center_point_y, 0.0));

          parking_space_id_text.pose.position.x = viz_point.x();
          parking_space_id_text.pose.position.y = viz_point.y();
          parking_space_id_text.pose.orientation.w = 1.0;

          parking_space_id_text.text = parking_space->id().id();
          hdmap_markers.markers.push_back(parking_space_id_text);
        }

        // viz parking space polygon
        {
          visualization_msgs::Marker lineStripMain;
          lineStripMain.header.frame_id = "map";
          lineStripMain.header.stamp = ros::Time::now();
          lineStripMain.ns = "/apollo_simulation/hdmap_visualize";
          lineStripMain.action = visualization_msgs::Marker::ADD;
          lineStripMain.pose.orientation.w = 1.0;
          lineStripMain.id = marker_index;
          marker_index ++;
          lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;

          // STRIP markers use only the x component of scale, for the line width
          lineStripMain.scale.x = 0.08;
          // STRIP is yello
          lineStripMain.color.a = 1.0;
          lineStripMain.color.r = 1.0;
          lineStripMain.color.g = 1.0;
          lineStripMain.color.b = 0.0;

          for (const auto &line_seg : parking_space->polygon().line_segments()) {
            auto viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(
                  line_seg.start().x(), line_seg.start().y(), 0.0));
            geometry_msgs::Point p;
            p.x = viz_point.x();
            p.y = viz_point.y();
            p.z = 0;
            lineStripMain.points.push_back(p);

            viz_point = HDMapPointToVizPoint(PointFactory::ToPointENU(
                  line_seg.end().x(), line_seg.end().y(), 0.0));
            p.x = viz_point.x();
            p.y = viz_point.y();
            p.z = 0;
            lineStripMain.points.push_back(p);
          }

          hdmap_markers.markers.push_back(lineStripMain);
        }
      }
    }
  }

  // publis the Marker Array
  // pub_hdmap_.publish(GenerateDeleteMarker());
  pub_hdmap_.publish(hdmap_markers);
}

visualization_msgs::MarkerArray HDMapVisualizer::GenerateDeleteMarker() {
  visualization_msgs::MarkerArray ret;
  visualization_msgs::Marker marker;
  marker.action = marker.DELETEALL;
  ret.markers.push_back(marker);
  return ret;
}
