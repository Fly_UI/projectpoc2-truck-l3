/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_simulation/sim_vehicle.h"

#include <string>
#include <memory>
#include <utility>

#include "apollo_common/log.h"
#include "apollo_common/util/util.h"
#include "apollo_common/util/message_util.h"
#include "apollo_common/util/point_factory.h"
#include "apollo_common/math/quaternion.h"
#include "apollo_common/math/math_utils.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_map/hdmap/hdmap_util.h"
#include "apollo_map/pnc_map/pnc_map.h"
#include "apollo_msgs/localization_msgs/localization.pb.h"
#include "apollo_msgs/chassis_msgs/chassis.pb.h"
#include "apollo_msgs/basic_msgs/geometry.pb.h"

using apollo::routing::RoutingRequest;
using apollo::routing::RoutingResponse;
using apollo::hdmap::LaneInfoConstPtr;
using apollo::common::PointENU;
using apollo::hdmap::RouteSegments;
using apollo::hdmap::HDMapUtil;
using apollo::hdmap::Path;
using apollo::hdmap::PncMap;
using apollo::hdmap::MapPathPoint;
using apollo::common::util::PointFactory;
using apollo::hdmap::ParkingSpaceInfoConstPtr;
using apollo::routing::ParkingSpaceType;

SimVehicle::SimVehicle(tf2_ros::Buffer &tf_buffer)
  : tf_buffer_(tf_buffer) {
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  // Project operation mode
  // "SIMULATION" 和 "REAL_RTK"
  if (ros::param::get("/apollo_simulation/running_mode", running_mode_)) {
    ROS_INFO_STREAM("=====================> Got parameter: " << running_mode_);
  } else {
    ROS_INFO_STREAM("=====================> Failed to get parameter");
    return;
  }


  /* simple_planning_simulator parameters */
  private_nh.param<double>("loop_rate", loop_rate_, 30.0);
  private_nh.param<double>("wheelbase", wheelbase_, 0.0);
  private_nh.param<double>("steer_transmission_ratio", steer_transmission_ratio_, 16.0);
  private_nh.param<double>("steer_single_direction_max_degree", steer_single_direction_max_degree_, 470.0);
  private_nh.param<std::string>("car_frame_id", car_frame_id_, std::string("base_link"));
  private_nh.param<std::string>("map_frame_id", map_frame_id_, std::string("map"));
  private_nh.param<bool>("add_measurement_noise", add_measurement_noise_, false);

  /* set pub sub topic name */
  routing_reader_ = nh.subscribe(FLAGS_routing_response_topic, 5, 
      &SimVehicle::OnRoutingResponse, this);
  planning_reader_ = nh.subscribe(FLAGS_planning_trajectory_topic, 5, 
      &SimVehicle::OnPlanning, this);
  control_reader_ = nh.subscribe(FLAGS_control_command_topic, 5, 
      &SimVehicle::OnControl, this);

  chassis_writer_ = nh.advertise<std_msgs::String>(FLAGS_chassis_topic, 1);
  localization_writer_ = nh.advertise<std_msgs::String>(FLAGS_localization_topic, 1);
  routing_writer_ = nh.advertise<std_msgs::String>(FLAGS_routing_request_topic, 1);



  pub_pose_ = private_nh.advertise<geometry_msgs::PoseStamped>("sim_pose", 1);
  pub_speed_ = private_nh.advertise<geometry_msgs::Twist>("sim_speed", 1);
  // pub_routing_path_ = private_nh.advertise<nav_msgs::Path>("path", 1);
  pub_routing_path_ = private_nh.advertise<visualization_msgs::MarkerArray>("routing/path", 1);
  pub_reference_line_ = private_nh.advertise<nav_msgs::Path>("planning/reference_line", 1);
  pub_planning_trajectory_ = private_nh.advertise<nav_msgs::Path>("planning/trajectory", 1);

  sub_navgoal_ = nh.subscribe("/move_base_simple/goal", 10, &SimVehicle::CallbackNavGoalPoseStamped, this);
  sub_initialtwist_ = nh.subscribe("/initial_twist", 10, &SimVehicle::CallbackInitialTwistStamped, this);
  sub_initialpose_ = nh.subscribe("/initial_pose", 10, &SimVehicle::CallbackInitialPoseWithCov, this);

  const double dt = 1.0 / loop_rate_;
  
  /* Timer */
  timer_simulation_ = nh.createTimer(ros::Duration(dt), &SimVehicle::TimerCallbackSimulation, this);    

  /* set vehicle model parameters */
  {
    std::string vehicle_model_type_str;
    private_nh.param<std::string>("vehicle_model_type", vehicle_model_type_str, std::string("IDEAL_STEER"));
    AINFO << "vehicle_model_type = " << vehicle_model_type_str;
    double vel_lim, vel_time_delay, vel_time_constant;
    private_nh.param<double>("vel_lim", vel_lim, 50.0);
    private_nh.param<double>("vel_time_delay", vel_time_delay, 0.25);
    private_nh.param<double>("vel_time_constant", vel_time_constant, 0.5);
    double accel_rate, acc_time_delay, acc_time_constant;
    private_nh.param<double>("accel_rate", accel_rate, 10.0);
    private_nh.param<double>("acc_time_delay", acc_time_delay, 0.1);
    private_nh.param<double>("acc_time_constant", acc_time_constant, 0.1);
    double steer_lim, steer_rate_lim, steer_time_delay, steer_time_constant, deadzone_delta_steer;
    private_nh.param<double>("steer_lim", steer_lim, 1.0);
    private_nh.param<double>("steer_rate_lim", steer_rate_lim, 5.0);
    private_nh.param<double>("steer_time_delay", steer_time_delay, 0.3);
    private_nh.param<double>("steer_time_constant", steer_time_constant, 0.3);
    private_nh.param<double>("deadzone_delta_steer", steer_time_constant, 0.0);

    if (vehicle_model_type_str == "IDEAL_STEER") {
      vehicle_model_type_ = VehicleModelType::IDEAL_STEER;
      vehicle_model_ptr_ = std::make_shared<SimModelIdealSteer>(wheelbase_);
    } else if (vehicle_model_type_str == "DELAY_STEER") {
      vehicle_model_type_ = VehicleModelType::DELAY_STEER;
      vehicle_model_ptr_ = std::make_shared<SimModelTimeDelaySteer>(
          vel_lim, steer_lim, accel_rate, steer_rate_lim, wheelbase_, dt, vel_time_delay,
          vel_time_constant, steer_time_delay, steer_time_constant, deadzone_delta_steer);
    } else if (vehicle_model_type_str == "DELAY_STEER_ACC") {
      vehicle_model_type_ = VehicleModelType::DELAY_STEER_ACC;
      vehicle_model_ptr_ = std::make_shared<SimModelTimeDelaySteerAccel>(
          vel_lim, steer_lim, accel_rate, steer_rate_lim, wheelbase_, dt, acc_time_delay,
          acc_time_constant, steer_time_delay, steer_time_constant, deadzone_delta_steer);
    } else {
      AERROR << "Invalid vehicle_model_type. Initialization failed.";
    }
  }

  /* set normal distribution noises */
  {
    int random_seed;
    private_nh.param<int>("random_seed", random_seed, 1);
    if (random_seed >= 0) {
      rand_engine_ptr_ = std::make_shared<std::mt19937>(random_seed);
    } else {
      std::random_device seed;
      rand_engine_ptr_ = std::make_shared<std::mt19937>(seed());
    }
    double pos_noise_stddev, vel_noise_stddev, rpy_noise_stddev, angvel_noise_stddev, steer_noise_stddev;
    private_nh.param<double>("pos_noise_stddev", pos_noise_stddev, 0.01);
    private_nh.param<double>("vel_noise_stddev", vel_noise_stddev, 0.0);
    private_nh.param<double>("rpy_noise_stddev", rpy_noise_stddev, 0.0001);
    private_nh.param<double>("angvel_noise_stddev", angvel_noise_stddev, 0.0);
    private_nh.param<double>("steer_noise_stddev", steer_noise_stddev, 0.0001);
    pos_norm_dist_ptr_ = std::make_shared<std::normal_distribution<>>(0.0, pos_noise_stddev);
    vel_norm_dist_ptr_ = std::make_shared<std::normal_distribution<>>(0.0, vel_noise_stddev);
    rpy_norm_dist_ptr_ = std::make_shared<std::normal_distribution<>>(0.0, rpy_noise_stddev);
    angvel_norm_dist_ptr_ = std::make_shared<std::normal_distribution<>>(0.0, angvel_noise_stddev);
    steer_norm_dist_ptr_ = std::make_shared<std::normal_distribution<>>(0.0, steer_noise_stddev);
  }

  current_pose_.orientation.w = 1.0;
  closest_pos_z_ = 0.0;

  if (hdmap_visualizer_.hdmap()) {
    // Start from origin to find a lane from the map.
    double s, l;
    LaneInfoConstPtr lane;
    if (!GetNearestLane(0.0, 0.0, &lane, &s, &l)) {
      AWARN << "Failed to get a dummy start point from map!";
      return;
    }
    auto start_point = lane->GetSmoothPoint(0.0);
    double theta = lane->Heading(s);
    //modify by travis beigin
    // hdmap_visualizer_.SetVizOffsetPointUseHDMapPose(PointFactory::ToPointENU(
    //     start_point.x(), start_point.y(), 0.0));
    hdmap_visualizer_.SetVizOffsetPointUseHDMapPose(PointFactory::ToPointENU(
        323858.413909887, 3463680.45143366, 0.0));

    // // initialized with zero for all components
    // auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
    //       start_point.x(), start_point.y(), 0.0));
    auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
          323858.413909887, 3463680.45143366, 0.0));      
    //modify by travis end
    
    geometry_msgs::Twist initial_twist;
    geometry_msgs::PoseWithCovarianceStamped initial_pose_with_cov;
    initial_pose_with_cov.header.frame_id = map_frame_id_;
    initial_pose_with_cov.header.stamp = ros::Time::now();
    initial_pose_with_cov.pose.pose.position.x = viz_point.x();
    initial_pose_with_cov.pose.pose.position.y = viz_point.y();
    initial_pose_with_cov.pose.pose.position.z = 0.0;
    initial_pose_with_cov.pose.pose.orientation = GetQuaternionFromRPY(0, 0, theta);
    SetInitialStateWithPoseTransform(initial_pose_with_cov, initial_twist);
  }

  fake_predictor_ = std::make_shared<FakePredictor>(hdmap_visualizer_);
}

void SimVehicle::OnRoutingResponse(const std_msgs::String::ConstPtr &msg) {
  std::lock_guard<std::mutex> lock(mutex_);

  RoutingResponse routing;
  if (!routing.ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  PublishRoutingPath(routing);
}

void SimVehicle::OnPlanning(const std_msgs::String::ConstPtr &msg) {
  std::lock_guard<std::mutex> lock(mutex_);

  apollo::planning::ADCTrajectory trajectory;
  if (!trajectory.ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  PublishPlanningData(trajectory);
}

void SimVehicle::OnControl(const std_msgs::String::ConstPtr &msg) {
  std::lock_guard<std::mutex> lock(mutex_);

  if (!control_command_.ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }
}

void SimVehicle::PublishChassis(const double &steering_angle, const geometry_msgs::Twist &twist,
                            apollo::canbus::Chassis::GearPosition gear_position) {
  apollo::canbus::Chassis chassis;
  apollo::common::util::FillHeader("SimVehicle", &chassis);

  chassis.set_engine_started(true);
  chassis.set_driving_mode(apollo::canbus::Chassis::COMPLETE_AUTO_DRIVE);
  chassis.set_gear_location(gear_position);

  chassis.set_speed_mps(twist.linear.x);
  if (gear_position == apollo::canbus::Chassis::GEAR_REVERSE) {
    chassis.set_speed_mps(-chassis.speed_mps());
  }
  chassis.set_throttle_percentage(0.0);
  chassis.set_brake_percentage(0.0);
  double feedback_steering_percentage = steering_angle * 180 /
                                        M_PI * steer_transmission_ratio_ /
                                        steer_single_direction_max_degree_ * 100;
  // AINFO << "feedback_steering_percentage: " << feedback_steering_percentage;
  chassis.set_steering_percentage(feedback_steering_percentage);

  std_msgs::String ros_msg;
  ros_msg.data = chassis.SerializeAsString();
  chassis_writer_.publish(ros_msg); 
}

void SimVehicle::PublishLocalization(const geometry_msgs::Pose &pose,
  const geometry_msgs::Twist &twist, const double &linear_acc) {
  auto adc_point = hdmap_visualizer_.VizPointToHDMapPoint(PointFactory::ToPointENU(
        pose.position.x, pose.position.y, 0.0));

  apollo::localization::LocalizationEstimate localization;
  // header
  apollo::common::util::FillHeader("SimVehicle", &localization);
  // position
  auto mutable_pose = localization.mutable_pose();
  mutable_pose->mutable_position()->set_x(adc_point.x());
  mutable_pose->mutable_position()->set_y(adc_point.y());
  mutable_pose->mutable_position()->set_z(adc_point.z());
  // orientation(RFU)
  double roll, pitch, yaw;
  tf2::getEulerYPR(pose.orientation, yaw, pitch, roll);
  yaw = apollo::common::math::NormalizeAngle(yaw - M_PI_2);
  auto rfu_orientation = GetQuaternionFromRPY(roll, pitch, yaw);
  mutable_pose->mutable_orientation()->set_qw(rfu_orientation.w);
  mutable_pose->mutable_orientation()->set_qx(rfu_orientation.x);
  mutable_pose->mutable_orientation()->set_qy(rfu_orientation.y);
  mutable_pose->mutable_orientation()->set_qz(rfu_orientation.z);
  // car heading
  double heading = ::apollo::common::math::QuaternionToHeading(
      rfu_orientation.w, rfu_orientation.x,
      rfu_orientation.y, rfu_orientation.z);
  mutable_pose->set_heading(heading);
  // linear velocity(RFU)
  mutable_pose->mutable_linear_velocity()->set_x(-twist.linear.y);
  mutable_pose->mutable_linear_velocity()->set_y(twist.linear.x);
  mutable_pose->mutable_linear_velocity()->set_z(twist.linear.z);
  // angular velocity(RFU)
  mutable_pose->mutable_angular_velocity()->set_x(-twist.angular.y);
  mutable_pose->mutable_angular_velocity()->set_y(twist.angular.x);
  mutable_pose->mutable_angular_velocity()->set_z(twist.angular.z);

  // Set linear_acceleration in both map reference frame and vehicle reference frame
  mutable_pose->mutable_linear_acceleration()->set_x(std::cos(heading) * linear_acc);
  mutable_pose->mutable_linear_acceleration()->set_y(std::sin(heading) * linear_acc);
  mutable_pose->mutable_linear_acceleration()->set_z(0.0);

  // publish localization messages
  std_msgs::String ros_msg;
  ros_msg.data = localization.SerializeAsString();
  localization_writer_.publish(ros_msg); 
  // AINFO << "[OnTimer]: Localization message publish success!";
}

void SimVehicle::PublishRoutingPath(const RoutingResponse &routing) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  std::vector<Path> paths;
  for (const auto &road : routing.road()) {
    for (const auto &passage_region : road.passage()) {
      std::vector<MapPathPoint> path_points;
      for (const auto &segment : passage_region.segment()) {
        auto lane_ptr = hdmap_visualizer_.hdmap()->GetLaneById(apollo::hdmap::MakeMapId(segment.id()));
        if (!lane_ptr) {
          AERROR << "Failed to find lane: " << segment.id();
          return;
        }
        if (segment.start_s() >= segment.end_s()) {
          continue;
        }
        auto points = MapPathPoint::GetPointsFromLane(lane_ptr, segment.start_s(), segment.end_s());
        path_points.insert(path_points.end(), points.begin(), points.end());
      }
      if (path_points.size() < 2) {
        return;
      }
      paths.emplace_back(Path(std::move(path_points)));
    }
  }

  // nav_msgs::Path ros_path;
  // ros_path.header.frame_id = map_frame_id_;
  // ros_path.header.stamp = ros::Time::now();
  // geometry_msgs::PoseStamped ps;
  // ps.header = ros_path.header;
  // for (const auto &path : paths) {
  //   for (const auto &point : path.path_points()) {
  //     auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
  //         point.x(), point.y(), 0.0));

  //     ps.pose.position.x = viz_point.x();
  //     ps.pose.position.y = viz_point.y();
  //     ps.pose.position.z = 0;
  //     ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.heading());
  //     ros_path.poses.push_back(ps);
  //   }
  // }
  // pub_routing_path_.publish(ros_path);

  visualization_msgs::MarkerArray routing_path_markers;
  size_t marker_index = 0;
  for (const auto &path : paths) {
    visualization_msgs::Marker lineStripMain;
    lineStripMain.header.frame_id = map_frame_id_;
    lineStripMain.header.stamp = ros::Time::now();
    lineStripMain.ns = "/apollo_simulation/routing_path";
    lineStripMain.action = visualization_msgs::Marker::ADD;
    lineStripMain.pose.orientation.w = 1.0;
    lineStripMain.id = marker_index;
    lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
    // STRIP markers use only the x component of scale, for the line width
    lineStripMain.scale.x = 0.3;
    // STRIP is red
    lineStripMain.color.a = 1.0;
    lineStripMain.color.r = 1.0;
    lineStripMain.color.g = 0.0;
    lineStripMain.color.b = 0.0;
    marker_index ++;
    for (const auto &point : path.path_points()) {
      auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
            point.x(), point.y(), 0.0));
      geometry_msgs::Point p;
      p.x = viz_point.x();
      p.y = viz_point.y();
      p.z = 0;
      lineStripMain.points.push_back(p);
    }
    routing_path_markers.markers.push_back(lineStripMain);
  }
  {
    visualization_msgs::MarkerArray delete_marker;
    visualization_msgs::Marker marker;
    marker.action = marker.DELETEALL;
    delete_marker.markers.push_back(marker);
    pub_routing_path_.publish(delete_marker);
  }
  pub_routing_path_.publish(routing_path_markers);
}

void SimVehicle::PublishPlanningData(const apollo::planning::ADCTrajectory &trajectory) {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return;
  }

  // reference_line
  {
    if (trajectory.debug().planning_data().path_size() > 0) {
      for (const auto &path : trajectory.debug().planning_data().path()) {
        if (path.name() == "planning_reference_line") {
          nav_msgs::Path ref_path;
          ref_path.header.frame_id = map_frame_id_;
          ref_path.header.stamp = ros::Time::now();
          geometry_msgs::PoseStamped ps;
          ps.header = ref_path.header;
          for (const auto &point : path.path_point()) {
            auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
                  point.x(), point.y(), 0.0));

            ps.pose.position.x = viz_point.x();
            ps.pose.position.y = viz_point.y();
            ps.pose.position.z = 0;
            ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.theta());
            ref_path.poses.push_back(ps);
          }
          pub_reference_line_.publish(ref_path);
          break;
        }
      }
    }
  }

  // trajectory
  {
    nav_msgs::Path path;
    path.header.frame_id = map_frame_id_;
    path.header.stamp = ros::Time::now();
    geometry_msgs::PoseStamped ps;
    ps.header = path.header;
    for (const auto &point : trajectory.trajectory_point()) {
      auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
            point.path_point().x(), point.path_point().y(), 0.0));

      ps.pose.position.x = viz_point.x();
      ps.pose.position.y = viz_point.y();
      ps.pose.position.z = 0;
      ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.path_point().theta());
      path.poses.push_back(ps);
    }
    pub_planning_trajectory_.publish(path);
  }
}

void SimVehicle::TimerCallbackSimulation(const ros::TimerEvent &event) {
  if (!is_initialized_) {
    AINFO << "waiting initial position...";
    return;
  }

  if (prev_update_time_ptr_ == nullptr) {
    prev_update_time_ptr_ = std::make_shared<ros::Time>(ros::Time::now());
  }

  std::lock_guard<std::mutex> lock(mutex_);

  if (control_command_.has_gear_location()) {
    // AINFO << "control_command: " << control_command_.ShortDebugString();
    // AINFO << "steering_target: " << control_command_.steering_target();

    double linear_vel = control_command_.debug().simple_lon_debug().speed_reference();
    double steer_angle = control_command_.debug().simple_lat_debug().steer_angle();
    steer_angle /= (180.0 / M_PI * steer_transmission_ratio_ / steer_single_direction_max_degree_ * 100.0);
    gear_position_ = control_command_.gear_location();

     // ===========================================lzh====================================================start
    // linear_vel和steer_angle是从apollo_control获取到的控制量，把这个两个量发送到底盘
    // geometry_msgs::Twist数据类型中的 linear.x 默认单位是 m/s
    // geometry_msgs::Twist数据类型中的 angular.z 默认单位是 弧度/s，这里代表的是角度
    ros::NodeHandle nh;
    geometry_msgs::Twist chassis_signal;
    chassis_signal.linear.x = linear_vel;                     // 线速度
    chassis_signal.angular.z = steer_angle * 180.0 / M_PI;    // steer_angle从弧度转换成角度
    ROS_INFO_STREAM("-------------- publish linear velocity:\t"
                    << chassis_signal.linear.x << "m/s");
    ROS_INFO_STREAM("-------------- publish steer angle:\t\t"
                    << chassis_signal.angular.z << "degree");
    // ===========================================lzh====================================================end

    if (vehicle_model_type_ == VehicleModelType::IDEAL_STEER ||
      vehicle_model_type_ == VehicleModelType::DELAY_STEER) {
      Eigen::VectorXd input(2);
      input << linear_vel, steer_angle;
      vehicle_model_ptr_->SetInput(input);
    } else if (vehicle_model_type_ == VehicleModelType::DELAY_STEER_ACC) {
      Eigen::VectorXd input(3);
      double drive_shift = (gear_position_ == apollo::canbus::Chassis::GEAR_REVERSE) ? -1.0 : 1.0;
      // 注意：这里的input中acceleration不管前进还是后退都是正向增大的，但是msg中后退是负向增大的
      double acceleration = control_command_.debug().simple_lon_debug().acceleration_cmd();
      // AINFO << "acceleration: " << acceleration;
      input << (acceleration * drive_shift), steer_angle, drive_shift;
      vehicle_model_ptr_->SetInput(input);
    } else {
      AWARN << "invalid vehicle_model_type_ error.";
    }
  }

  /* calculate delta time */
  const double dt = (ros::Time::now() - *prev_update_time_ptr_).toSec();
  *prev_update_time_ptr_ = ros::Time::now();

  /* update vehicle dynamics*/
  vehicle_model_ptr_->Update(dt);

  /* save current vehicle pose & twist */
  //modify by travis begin
  double roll{};
  double pitch{};
  double yaw{};
  if (running_mode_ == "SIMULATOR") {
    current_pose_.position.x = vehicle_model_ptr_->GetX();
    current_pose_.position.y = vehicle_model_ptr_->GetY();
    closest_pos_z_ = 0.0;
    current_pose_.position.z = closest_pos_z_;
    roll = 0.0;
    pitch = 0.0;
    yaw = vehicle_model_ptr_->GetYaw();
    current_twist_.linear.x = vehicle_model_ptr_->GetVx();
    current_twist_.angular.z = vehicle_model_ptr_->GetWz();
  }else{
    current_pose_.position.x = special_pose_.position.x;      // 获取到rtk定位信息横坐标
    current_pose_.position.y = special_pose_.position.y;      // 获取到rtk定位信息纵坐标
    closest_pos_z_ = 0.0;
    current_pose_.position.z = closest_pos_z_;
    roll = 0.0;
    pitch = 0.0;
    yaw = special_pose_.position.z;                    // 获取到rtk的航向角
    current_twist_.linear.x = 2;
    current_twist_.angular.z = 0;


    std::cout << "========>   current_pose_.position.x : " << current_pose_.position.x << std::endl;
    std::cout << "========>   current_pose_.position.y : " << current_pose_.position.y << std::endl;
    std::cout << "========>   current_pose_.position.x : " << current_pose_.position.x << std::endl;
    std::cout << "========>   current_pose_.position.y : " << current_pose_.position.y << std::endl;
    std::cout << "========>   yaw                      : " << yaw << std::endl;
    std::cout << "========>   current_twist_.linear.x  : " << current_twist_.linear.x << std::endl;
    std::cout << "========>   current_twist_.angular.z : " << current_twist_.angular.z << std::endl;
  }

  //modify by travis end
  double linear_acc = vehicle_model_ptr_->GetACCx();

  if (add_measurement_noise_) {
    current_pose_.position.x += (*pos_norm_dist_ptr_)(*rand_engine_ptr_);
    current_pose_.position.y += (*pos_norm_dist_ptr_)(*rand_engine_ptr_);
    current_pose_.position.z += (*pos_norm_dist_ptr_)(*rand_engine_ptr_);
    roll += (*rpy_norm_dist_ptr_)(*rand_engine_ptr_);
    pitch += (*rpy_norm_dist_ptr_)(*rand_engine_ptr_);
    yaw += (*rpy_norm_dist_ptr_)(*rand_engine_ptr_);
    if (current_twist_.linear.x >= 0.0) {
      current_twist_.linear.x += (*vel_norm_dist_ptr_)(*rand_engine_ptr_);
    } else {
      current_twist_.linear.x -= (*vel_norm_dist_ptr_)(*rand_engine_ptr_);
    }
    current_twist_.angular.z += (*angvel_norm_dist_ptr_)(*rand_engine_ptr_);
  }

  current_pose_.orientation = GetQuaternionFromRPY(roll, pitch, yaw);

  /* publish pose */
  ros::Time current_time = ros::Time::now();
  geometry_msgs::PoseStamped sim_ps;
  sim_ps.header.frame_id = map_frame_id_;
  sim_ps.header.stamp = current_time;
  sim_ps.pose = current_pose_;
  pub_pose_.publish(sim_ps);

  /* publish speed */
  pub_speed_.publish(current_twist_);

  /* publish for apollo */
  double steering_angle = vehicle_model_ptr_->GetSteer();
  if (add_measurement_noise_) {
    steering_angle += (*steer_norm_dist_ptr_)(*rand_engine_ptr_);
  }
  PublishChassis(steering_angle, current_twist_, gear_position_);
  PublishLocalization(current_pose_, current_twist_, linear_acc);

  hdmap_visualizer_.SetVizCenterPointUseVizPose(PointFactory::ToPointENU(
      current_pose_.position.x, current_pose_.position.y, 0.0));

  /* publish tf */
  PublishTF(current_pose_);
}

void SimVehicle::CallbackNavGoalPoseStamped(const geometry_msgs::PoseStamped::ConstPtr &msg) {
  RoutingRequest routing_request;
  // current pose
  auto start_point = hdmap_visualizer_.VizPointToHDMapPoint(PointFactory::ToPointENU(
        current_pose_.position.x, current_pose_.position.y, 0.0));
  //modify by travis begin
  // if (!ConstructLaneWayPoint(start_point.x(), start_point.y(), routing_request.add_waypoint())) {
  if (!ConstructLaneWayPoint(323858.413909887, 3463680.45143366, routing_request.add_waypoint())) {  
  //modify by travis end
    AERROR << "Failed to prepare a routing request:"
      << " cannot locate start point on map.";
    return;
  }
  // nav goal pose
  auto end_point = hdmap_visualizer_.VizPointToHDMapPoint(PointFactory::ToPointENU(
        msg->pose.position.x, msg->pose.position.y, 0.0));
  if (!ConstructLaneWayPoint(end_point.x(), end_point.y(), routing_request.add_waypoint())) {
    AERROR << "Failed to prepare a routing request:"
      << " cannot locate end point on map.";
    return;
  }
  // parking info
  apollo::routing::ParkingInfo parking_info;
  if (ConstructPakingInfo(end_point.x(), end_point.y(), &parking_info)) {
    routing_request.mutable_parking_info()->CopyFrom(parking_info);
  }
  // pub routing request
  apollo::common::util::FillHeader("SimVehicle", &routing_request);
  std_msgs::String ros_msg;
  ros_msg.data = routing_request.SerializeAsString();
  routing_writer_.publish(ros_msg); 
  AINFO << "Constructed RoutingRequest to be sent:\n"
    << routing_request.DebugString();
}

void SimVehicle::CallbackInitialPoseWithCov(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg) {
  geometry_msgs::Twist initial_twist;  // initialized with zero for all components
  if (initial_twist_ptr_) {
    initial_twist = initial_twist_ptr_->twist;
  }
  // save initial pose
  initial_pose_with_cov_ptr_ = msg;
  SetInitialStateWithPoseTransform(*initial_pose_with_cov_ptr_, initial_twist);
}

void SimVehicle::CallbackInitialPoseStamped(const geometry_msgs::PoseStamped::ConstPtr &msg) {
  geometry_msgs::Twist initial_twist;  // initialized with zero for all components
  if (initial_twist_ptr_) {
    initial_twist = initial_twist_ptr_->twist;
  }
  // save initial pose
  initial_pose_ptr_ = msg;
  SetInitialStateWithPoseTransform(*initial_pose_ptr_, initial_twist);
}

void SimVehicle::CallbackInitialTwistStamped(const geometry_msgs::TwistStamped::ConstPtr &msg) {
  // save initial pose
  initial_twist_ptr_ = msg;
  if (initial_pose_ptr_) {
    SetInitialStateWithPoseTransform(*initial_pose_ptr_, initial_twist_ptr_->twist);
    // input twist to simulator's internal parameter
    current_pose_ = initial_pose_ptr_->pose;
    current_twist_ = initial_twist_ptr_->twist;
  } else if (initial_pose_with_cov_ptr_) {
    SetInitialStateWithPoseTransform(*initial_pose_with_cov_ptr_, initial_twist_ptr_->twist);
  }
}

void SimVehicle::SetInitialStateWithPoseTransform(
  const geometry_msgs::PoseStamped &pose_stamped, const geometry_msgs::Twist &twist) {
  geometry_msgs::TransformStamped transform;
  GetTransformFromTF(map_frame_id_, pose_stamped.header.frame_id, transform);
  geometry_msgs::Pose pose;
  pose.position.x = pose_stamped.pose.position.x + transform.transform.translation.x;
  pose.position.y = pose_stamped.pose.position.y + transform.transform.translation.y;
  pose.position.z = pose_stamped.pose.position.z + transform.transform.translation.z;
  pose.orientation = pose_stamped.pose.orientation;
  SetInitialState(pose, twist);
}

void SimVehicle::SetInitialStateWithPoseTransform(const geometry_msgs::PoseWithCovarianceStamped &pose,
  const geometry_msgs::Twist &twist) {
  geometry_msgs::PoseStamped ps;
  ps.header = pose.header;
  ps.pose = pose.pose.pose;
  SetInitialStateWithPoseTransform(ps, twist);
}

void SimVehicle::SetInitialState(const geometry_msgs::Pose &pose, const geometry_msgs::Twist &twist) {
  const double x = pose.position.x;
  const double y = pose.position.y;
  const double yaw = tf2::getYaw(pose.orientation);
  const double vx = twist.linear.x;
  const double steer = 0.0;
  const double acc = 0.0;

  if (vehicle_model_type_ == VehicleModelType::IDEAL_STEER) {
    Eigen::VectorXd state(3);
    state << x, y, yaw;
    vehicle_model_ptr_->SetState(state);
  } else if (vehicle_model_type_ == VehicleModelType::DELAY_STEER) {
    Eigen::VectorXd state(5);
    state << x, y, yaw, vx, steer;
    vehicle_model_ptr_->SetState(state);
  } else if (vehicle_model_type_ == VehicleModelType::DELAY_STEER_ACC) {
    Eigen::VectorXd state(6);
    state << x, y, yaw, vx, steer, acc;
    vehicle_model_ptr_->SetState(state);
  } else {
    AWARN << "undesired vehicle model type! Initialization failed.";
    return;
  }

  is_initialized_ = true;
}

void SimVehicle::GetTransformFromTF(const std::string parent_frame, const std::string child_frame,
  geometry_msgs::TransformStamped &transform) {
  while (ros::ok()) {
    try {
      transform = tf_buffer_.lookupTransform(parent_frame, child_frame, ros::Time(0));
      break;
    } catch (tf2::TransformException &ex) {
      AERROR << ex.what();
    }
  }
}

void SimVehicle::PublishTF(const geometry_msgs::Pose &pose) {
  ros::Time current_time = ros::Time::now();

  // send odom transform
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.stamp = current_time;
  odom_trans.header.frame_id = map_frame_id_;
  odom_trans.child_frame_id = car_frame_id_;
  odom_trans.transform.translation.x = pose.position.x;
  odom_trans.transform.translation.y = pose.position.y;
  odom_trans.transform.translation.z = pose.position.z;
  odom_trans.transform.rotation = pose.orientation;
  tf_broadcaster_.sendTransform(odom_trans);
}

geometry_msgs::Quaternion SimVehicle::GetQuaternionFromRPY(
  const double &roll, const double &pitch, const double &yaw) {
  tf2::Quaternion q;
  q.setRPY(roll, pitch, yaw);
  return tf2::toMsg(q);
}

double SimVehicle::GetNumberFromXMLRPC(XmlRpc::XmlRpcValue &value, const std::string &full_param_name) {
  // Make sure that the value we're looking at is either a double or an int.
  if (value.getType() != XmlRpc::XmlRpcValue::TypeInt && value.getType() != XmlRpc::XmlRpcValue::TypeDouble) {
    std::string &value_string = value;
    AFATAL << "Values in the footprint specification (param " << full_param_name << ") must be numbers. Found value " << value_string << ".";
    throw std::runtime_error("Values in the footprint specification must be numbers");
  }
  return value.getType() == XmlRpc::XmlRpcValue::TypeInt ? (int)(value) : (double)(value);
}

std::vector<geometry_msgs::Point> SimVehicle::MakeFootprintFromXMLRPC(XmlRpc::XmlRpcValue &footprint_xmlrpc,
  const std::string &full_param_name, const size_t min_size) {
  // Make sure we have an array of at least 3 elements.
  if (footprint_xmlrpc.getType() != XmlRpc::XmlRpcValue::TypeArray || footprint_xmlrpc.size() < min_size) {
    AFATAL << "The footprint must be specified as list of lists on the parameter server, " << full_param_name 
           << " was specified as " << footprint_xmlrpc;
    throw std::runtime_error("The footprint must be specified as list of lists on the parameter server with at least "
      "1 points eg: [[x1, y1], [x2, y2], ..., [xn, yn]]");
  }
  std::vector<geometry_msgs::Point> footprint;
  geometry_msgs::Point pt;
  for (int i = 0; i < footprint_xmlrpc.size(); ++i) {
    // Make sure each element of the list is an array of size 2. (x and y coordinates)
    XmlRpc::XmlRpcValue point = footprint_xmlrpc[ i ];
    if (point.getType() != XmlRpc::XmlRpcValue::TypeArray || point.size() != 2) {
      AFATAL << "The footprint (parameter " << full_param_name << ") must be specified as list of lists on the parameter server eg: "
        "[[x1, y1], [x2, y2], ..., [xn, yn]], but this spec is not of that form.";
      throw std::runtime_error("The footprint must be specified as list of lists on the parameter server eg: "
        "[[x1, y1], [x2, y2], ..., [xn, yn]], but this spec is not of that form");
    }
    pt.x = GetNumberFromXMLRPC(point[0], full_param_name);
    pt.y = GetNumberFromXMLRPC(point[1], full_param_name);
    footprint.push_back(pt);
  }
  return footprint;
}

bool SimVehicle::ConstructPakingInfo(const double x, const double y,
  apollo::routing::ParkingInfo *parking_info) const {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return false;
  }

  auto ref_point = PointFactory::ToPointENU(x, y, 0.0);

  std::vector<ParkingSpaceInfoConstPtr> parking_spaces;
  const int status = hdmap_visualizer_.hdmap()->GetParkingSpaces(ref_point, 10.0, &parking_spaces);
  if (status < 0 || parking_spaces.empty()) {
    AWARN << "failed to get parking spaces.";
    return false;
  }

  for (const auto &parking_space : parking_spaces) {
    if (parking_space->polygon().IsPointIn(PointFactory::ToVec2d(ref_point))) {
      parking_info->set_parking_space_id(parking_space->id().id());
      double parking_point_x = 0.5 * (parking_space->polygon().max_x() + parking_space->polygon().min_x());
      double parking_point_y = 0.5 * (parking_space->polygon().max_y() + parking_space->polygon().min_y());
      auto *parking_point = parking_info->mutable_parking_point();
      parking_point->set_x(parking_point_x);
      parking_point->set_y(parking_point_y);
      parking_point->set_z(0.0);
      parking_info->set_parking_space_type(ParkingSpaceType::VERTICAL_PLOT);
      auto *points = parking_info->mutable_corner_point()->mutable_point();
      for (const auto &point : parking_space->polygon().points()) {
        auto *p = points->Add();
        p->set_x(point.x());
        p->set_y(point.y());
      }
      break;
    }
  }

  return true;
}

bool SimVehicle::ConstructLaneWayPoint(const double x, const double y,
  apollo::routing::LaneWaypoint *laneWayPoint) const {
  double s, l;
  LaneInfoConstPtr lane;
  if (!GetNearestLane(x, y, &lane, &s, &l)) {
    return false;
  }

  laneWayPoint->set_id(lane->id().id());
  laneWayPoint->set_s(s);
  auto *pose = laneWayPoint->mutable_pose();
  pose->set_x(x);
  pose->set_y(y);

  return true;
}

bool SimVehicle::GetNearestLane(const double x, const double y,
  LaneInfoConstPtr *nearest_lane, double *nearest_s, double *nearest_l) const {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return false;
  }

  PointENU point;
  point.set_x(x);
  point.set_y(y);
  if (hdmap_visualizer_.hdmap()->GetNearestLane(point, nearest_lane, nearest_s, nearest_l) < 0) {
    AERROR << "Failed to get nearest lane!";
    return false;
  }
  return true;
}