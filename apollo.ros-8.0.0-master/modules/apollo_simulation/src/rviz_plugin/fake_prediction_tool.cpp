/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "fake_prediction_tool.h"

#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>

#include <rviz/display_context.h>
#include <rviz/ogre_helpers/arrow.h>
#include <rviz/properties/string_property.h>

#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(rviz::FakePredictionTool, rviz::Tool)

namespace rviz {

FakePredictionTool::FakePredictionTool() {
  shortcut_key_ = 'f';
  topic_property_ =
    new StringProperty("Topic", "/fake_prediction",
    "The topic on which to publish fake prediction for apollo.",
    getPropertyContainer(), SLOT(updateTopic()), this);
}

void FakePredictionTool::onInitialize() {
  PoseTool::onInitialize();
  arrow_->setColor(1.0f, 1.0f, 0.0f, 1.0f);
  setName("Apollo Fake Prediction");
  updateTopic();
}

void FakePredictionTool::updateTopic() {
  try {
    pub_ = nh_.advertise<geometry_msgs::PoseStamped>
      (topic_property_->getStdString(), 1);
  } catch (const ros::Exception &e) {
    ROS_ERROR_STREAM_NAMED("FakePredictionTool", e.what());
  }
}

void FakePredictionTool::onPoseSet(double x, double y, double theta) {
  std::string fixed_frame = context_->getFixedFrame().toStdString();
  tf::Quaternion quat;
  quat.setRPY(0.0, 0.0, theta);
  tf::Stamped<tf::Pose> p =
    tf::Stamped<tf::Pose>(tf::Pose(quat, tf::Point(x, y, 0.0)), ros::Time::now(),
      fixed_frame);
  geometry_msgs::PoseStamped target;
  tf::poseStampedTFToMsg(p, target);
  ROS_INFO("Setting fake prediction: Frame:%s, Position(%.3f, %.3f, %.3f), Orientation(%.3f, %.3f, %.3f, %.3f) = "
    "Angle: %.3f\n",
    fixed_frame.c_str(), target.pose.position.x, target.pose.position.y,
    target.pose.position.z,
    target.pose.orientation.x, target.pose.orientation.y, target.pose.orientation.z,
    target.pose.orientation.w, theta);
  pub_.publish(target);
}

} // end namespace rviz
