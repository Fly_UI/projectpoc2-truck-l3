/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_simulation/sim_control.h"

#include <cmath>

#include "apollo_common/log.h"
#include "apollo_common/math/linear_interpolation.h"
#include "apollo_common/math/math_utils.h"
#include "apollo_common/math/quaternion.h"
#include "apollo_common/time/time.h"
#include "apollo_common/time/clock.h"
#include "apollo_common/file.h"
#include "apollo_common/util/util.h"
#include "apollo_common/util/message_util.h"
#include "apollo_common/util/point_factory.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_map/hdmap/hdmap_util.h"
#include "apollo_map/pnc_map/pnc_map.h"
#include "apollo_msgs/localization_msgs/localization.pb.h"
#include "apollo_msgs/chassis_msgs/chassis.pb.h"
#include "apollo_msgs/basic_msgs/geometry.pb.h"

namespace apollo {

using apollo::canbus::Chassis;
using apollo::common::Header;
using apollo::common::Point3D;
using apollo::common::Quaternion;
using apollo::common::TrajectoryPoint;
using apollo::common::PointENU;
using apollo::common::math::HeadingToQuaternion;
using apollo::common::math::InverseQuaternionRotate;
using apollo::common::math::InterpolateUsingLinearApproximation;
using apollo::common::math::NormalizeAngle;
using apollo::common::math::QuaternionToHeading;
using apollo::common::time::Clock;
using apollo::localization::LocalizationEstimate;
using apollo::routing::RoutingRequest;
using apollo::routing::RoutingResponse;
using apollo::hdmap::LaneInfoConstPtr;
using apollo::hdmap::RouteSegments;
using apollo::hdmap::HDMapUtil;
using apollo::hdmap::Path;
using apollo::hdmap::PncMap;
using apollo::hdmap::MapPathPoint;
using apollo::common::util::PointFactory;
using apollo::hdmap::ParkingSpaceInfoConstPtr;
using apollo::routing::ParkingSpaceType;

namespace {

void TransformToVRF(const Point3D &point_mrf, const Quaternion &orientation,
  Point3D *point_vrf) {
  Eigen::Vector3d v_mrf(point_mrf.x(), point_mrf.y(), point_mrf.z());
  auto v_vrf = InverseQuaternionRotate(orientation, v_mrf);
  point_vrf->set_x(v_vrf.x());
  point_vrf->set_y(v_vrf.y());
  point_vrf->set_z(v_vrf.z());
}

bool IsSameHeader(const Header &lhs, const Header &rhs) {
  return lhs.sequence_num() == rhs.sequence_num() &&
    lhs.timestamp_sec() == rhs.timestamp_sec();
}

}  // namespace

SimControl::SimControl() {
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  sub_nav_goal_ = nh.subscribe("/move_base_simple/goal", 10, &SimControl::CallbackNavGoalPose, this);
  sub_initial_pose_ = nh.subscribe("/initial_pose", 10, &SimControl::CallbackInitialPose, this);
  pub_sim_pose_ = private_nh.advertise<geometry_msgs::PoseStamped>("sim_pose", 1);
  // pub_routing_path_ = private_nh.advertise<nav_msgs::Path>("path", 1);
  pub_routing_path_ = private_nh.advertise<visualization_msgs::MarkerArray>("routing/path", 1);
  pub_reference_line_ = private_nh.advertise<nav_msgs::Path>("planning/reference_line", 1);
  pub_planning_trajectory_ = private_nh.advertise<nav_msgs::Path>("planning/trajectory", 1);

  private_nh.param<std::string>("car_frame_id", car_frame_id_, std::string("base_link"));
  private_nh.param<std::string>("map_frame_id", map_frame_id_, std::string("map"));

  fake_predictor_ = std::make_shared<FakePredictor>(hdmap_visualizer_);

  Start();
}

void SimControl::Init(bool set_start_point, double start_velocity,
  double start_acceleration) {
  // Setup planning and routing result data callback.
  ros::NodeHandle nh;
  routing_reader_ = nh.subscribe(FLAGS_routing_response_topic, 5, 
      &SimControl::OnRoutingResponse, this);
  planning_reader_ = nh.subscribe(FLAGS_planning_trajectory_topic, 5, 
      &SimControl::OnPlanning, this);

  chassis_writer_ = nh.advertise<std_msgs::String>(FLAGS_chassis_topic, 1);
  localization_writer_ = nh.advertise<std_msgs::String>(FLAGS_localization_topic, 1);
  routing_writer_ = nh.advertise<std_msgs::String>(FLAGS_routing_request_topic, 1);

  // Start timer to publish localization and chassis messages.
  sim_control_timer_ = nh.createTimer(ros::Duration(kSimControlInterval), 
      &SimControl::TimerCallback, this);

  if (set_start_point) {
    // Start from origin to find a lane from the map.
    double s, l;
    LaneInfoConstPtr lane;
    if (!GetNearestLane(0.0, 0.0, &lane, &s, &l)) {
      AWARN << "Failed to get a dummy start point from map!";
      return;
    }

    auto start_point = lane->GetSmoothPoint(0.0);
    double theta = lane->Heading(s);

    TrajectoryPoint point;
    point.mutable_path_point()->set_x(start_point.x());
    point.mutable_path_point()->set_y(start_point.y());
    point.mutable_path_point()->set_z(start_point.z());
    point.set_v(start_velocity);
    point.set_a(start_acceleration);
    point.mutable_path_point()->set_theta(theta);
    SetStartPoint(point);

    hdmap_visualizer_.SetVizOffsetPointUseHDMapPose(PointFactory::ToPointENU(
        start_point.x(), start_point.y(), 0.0));
  }

  start_velocity_ = start_velocity;
  start_acceleration_ = start_acceleration;
}

void SimControl::SetStartPoint(const TrajectoryPoint &start_point) {
  AINFO << "new start point[" << start_point.path_point().x() << ", "
    << start_point.path_point().y() << "," << start_point.path_point().theta() << "]";
  next_point_ = start_point;
  prev_point_index_ = next_point_index_ = 0;
  received_planning_ = false;
}

void SimControl::ClearPlanning() {
  current_trajectory_.Clear();
  received_planning_ = false;
  if (planning_count_ > 0) {
    planning_count_ = 0;
  }
}

void SimControl::Reset() {
  current_routing_header_.Clear();
  re_routing_triggered_ = false;
  ClearPlanning();
}

void SimControl::OnRoutingResponse(const std_msgs::String::ConstPtr &msg) {
  std::lock_guard<std::mutex> lock(mutex_);

  if (!enabled_) {
    return;
  }

  RoutingResponse routing;
  if (!routing.ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  CHECK_GE(routing.routing_request().waypoint_size(), 2)
      << "routing should have at least two waypoints";
  const auto &start_pose = routing.routing_request().waypoint(0).pose();

  current_routing_header_ = routing.header();

  // If this is from a planning re-routing request, don't reset car's location.
  re_routing_triggered_ =
    routing.routing_request().header().module_name() == "planning";
  if (!re_routing_triggered_) {
    ClearPlanning();

    // double s, l;
    // LaneInfoConstPtr nearest_lane;
    // if (!GetNearestLane(start_pose.x(), start_pose.y(), &nearest_lane, &s, &l)) {
    //   return;
    // }
    // double theta = nearest_lane->Heading(s);

    // TrajectoryPoint point;
    // point.mutable_path_point()->set_x(start_pose.x());
    // point.mutable_path_point()->set_y(start_pose.y());
    // point.set_a(0.0);
    // point.set_v(0.0);
    // point.mutable_path_point()->set_theta(theta);
    // SetStartPoint(point);
  }

  PublishRosRoutingPath(routing);
}

void SimControl::Start() {
  if (!enabled_) {
    if (!inited_) {
      // Only place the car when there is not a localization.
      Init(true);
    }
    Reset();
    sim_control_timer_.start();
    enabled_ = true;
  }
}

void SimControl::Stop() {
  if (enabled_) {
    sim_control_timer_.stop();
    enabled_ = false;
  }
}

void SimControl::OnPlanning(const std_msgs::String::ConstPtr &msg) {
  std::lock_guard<std::mutex> lock(mutex_);

  if (!enabled_) {
    return;
  }

  apollo::planning::ADCTrajectory trajectory;
  if (!trajectory.ParseFromString(msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  // AINFO << "planning trajectory: " << trajectory.ShortDebugString();
  // AINFO << "planning trajectory path_point size: " << trajectory.path_point().size();
  // AINFO << "planning trajectory trajectory_point size: " << trajectory.trajectory_point().size();
  // AINFO << "planning trajectory adc_path_point size: " << trajectory.adc_path_point().size();
  // AINFO << "planning trajectory adc_trajectory_point size: " << trajectory.adc_trajectory_point().size();

  // Reset current trajectory and the indices upon receiving a new trajectory.
  // The routing SimControl owns must match with the one Planning has.
  if (re_routing_triggered_ ||
    IsSameHeader(trajectory.routing_header(), current_routing_header_)) {
    // Hold a few cycles until the position information is fully refreshed on
    // planning side. Don't wait for the very first planning received.
    ++planning_count_;
    if (planning_count_ == 0 || planning_count_ >= kPlanningCountToStart) {
      planning_count_ = kPlanningCountToStart;
      current_trajectory_ = trajectory;
      prev_point_index_ = 0;
      next_point_index_ = 0;
      received_planning_ = true;
    }
  } else {
    ClearPlanning();
  }

  if (trajectory.header().status().error_code() != apollo::common::ErrorCode::OK) {
    ClearPlanning();
  }

  if (trajectory.debug().planning_data().path_size() > 0) {
    for (const auto &path : trajectory.debug().planning_data().path()) {
      if (path.name() == "planning_reference_line") {
        PublishRosReferenceLine(path);
        break;
      }
    }
  }
  PublishRosPlanningTrajectory(trajectory);
}

void SimControl::Freeze() {
  next_point_.set_v(0.0);
  next_point_.set_a(0.0);
  prev_point_ = next_point_;
}

void SimControl::TimerCallback(const ros::TimerEvent &event) {
  RunOnce();
}

void SimControl::RunOnce() {
  TrajectoryPoint trajectory_point;
  Chassis::GearPosition gear_position;
  if (!PerfectControlModel(&trajectory_point, &gear_position)) {
    AERROR << "Failed to calculate next point with perfect control model";
    return;
  }

  PublishChassis(trajectory_point.v(), gear_position);
  PublishLocalization(trajectory_point);

  PublishTF(trajectory_point);
  PublishRosLocalization(trajectory_point);

  hdmap_visualizer_.SetVizCenterPointUseHDMapPose(PointFactory::ToPointENU(
      trajectory_point.path_point().x(), trajectory_point.path_point().y(), 0.0));
}

bool SimControl::PerfectControlModel(TrajectoryPoint *point,
  Chassis::GearPosition *gear_position) {
  std::lock_guard<std::mutex> lock(mutex_);

  // Result of the interpolation.
  auto relative_time =
    Clock::NowInSeconds() - current_trajectory_.header().timestamp_sec();
  const auto &trajectory = current_trajectory_.trajectory_point();
  *gear_position = current_trajectory_.gear();

  if (!received_planning_) {
    prev_point_ = next_point_;
  } else {
    if (current_trajectory_.estop().is_estop() ||
      next_point_index_ >= trajectory.size()) {
      // Freeze the car when there's an estop or the current trajectory has
      // been exhausted.
      Freeze();
    } else {
      // Determine the status of the car based on received planning message.
      while (next_point_index_ < trajectory.size() &&
        relative_time >
        trajectory.Get(next_point_index_).relative_time()) {
        ++next_point_index_;
      }

      if (next_point_index_ == 0) {
        AERROR << "First trajectory point is a future point!";
        return false;
      }

      if (next_point_index_ >= trajectory.size()) {
        next_point_index_ = trajectory.size() - 1;
      }
      prev_point_index_ = next_point_index_ - 1;

      next_point_ = trajectory.Get(next_point_index_);
      prev_point_ = trajectory.Get(prev_point_index_);
    }
  }

  if (relative_time > next_point_.relative_time()) {
    // Don't try to extrapolate if relative_time passes last point
    *point = next_point_;
  } else {
    *point = InterpolateUsingLinearApproximation(prev_point_, next_point_,
        relative_time);
  }
  return true;
}

void SimControl::PublishChassis(double cur_speed,
  Chassis::GearPosition gear_position) {
  Chassis chassis;
  common::util::FillHeader("SimControl", &chassis);

  chassis.set_engine_started(true);
  chassis.set_driving_mode(Chassis::COMPLETE_AUTO_DRIVE);
  chassis.set_gear_location(gear_position);

  chassis.set_speed_mps(static_cast<float>(cur_speed));
  if (gear_position == canbus::Chassis::GEAR_REVERSE) {
    chassis.set_speed_mps(-chassis.speed_mps());
  }
  chassis.set_throttle_percentage(0.0);
  chassis.set_brake_percentage(0.0);

  std_msgs::String ros_msg;
  ros_msg.data = chassis.SerializeAsString();
  chassis_writer_.publish(ros_msg); 
}

void SimControl::PublishLocalization(const TrajectoryPoint &point) {
  LocalizationEstimate localization;
  common::util::FillHeader("SimControl", &localization);

  auto *pose = localization.mutable_pose();
  auto prev = prev_point_.path_point();
  auto next = next_point_.path_point();

  // Set position
  pose->mutable_position()->set_x(point.path_point().x());
  pose->mutable_position()->set_y(point.path_point().y());
  pose->mutable_position()->set_z(point.path_point().z());
  // Set orientation and heading
  double cur_theta = point.path_point().theta();

  Eigen::Quaternion<double> cur_orientation =
    HeadingToQuaternion<double>(cur_theta);
  pose->mutable_orientation()->set_qw(cur_orientation.w());
  pose->mutable_orientation()->set_qx(cur_orientation.x());
  pose->mutable_orientation()->set_qy(cur_orientation.y());
  pose->mutable_orientation()->set_qz(cur_orientation.z());
  pose->set_heading(cur_theta);

  // Set linear_velocity
  pose->mutable_linear_velocity()->set_x(std::cos(cur_theta) * point.v());
  pose->mutable_linear_velocity()->set_y(std::sin(cur_theta) * point.v());
  pose->mutable_linear_velocity()->set_z(0);

  // Set angular_velocity in both map reference frame and vehicle reference
  // frame
  pose->mutable_angular_velocity()->set_x(0);
  pose->mutable_angular_velocity()->set_y(0);
  pose->mutable_angular_velocity()->set_z(point.v() *
    point.path_point().kappa());

  TransformToVRF(pose->angular_velocity(), pose->orientation(),
    pose->mutable_angular_velocity_vrf());

  // Set linear_acceleration in both map reference frame and vehicle reference
  // frame
  auto *linear_acceleration = pose->mutable_linear_acceleration();
  linear_acceleration->set_x(std::cos(cur_theta) * point.a());
  linear_acceleration->set_y(std::sin(cur_theta) * point.a());
  linear_acceleration->set_z(0);

  TransformToVRF(pose->linear_acceleration(), pose->orientation(),
    pose->mutable_linear_acceleration_vrf());

  std_msgs::String ros_msg;
  ros_msg.data = localization.SerializeAsString();
  localization_writer_.publish(ros_msg); 

  adc_position_.set_x(pose->position().x());
  adc_position_.set_y(pose->position().y());
  adc_position_.set_z(pose->position().z());
}

void SimControl::CallbackNavGoalPose(const geometry_msgs::PoseStamped::ConstPtr &msg) {
  if (!enabled_) {
    return;
  }

  RoutingRequest routing_request;
  // current pose
  if (!ConstructLaneWayPoint(next_point_.path_point().x(), next_point_.path_point().y(),
      routing_request.add_waypoint())) {
    AERROR << "Failed to prepare a routing request:"
      << " cannot locate start point on map.";
    return;
  }

  // nav goal pose
  auto end_point = hdmap_visualizer_.VizPointToHDMapPoint(PointFactory::ToPointENU(
        msg->pose.position.x, msg->pose.position.y, 0.0));
  if (!ConstructLaneWayPoint(end_point.x(), end_point.y(), routing_request.add_waypoint())) {
    AERROR << "Failed to prepare a routing request:"
      << " cannot locate end point on map.";
    return;
  }

  // parking info
  apollo::routing::ParkingInfo parking_info;
  if (ConstructPakingInfo(end_point.x(), end_point.y(), &parking_info)) {
    routing_request.mutable_parking_info()->CopyFrom(parking_info);
  }

  // pub routing request
  common::util::FillHeader("SimControl", &routing_request);
  std_msgs::String ros_msg;
  ros_msg.data = routing_request.SerializeAsString();
  routing_writer_.publish(ros_msg); 

  // AINFO << "Constructed RoutingRequest to be sent:\n"
  //   << routing_request.DebugString();
}

void SimControl::CallbackInitialPose(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg) {
  if (!enabled_) {
    return;
  }

  auto start_pose = hdmap_visualizer_.VizPointToHDMapPoint(PointFactory::ToPointENU(
        msg->pose.pose.position.x, msg->pose.pose.position.y, 0.0));

  double s, l;
  LaneInfoConstPtr nearest_lane;
  if (!GetNearestLane(start_pose.x(), start_pose.y(), &nearest_lane, &s, &l)) {
    return;
  }
  // 如果标记位置在车道内，就用车道方向
  double theta = nearest_lane->Heading(s);
  double half_lane_width = nearest_lane->GetWidth(s) / 2.0;
  if (l > half_lane_width || l < -half_lane_width) {
    double roll, pitch, yaw;
    tf2::getEulerYPR(msg->pose.pose.orientation, yaw, pitch, roll);
    theta = yaw;
  }

  TrajectoryPoint point;
  point.mutable_path_point()->set_x(start_pose.x());
  point.mutable_path_point()->set_y(start_pose.y());
  point.set_a(0.0);
  point.set_v(0.0);
  point.mutable_path_point()->set_theta(theta);
  SetStartPoint(point);
}

void SimControl::PublishTF(const apollo::common::TrajectoryPoint &point) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
        point.path_point().x(), point.path_point().y(), 0.0));

  geometry_msgs::TransformStamped map2base_link_trans;
  map2base_link_trans.header.stamp = ros::Time::now();
  map2base_link_trans.header.frame_id = map_frame_id_;
  map2base_link_trans.child_frame_id = car_frame_id_;
  map2base_link_trans.transform.translation.x = viz_point.x();
  map2base_link_trans.transform.translation.y = viz_point.y();
  map2base_link_trans.transform.translation.z = 0.0;
  map2base_link_trans.transform.rotation = GetQuaternionFromRPY(0.0, 0.0, point.path_point().theta());
  tf_broadcaster_.sendTransform(map2base_link_trans);
}

void SimControl::PublishRosLocalization(const apollo::common::TrajectoryPoint &point) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
        point.path_point().x(), point.path_point().y(), 0.0));

  geometry_msgs::PoseStamped sim_pose;
  sim_pose.header.frame_id = map_frame_id_;
  sim_pose.header.stamp = ros::Time::now();;
  sim_pose.pose.position.x = viz_point.x();
  sim_pose.pose.position.y = viz_point.y();
  sim_pose.pose.position.z = 0.0;
  sim_pose.pose.orientation = GetQuaternionFromRPY(0.0, 0.0, point.path_point().theta());

  pub_sim_pose_.publish(sim_pose);
}

void SimControl::PublishRosRoutingPath(const RoutingResponse &routing) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  std::vector<Path> paths;
  for (const auto &road : routing.road()) {
    for (const auto &passage_region : road.passage()) {
      std::vector<MapPathPoint> path_points;
      for (const auto &segment : passage_region.segment()) {
        auto lane_ptr = hdmap_visualizer_.hdmap()->GetLaneById(apollo::hdmap::MakeMapId(segment.id()));
        if (!lane_ptr) {
          AERROR << "Failed to find lane: " << segment.id();
          return;
        }
        if (segment.start_s() >= segment.end_s()) {
          continue;
        }
        auto points = MapPathPoint::GetPointsFromLane(lane_ptr, segment.start_s(), segment.end_s());
        path_points.insert(path_points.end(), points.begin(), points.end());
      }
      if (path_points.size() < 2) {
        return;
      }
      paths.emplace_back(Path(std::move(path_points)));
    }
  }

  // nav_msgs::Path ros_path;
  // ros_path.header.frame_id = map_frame_id_;
  // ros_path.header.stamp = ros::Time::now();
  // geometry_msgs::PoseStamped ps;
  // ps.header = ros_path.header;
  // for (const auto &path : paths) {
  //   for (const auto &point : path.path_points()) {
  //     auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
  //           point.x(), point.y(), 0.0));

  //     ps.pose.position.x = viz_point.x();
  //     ps.pose.position.y = viz_point.y();
  //     ps.pose.position.z = 0;
  //     ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.heading());
  //     ros_path.poses.push_back(ps);
  //   }
  // }
  // pub_routing_path_.publish(ros_path);

  visualization_msgs::MarkerArray routing_path_markers;
  size_t marker_index = 0;
  for (const auto &path : paths) {
    visualization_msgs::Marker lineStripMain;
    lineStripMain.header.frame_id = map_frame_id_;
    lineStripMain.header.stamp = ros::Time::now();
    lineStripMain.ns = "/apollo_simulation/routing_path";
    lineStripMain.action = visualization_msgs::Marker::ADD;
    lineStripMain.pose.orientation.w = 1.0;
    lineStripMain.id = marker_index;
    lineStripMain.type = visualization_msgs::Marker::LINE_STRIP;
    // STRIP markers use only the x component of scale, for the line width
    lineStripMain.scale.x = 0.3;
    // STRIP is red
    lineStripMain.color.a = 1.0;
    lineStripMain.color.r = 1.0;
    lineStripMain.color.g = 0.0;
    lineStripMain.color.b = 0.0;
    marker_index ++;
    for (const auto &point : path.path_points()) {
      auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
            point.x(), point.y(), 0.0));
      geometry_msgs::Point p;
      p.x = viz_point.x();
      p.y = viz_point.y();
      p.z = 0;
      lineStripMain.points.push_back(p);
    }
    routing_path_markers.markers.push_back(lineStripMain);
  }
  {
    visualization_msgs::MarkerArray delete_marker;
    visualization_msgs::Marker marker;
    marker.action = marker.DELETEALL;
    delete_marker.markers.push_back(marker);
    pub_routing_path_.publish(delete_marker);
  }
  pub_routing_path_.publish(routing_path_markers);
}

void SimControl::PublishRosReferenceLine(const apollo::common::Path &path) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  nav_msgs::Path ref_path;
  ref_path.header.frame_id = map_frame_id_;
  ref_path.header.stamp = ros::Time::now();
  geometry_msgs::PoseStamped ps;
  ps.header = ref_path.header;
  for (const auto &point : path.path_point()) {
    auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
          point.x(), point.y(), 0.0));

    ps.pose.position.x = viz_point.x();
    ps.pose.position.y = viz_point.y();
    ps.pose.position.z = 0;
    ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.theta());
    ref_path.poses.push_back(ps);
  }
  pub_reference_line_.publish(ref_path);
}

void SimControl::PublishRosPlanningTrajectory(const apollo::planning::ADCTrajectory &trajectory) {
  if (!hdmap_visualizer_.hdmap()) {
    return;
  }

  nav_msgs::Path path;
  path.header.frame_id = map_frame_id_;
  path.header.stamp = ros::Time::now();
  geometry_msgs::PoseStamped ps;
  ps.header = path.header;
  for (const auto &point : trajectory.trajectory_point()) {
    auto viz_point = hdmap_visualizer_.HDMapPointToVizPoint(PointFactory::ToPointENU(
          point.path_point().x(), point.path_point().y(), 0.0));

    ps.pose.position.x = viz_point.x();
    ps.pose.position.y = viz_point.y();
    ps.pose.position.z = 0;
    ps.pose.orientation = GetQuaternionFromRPY(0, 0, point.path_point().theta());
    path.poses.push_back(ps);
  }
  pub_planning_trajectory_.publish(path);
}

bool SimControl::ConstructPakingInfo(const double x, const double y,
  apollo::routing::ParkingInfo *parking_info) const {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return false;
  }

  auto ref_point = PointFactory::ToPointENU(x, y, 0.0);

  std::vector<ParkingSpaceInfoConstPtr> parking_spaces;
  const int status = hdmap_visualizer_.hdmap()->GetParkingSpaces(ref_point, 10.0, &parking_spaces);
  if (status < 0 || parking_spaces.empty()) {
    AWARN << "failed to get parking spaces.";
    return false;
  }

  for (const auto &parking_space : parking_spaces) {
    if (parking_space->polygon().IsPointIn(PointFactory::ToVec2d(ref_point))) {
      parking_info->set_parking_space_id(parking_space->id().id());
      double parking_point_x = 0.5 * (parking_space->polygon().max_x() + parking_space->polygon().min_x());
      double parking_point_y = 0.5 * (parking_space->polygon().max_y() + parking_space->polygon().min_y());
      auto *parking_point = parking_info->mutable_parking_point();
      parking_point->set_x(parking_point_x);
      parking_point->set_y(parking_point_y);
      parking_point->set_z(0.0);
      parking_info->set_parking_space_type(ParkingSpaceType::VERTICAL_PLOT);
      auto *points = parking_info->mutable_corner_point()->mutable_point();
      for (const auto &point : parking_space->polygon().points()) {
        auto *p = points->Add();
        p->set_x(point.x());
        p->set_y(point.y());
      }
      break;
    }
  }

  return true;
}

bool SimControl::ConstructLaneWayPoint(const double x, const double y,
  apollo::routing::LaneWaypoint *laneWayPoint) const {
  double s, l;
  LaneInfoConstPtr lane;
  if (!GetNearestLane(x, y, &lane, &s, &l)) {
    return false;
  }

  laneWayPoint->set_id(lane->id().id());
  laneWayPoint->set_s(s);
  auto *pose = laneWayPoint->mutable_pose();
  pose->set_x(x);
  pose->set_y(y);

  return true;
}

bool SimControl::GetNearestLane(const double x, const double y,
  LaneInfoConstPtr *nearest_lane, double *nearest_s, double *nearest_l) const {
  if (!hdmap_visualizer_.hdmap()) {
    AERROR << "map is not ready!";
    return false;
  }

  PointENU point;
  point.set_x(x);
  point.set_y(y);
  if (hdmap_visualizer_.hdmap()->GetNearestLane(point, nearest_lane, nearest_s, nearest_l) < 0) {
    AERROR << "Failed to get nearest lane!";
    return false;
  }
  return true;
}

}  // namespace apollo
