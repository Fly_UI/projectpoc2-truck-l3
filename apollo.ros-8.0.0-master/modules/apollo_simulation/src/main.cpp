/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <ros/ros.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>

#include "apollo_common/log.h"
#include "apollo_simulation/sim_vehicle.h"
#include "apollo_simulation/sim_control.h"

int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  ros::init(argc, argv, "apollo_simulation");
  
  ros::NodeHandle private_nh("~");
  bool use_sim_ctrl;
  private_nh.param<bool>("use_sim_ctrl", use_sim_ctrl, false);

  tf2_ros::Buffer buffer(ros::Duration(10));
  tf2_ros::TransformListener tf(buffer);

  std::shared_ptr<SimVehicle> sim_pnc_ptr;
  std::shared_ptr<apollo::SimControl> sim_ctrl_ptr;

  use_sim_ctrl = 0;  //add by travis for the "simulation.launch" changed is not useful

  if (use_sim_ctrl) {
    sim_ctrl_ptr = std::make_shared<apollo::SimControl>();
    std::cout << __LINE__<< std::endl;
  } else {
    sim_pnc_ptr = std::make_shared<SimVehicle>(buffer);
    std::cout << __LINE__<< std::endl;
  }

  // ros::NodeHandle nh;
  // nh.subscribe("Gps2Apollo", 100, &SimVehicle::SubCallback, this);
  //   nh.subscribe("/Gps2Apollo", 100, &SimVehicle::SubCallback, this);

  ros::spin();
  ros::waitForShutdown();

  return 0;
};
