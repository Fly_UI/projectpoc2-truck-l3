^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package apollo_simulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.3 (2023-01-30)
------------------
* add chassis feedback steering percentage
* Contributors: forrest

0.0.2 (2022-10-12)
------------------
* add set dynamic obstacle
* Contributors: forrest

0.0.1 (2022-04-22)
------------------
* finish first version
* Contributors: forrest