/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include "apollo_msgs/basic_msgs/geometry.pb.h"
#include "apollo_map/hdmap/hdmap.h"

class HDMapVisualizer {
 public:
  HDMapVisualizer();
  ~HDMapVisualizer();

  const apollo::hdmap::HDMap *hdmap() const {
    return hdmap_;
  }

  void SetVizCenterPointUseVizPose(const apollo::common::PointENU &point) {
    viz_center_point_ = VizPointToHDMapPoint(point);
  }

  void SetVizCenterPointUseHDMapPose(const apollo::common::PointENU &point) {
    viz_center_point_ = point;
  }

  void SetVizOffsetPointUseHDMapPose(const apollo::common::PointENU &point) {
    viz_offset_point_ = point;
  }

  apollo::common::PointENU GetVizOffset() const {
    return viz_offset_point_;
  }

  inline apollo::common::PointENU HDMapPointToVizPoint(const apollo::common::PointENU &hdmap_point) {
    apollo::common::PointENU viz_point;
    viz_point.set_x(hdmap_point.x() - viz_offset_point_.x());
    viz_point.set_y(hdmap_point.y() - viz_offset_point_.y());
    viz_point.set_z(0.0);
    return viz_point;
  }

  inline apollo::common::PointENU VizPointToHDMapPoint(const apollo::common::PointENU &viz_point) {
    apollo::common::PointENU hdmap_point;
    hdmap_point.set_x(viz_point.x() + viz_offset_point_.x());
    hdmap_point.set_y(viz_point.y() + viz_offset_point_.y());
    hdmap_point.set_z(0.0);
    return hdmap_point;
  }

 private:
  void TimerCallbackForVisualizing(const ros::TimerEvent &event);

  visualization_msgs::MarkerArray GenerateDeleteMarker();

 private:
  apollo::common::PointENU viz_offset_point_;
  apollo::common::PointENU viz_center_point_;

  double viz_rate_ = 5.0;
  double viz_distance_ = 500.0;

  const apollo::hdmap::HDMap *hdmap_ = nullptr;

  ros::Timer timer_hdmap_visualizer_;
  ros::Publisher pub_hdmap_;
};
