// Generated by gencpp from file vaeb/Relbl4Vcc.msg
// DO NOT EDIT!


#ifndef VAEB_MESSAGE_RELBL4VCC_H
#define VAEB_MESSAGE_RELBL4VCC_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace vaeb
{
template <class ContainerAllocator>
struct Relbl4Vcc_
{
  typedef Relbl4Vcc_<ContainerAllocator> Type;

  Relbl4Vcc_()
    {
    }
  Relbl4Vcc_(const ContainerAllocator& _alloc)
    {
  (void)_alloc;
    }





// reducing the odds to have name collisions with Windows.h 
#if defined(_WIN32) && defined(RELBL4Vcc_NotRelbl)
  #undef RELBL4Vcc_NotRelbl
#endif
#if defined(_WIN32) && defined(RELBL4Vcc_CoastRelbl)
  #undef RELBL4Vcc_CoastRelbl
#endif
#if defined(_WIN32) && defined(RELBL4Vcc_BrkSpprtRelbl)
  #undef RELBL4Vcc_BrkSpprtRelbl
#endif
#if defined(_WIN32) && defined(RELBL4Vcc_BrkgRelbl)
  #undef RELBL4Vcc_BrkgRelbl
#endif

  enum {
    RELBL4Vcc_NotRelbl = 0,
    RELBL4Vcc_CoastRelbl = 1,
    RELBL4Vcc_BrkSpprtRelbl = 2,
    RELBL4Vcc_BrkgRelbl = 3,
  };


  typedef boost::shared_ptr< ::vaeb::Relbl4Vcc_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::vaeb::Relbl4Vcc_<ContainerAllocator> const> ConstPtr;

}; // struct Relbl4Vcc_

typedef ::vaeb::Relbl4Vcc_<std::allocator<void> > Relbl4Vcc;

typedef boost::shared_ptr< ::vaeb::Relbl4Vcc > Relbl4VccPtr;
typedef boost::shared_ptr< ::vaeb::Relbl4Vcc const> Relbl4VccConstPtr;

// constants requiring out of line definition

   

   

   

   



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::vaeb::Relbl4Vcc_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::vaeb::Relbl4Vcc_<ContainerAllocator> >::stream(s, "", v);
return s;
}


} // namespace vaeb

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::vaeb::Relbl4Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::Relbl4Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::Relbl4Vcc_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "86e230a451bed2e9f4fbe05c46d914b1";
  }

  static const char* value(const ::vaeb::Relbl4Vcc_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x86e230a451bed2e9ULL;
  static const uint64_t static_value2 = 0xf4fbe05c46d914b1ULL;
};

template<class ContainerAllocator>
struct DataType< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "vaeb/Relbl4Vcc";
  }

  static const char* value(const ::vaeb::Relbl4Vcc_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int8 RELBL4Vcc_NotRelbl = 0\n"
"int8 RELBL4Vcc_CoastRelbl = 1\n"
"int8 RELBL4Vcc_BrkSpprtRelbl = 2\n"
"int8 RELBL4Vcc_BrkgRelbl = 3\n"
;
  }

  static const char* value(const ::vaeb::Relbl4Vcc_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream&, T)
    {}

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Relbl4Vcc_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::vaeb::Relbl4Vcc_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream&, const std::string&, const ::vaeb::Relbl4Vcc_<ContainerAllocator>&)
  {}
};

} // namespace message_operations
} // namespace ros

#endif // VAEB_MESSAGE_RELBL4VCC_H
