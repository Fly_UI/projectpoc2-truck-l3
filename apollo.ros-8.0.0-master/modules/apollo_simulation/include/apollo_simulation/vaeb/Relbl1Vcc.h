// Generated by gencpp from file vaeb/Relbl1Vcc.msg
// DO NOT EDIT!


#ifndef VAEB_MESSAGE_RELBL1VCC_H
#define VAEB_MESSAGE_RELBL1VCC_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace vaeb
{
template <class ContainerAllocator>
struct Relbl1Vcc_
{
  typedef Relbl1Vcc_<ContainerAllocator> Type;

  Relbl1Vcc_()
    {
    }
  Relbl1Vcc_(const ContainerAllocator& _alloc)
    {
  (void)_alloc;
    }





// reducing the odds to have name collisions with Windows.h 
#if defined(_WIN32) && defined(Relbl1Vcc_NotRelbl)
  #undef Relbl1Vcc_NotRelbl
#endif
#if defined(_WIN32) && defined(Relbl1Vcc_Relbl)
  #undef Relbl1Vcc_Relbl
#endif

  enum {
    Relbl1Vcc_NotRelbl = 0,
    Relbl1Vcc_Relbl = 1,
  };


  typedef boost::shared_ptr< ::vaeb::Relbl1Vcc_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::vaeb::Relbl1Vcc_<ContainerAllocator> const> ConstPtr;

}; // struct Relbl1Vcc_

typedef ::vaeb::Relbl1Vcc_<std::allocator<void> > Relbl1Vcc;

typedef boost::shared_ptr< ::vaeb::Relbl1Vcc > Relbl1VccPtr;
typedef boost::shared_ptr< ::vaeb::Relbl1Vcc const> Relbl1VccConstPtr;

// constants requiring out of line definition

   

   



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::vaeb::Relbl1Vcc_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::vaeb::Relbl1Vcc_<ContainerAllocator> >::stream(s, "", v);
return s;
}


} // namespace vaeb

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::vaeb::Relbl1Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::Relbl1Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::Relbl1Vcc_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "98263ef2dfcb027345aa8364389378be";
  }

  static const char* value(const ::vaeb::Relbl1Vcc_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x98263ef2dfcb0273ULL;
  static const uint64_t static_value2 = 0x45aa8364389378beULL;
};

template<class ContainerAllocator>
struct DataType< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "vaeb/Relbl1Vcc";
  }

  static const char* value(const ::vaeb::Relbl1Vcc_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int8 Relbl1Vcc_NotRelbl = 0\n"
"int8 Relbl1Vcc_Relbl = 1\n"
;
  }

  static const char* value(const ::vaeb::Relbl1Vcc_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream&, T)
    {}

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Relbl1Vcc_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::vaeb::Relbl1Vcc_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream&, const std::string&, const ::vaeb::Relbl1Vcc_<ContainerAllocator>&)
  {}
};

} // namespace message_operations
} // namespace ros

#endif // VAEB_MESSAGE_RELBL1VCC_H
