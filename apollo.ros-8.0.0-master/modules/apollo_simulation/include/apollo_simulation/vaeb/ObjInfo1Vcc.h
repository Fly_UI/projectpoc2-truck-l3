// Generated by gencpp from file vaeb/ObjInfo1Vcc.msg
// DO NOT EDIT!


#ifndef VAEB_MESSAGE_OBJINFO1VCC_H
#define VAEB_MESSAGE_OBJINFO1VCC_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace vaeb
{
template <class ContainerAllocator>
struct ObjInfo1Vcc_
{
  typedef ObjInfo1Vcc_<ContainerAllocator> Type;

  ObjInfo1Vcc_()
    : Typ(0)
    , Width(0.0)
    , Hei(0.0)
    , SideNear(0)
    , MtnPat(0)
    , MtnPatHist(0)
    , DstLatFromMidOfLaneSelf(0.0)  {
    }
  ObjInfo1Vcc_(const ContainerAllocator& _alloc)
    : Typ(0)
    , Width(0.0)
    , Hei(0.0)
    , SideNear(0)
    , MtnPat(0)
    , MtnPatHist(0)
    , DstLatFromMidOfLaneSelf(0.0)  {
  (void)_alloc;
    }



   typedef int8_t _Typ_type;
  _Typ_type Typ;

   typedef float _Width_type;
  _Width_type Width;

   typedef float _Hei_type;
  _Hei_type Hei;

   typedef int8_t _SideNear_type;
  _SideNear_type SideNear;

   typedef int8_t _MtnPat_type;
  _MtnPat_type MtnPat;

   typedef int8_t _MtnPatHist_type;
  _MtnPatHist_type MtnPatHist;

   typedef float _DstLatFromMidOfLaneSelf_type;
  _DstLatFromMidOfLaneSelf_type DstLatFromMidOfLaneSelf;





  typedef boost::shared_ptr< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> const> ConstPtr;

}; // struct ObjInfo1Vcc_

typedef ::vaeb::ObjInfo1Vcc_<std::allocator<void> > ObjInfo1Vcc;

typedef boost::shared_ptr< ::vaeb::ObjInfo1Vcc > ObjInfo1VccPtr;
typedef boost::shared_ptr< ::vaeb::ObjInfo1Vcc const> ObjInfo1VccConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::vaeb::ObjInfo1Vcc_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::vaeb::ObjInfo1Vcc_<ContainerAllocator1> & lhs, const ::vaeb::ObjInfo1Vcc_<ContainerAllocator2> & rhs)
{
  return lhs.Typ == rhs.Typ &&
    lhs.Width == rhs.Width &&
    lhs.Hei == rhs.Hei &&
    lhs.SideNear == rhs.SideNear &&
    lhs.MtnPat == rhs.MtnPat &&
    lhs.MtnPatHist == rhs.MtnPatHist &&
    lhs.DstLatFromMidOfLaneSelf == rhs.DstLatFromMidOfLaneSelf;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::vaeb::ObjInfo1Vcc_<ContainerAllocator1> & lhs, const ::vaeb::ObjInfo1Vcc_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace vaeb

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "0e199ebb1bcc43d1c3cb19255e67c883";
  }

  static const char* value(const ::vaeb::ObjInfo1Vcc_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x0e199ebb1bcc43d1ULL;
  static const uint64_t static_value2 = 0xc3cb19255e67c883ULL;
};

template<class ContainerAllocator>
struct DataType< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "vaeb/ObjInfo1Vcc";
  }

  static const char* value(const ::vaeb::ObjInfo1Vcc_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int8 Typ\n"
"float32 Width\n"
"float32 Hei\n"
"int8 SideNear\n"
"int8 MtnPat\n"
"int8 MtnPatHist\n"
"float32 DstLatFromMidOfLaneSelf\n"
;
  }

  static const char* value(const ::vaeb::ObjInfo1Vcc_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.Typ);
      stream.next(m.Width);
      stream.next(m.Hei);
      stream.next(m.SideNear);
      stream.next(m.MtnPat);
      stream.next(m.MtnPatHist);
      stream.next(m.DstLatFromMidOfLaneSelf);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ObjInfo1Vcc_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::vaeb::ObjInfo1Vcc_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::vaeb::ObjInfo1Vcc_<ContainerAllocator>& v)
  {
    s << indent << "Typ: ";
    Printer<int8_t>::stream(s, indent + "  ", v.Typ);
    s << indent << "Width: ";
    Printer<float>::stream(s, indent + "  ", v.Width);
    s << indent << "Hei: ";
    Printer<float>::stream(s, indent + "  ", v.Hei);
    s << indent << "SideNear: ";
    Printer<int8_t>::stream(s, indent + "  ", v.SideNear);
    s << indent << "MtnPat: ";
    Printer<int8_t>::stream(s, indent + "  ", v.MtnPat);
    s << indent << "MtnPatHist: ";
    Printer<int8_t>::stream(s, indent + "  ", v.MtnPatHist);
    s << indent << "DstLatFromMidOfLaneSelf: ";
    Printer<float>::stream(s, indent + "  ", v.DstLatFromMidOfLaneSelf);
  }
};

} // namespace message_operations
} // namespace ros

#endif // VAEB_MESSAGE_OBJINFO1VCC_H
