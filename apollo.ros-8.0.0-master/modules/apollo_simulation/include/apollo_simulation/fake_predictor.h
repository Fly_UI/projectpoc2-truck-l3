/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <string>
#include <mutex>

#include <ros/ros.h>
#include <tf_conversions/tf_eigen.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/utils.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <dynamic_reconfigure/server.h>
#include <apollo_simulation/FakePredictorConfig.h>

#include "apollo_msgs/prediction_msgs/prediction_obstacle.pb.h"
#include "apollo_simulation/hdmap_visualizer.h"
//add by travis begin
#include "vaeb_node_msgs/vaeb.h"
#include <cmath>
//add by travis end

struct UTMCoordinate{
    double x;
    double y;
  };
  struct RelativeCoordinate{
    double x;
    double y;
  };
  
class FakePredictor {
 public:
  FakePredictor(HDMapVisualizer &hdmap_visualizer);
  ~FakePredictor();

public:
  double selfCarPosnLgt = 0.0;
  double selfCarPosnLat = 0.0;
  

 private:
  void ReconfigureCB(simulation::FakePredictorConfig &config, uint32_t level);

  void PublishFakePrediction(const ros::TimerEvent &event);

  void UpdatePrediction();

  void PubPredictionObstacle();

  void CallbackFakePrediction(const geometry_msgs::PoseStamped::ConstPtr &msg);
  //===========================================travis====================================================begin
  void CallbackObstacle(const vaeb_node_msgs::vaeb::ConstPtr &msgs);
  void GpsDataCallback(const geometry_msgs::Pose::ConstPtr& msg);
  //===========================================travis====================================================end

  bool GetNearestLane(const double x, const double y,
    apollo::hdmap::LaneInfoConstPtr *nearest_lane,
    double *nearest_s, double *nearest_l) const;

  void FillPerceptionPolygon(
    apollo::perception::PerceptionObstacle *const perception_obstacle,
    const double mid_x, const double mid_y, const double mid_z,
    const double length, const double width, const double height, const double heading);

  inline geometry_msgs::Quaternion GetQuaternionFromRPY(
    const double &roll, const double &pitch, const double &yaw) {
    tf2::Quaternion q;
    q.setRPY(roll, pitch, yaw);
    return tf2::toMsg(q);
  }

 private:
  std::string map_frame_id_ = "map";
  std::string car_frame_id_ = "base_link";
  std::string running_mode_; 

  HDMapVisualizer &hdmap_visualizer_;

  apollo::prediction::PredictionObstacles prediction_;
  std::mutex prediction_mutex_;
  int prediction_count_ = 0;

  bool obstacle_clear_ = false;
  double obstacle_speed_ = 0;
  int obstacle_max_size_ = 0;

  dynamic_reconfigure::Server<simulation::FakePredictorConfig> *dsrv_ = nullptr;
  simulation::FakePredictorConfig default_config_;
  bool setup_ = false;
  int startIndex = 0;
  int index = -1;

  ros::Timer sim_prediction_timer_;
  static constexpr double kSimPredictionIntervalMs = 0.1; 

  ros::Publisher  prediction_writer_;

  ros::Subscriber sub_fake_prediction_;
  ros::Subscriber sub_self_carLocation_;
  ros::Publisher  pub_prediction_obstacle_;

  UTMCoordinate rotateCoordinate(const UTMCoordinate& coordinate, double heading,double obstacleRelative_X,double obstacleRelative_Y){
    UTMCoordinate rotatedCoordinate;
    double angle = M_PI / 2 - heading / 180 * M_PI;; // 将角度转换为弧度
    rotatedCoordinate.x = coordinate.x  + obstacleRelative_X * cos(angle) - obstacleRelative_Y * sin(angle);
    rotatedCoordinate.y = coordinate.y  + obstacleRelative_Y * cos(angle) + obstacleRelative_X * sin(angle);
    return rotatedCoordinate;
  };
  UTMCoordinate calculateObstacleUTMCoordinate(const UTMCoordinate& vehicleUTM, double heading, const RelativeCoordinate& cameraRelative, const RelativeCoordinate& obstacleRelative){
     // 将车辆的UTM坐标进行旋转
    UTMCoordinate rotatedVehicleUTM = rotateCoordinate(vehicleUTM, heading,obstacleRelative.x,obstacleRelative.y);

    // // 计算摄像头的UTM坐,
    // UTMCoordinate cameraUTM;
    // cameraUTM.x = cameraRelative.x + rotatedVehicleUTM.x;
    // cameraUTM.y = cameraRelative.y + rotatedVehicleUTM.y;

    // 计算障碍物的UTM坐标
    UTMCoordinate obstacleUTM;
    obstacleUTM.x =  rotatedVehicleUTM.x+cameraRelative.x;
    obstacleUTM.y =  rotatedVehicleUTM.y+cameraRelative.y;

    return obstacleUTM;
  }


};