/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <string>

#include <ros/ros.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/utils.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include "apollo_msgs/chassis_msgs/chassis.pb.h"
#include "apollo_msgs/localization_msgs/localization.pb.h"
#include "apollo_msgs/routing_msgs/routing.pb.h"
#include "apollo_msgs/planning_msgs/planning.pb.h"

#include "apollo_simulation/hdmap_visualizer.h"
#include "apollo_simulation/fake_predictor.h"

namespace apollo {

/**
 * @class SimControl
 * @brief A module that simulates a 'perfect control' algorithm, which assumes
 * an ideal world where the car can be perfectly placed wherever the planning
 * asks it to be, with the expected speed, acceleration, etc.
 */
class SimControl {
 public:
  /**
   * @brief Constructor of SimControl.
   * @param map_service the pointer of MapService.
   */
  explicit SimControl();

  bool IsEnabled() const {
    return enabled_;
  }

  /**
   * @brief setup callbacks and timer
   * @param set_start_point initialize localization.
   */
  void Init(bool set_start_point, double start_velocity = 0.0,
    double start_acceleration = 0.0);

  /**
   * @brief Starts the timer to publish simulated localization and chassis
   * messages.
   */
  void Start();

  /**
   * @brief Stops the timer.
   */
  void Stop();

  /**
   * @brief Resets the internal state.
   */
  void Reset();

  void RunOnce();

 private:
  void OnPlanning(const std_msgs::String::ConstPtr &trajectory);
  void OnRoutingResponse(const std_msgs::String::ConstPtr &routing);

  /**
   * @brief Predict the next trajectory point using perfect control model
   */
  bool PerfectControlModel(apollo::common::TrajectoryPoint *point,
                           apollo::canbus::Chassis::GearPosition *gear_position);

  void PublishChassis(double cur_speed,
                      apollo::canbus::Chassis::GearPosition gear_position);

  void PublishLocalization(const apollo::common::TrajectoryPoint &point);

  // Reset the start point, which can be a dummy point on the map or received
  // from the routing module.
  void SetStartPoint(const apollo::common::TrajectoryPoint &point);

  void Freeze();

  void TimerCallback(const ros::TimerEvent &event);

  void ClearPlanning();

  void CallbackNavGoalPose(const geometry_msgs::PoseStamped::ConstPtr &msg);
  void CallbackInitialPose(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg);

  void PublishTF(const apollo::common::TrajectoryPoint &point);
  void PublishRosLocalization(const apollo::common::TrajectoryPoint &point);
  void PublishRosRoutingPath(const apollo::routing::RoutingResponse &routing);
  void PublishRosReferenceLine(const apollo::common::Path &path);
  void PublishRosPlanningTrajectory(const apollo::planning::ADCTrajectory &trajectory);

  bool ConstructPakingInfo(const double x, const double y,
    apollo::routing::ParkingInfo *parking_info) const;

  bool ConstructLaneWayPoint(const double x, const double y,
    apollo::routing::LaneWaypoint *laneWayPoint) const;

  bool GetNearestLane(const double x, const double y,
    apollo::hdmap::LaneInfoConstPtr *nearest_lane,
    double *nearest_s, double *nearest_l) const;

  inline geometry_msgs::Quaternion GetQuaternionFromRPY(
    const double &roll, const double &pitch, const double &yaw) {
    tf2::Quaternion q;
    q.setRPY(roll, pitch, yaw);
    return tf2::toMsg(q);
  }

 private:
  std::string map_frame_id_ = "map";
  std::string car_frame_id_ = "base_link";
  HDMapVisualizer hdmap_visualizer_;
  std::shared_ptr<FakePredictor> fake_predictor_;
  std::mutex mutex_;

  // The timer to publish simulated localization and chassis messages.
  ros::Timer sim_control_timer_;

  // Time interval of the timer, in seconds.
  static constexpr double kSimControlInterval = 0.01;

  // The latest received planning trajectory.
  apollo::planning::ADCTrajectory current_trajectory_;
  // The index of the previous and next point with regard to the
  // current_trajectory.
  int prev_point_index_ = 0;
  int next_point_index_ = 0;

  // Whether there's a planning received after the most recent routing.
  bool received_planning_ = false;

  // Number of planning received in terms of one RoutingResponse.
  int planning_count_ = -1;

  bool re_routing_triggered_ = false;

  // Whether the sim control is enabled / initialized.
  bool enabled_ = false;
  bool inited_ = false;

  // The header of the routing planning is following.
  apollo::common::Header current_routing_header_;

  apollo::common::TrajectoryPoint prev_point_;
  apollo::common::TrajectoryPoint next_point_;

  common::PathPoint adc_position_;

  // Initial velocity and acceleration of the main vehicle
  double start_velocity_ = 0.0;
  double start_acceleration_ = 0.0;

  static constexpr int kPlanningCountToStart = 5;

  tf2_ros::TransformBroadcaster tf_broadcaster_;
  ros::Subscriber sub_nav_goal_;
  ros::Subscriber sub_initial_pose_;
  ros::Publisher  pub_sim_pose_;
  ros::Publisher  pub_routing_path_;
  ros::Publisher  pub_reference_line_;
  ros::Publisher  pub_planning_trajectory_;

  ros::Subscriber routing_reader_;
  ros::Subscriber planning_reader_;
  ros::Publisher  chassis_writer_;
  ros::Publisher  localization_writer_;
  ros::Publisher  routing_writer_;
};

}  // namespace apollo
