/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <iostream>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/LU>

#include "apollo_simulation/vehicle_sim_model/sim_model_interface.h"

/**
 * @class simple_planning_simulator ideal twist model
 * @brief calculate ideal twist dynamics
 */
class SimModelIdealTwist : public SimModelInterface {
 public:
  /**
   * @brief constructor
   */
  SimModelIdealTwist();

  /**
   * @brief destructor
   */
  ~SimModelIdealTwist() = default;

 private:
  enum IDX {
    X = 0,
    Y,
    YAW,
  };
  enum IDX_U {
    VX_DES = 0,
    WZ_DES,
  };

  /**
   * @brief get vehicle position x
   */
  double GetX() override;

  /**
   * @brief get vehicle position y
   */
  double GetY() override;

  /**
   * @brief get vehicle angle yaw
   */
  double GetYaw() override;

  /**
   * @brief get vehicle velocity vx
   */
  double GetVx() override;

  /**
   * @brief get vehicle angular-velocity wz
   */
  double GetWz() override;

  /**
   * @brief get vehicle linear-accration accx
   */
  double GetACCx() override;

  /**
   * @brief get vehicle steering angle
   */
  double GetSteer() override;

  /**
   * @brief update vehicle states
   * @param [in] dt delta time [s]
   */
  void Update(const double &dt) override;

  /**
   * @brief calculate derivative of states with ideal twist model
   * @param [in] state current model state
   * @param [in] input input vector to model
   */
  Eigen::VectorXd CalcModel(const Eigen::VectorXd &state, const Eigen::VectorXd &input) override;
};

/**
 * @class simple_planning_simulator ideal steering model
 * @brief calculate ideal steering dynamics
 */
class SimModelIdealSteer : public SimModelInterface {
 public:
  /**
   * @brief constructor
   * @param [in] wheelbase vehicle wheelbase length [m]
   */
  explicit SimModelIdealSteer(double wheelbase);

  /**
   * @brief destructor
   */
  ~SimModelIdealSteer() = default;

 private:
  enum IDX {
    X = 0,
    Y,
    YAW,
  };
  enum IDX_U {
    VX_DES = 0,
    STEER_DES,
  };

  const double wheelbase_;  //!< @brief vehicle wheelbase length

  /**
   * @brief get vehicle position x
   */
  double GetX() override;

  /**
   * @brief get vehicle position y
   */
  double GetY() override;

  /**
   * @brief get vehicle angle yaw
   */
  double GetYaw() override;

  /**
   * @brief get vehicle velocity vx
   */
  double GetVx() override;

  /**
   * @brief get vehicle angular-velocity wz
   */
  double GetWz() override;

  /**
   * @brief get vehicle linear-accration accx
   */
  double GetACCx() override;
  
  /**
   * @brief get vehicle steering angle
   */
  double GetSteer() override;

  /**
   * @brief update vehicle states
   * @param [in] dt delta time [s]
   */
  void Update(const double &dt) override;

  /**
   * @brief calculate derivative of states with ideal steering model
   * @param [in] state current model state
   * @param [in] input input vector to model
   */
  Eigen::VectorXd CalcModel(const Eigen::VectorXd &state, const Eigen::VectorXd &input) override;
};

/**
 * @class wf_simulator ideal acceleration and steering model
 * @brief calculate ideal steering dynamics
 */
class SimModelIdealAccel : public SimModelInterface {
 public:
  /**
   * @brief constructor
   * @param [in] wheelbase vehicle wheelbase length [m]
   */
  explicit SimModelIdealAccel(double wheelbase);

  /**
   * @brief destructor
   */
  ~SimModelIdealAccel() = default;

 private:
  enum IDX {
    X = 0,
    Y,
    YAW,
    VX,
  };
  enum IDX_U {
    AX_DES = 0,
    STEER_DES,
  };

  const double wheelbase_;  //!< @brief vehicle wheelbase length

  /**
   * @brief get vehicle position x
   */
  double GetX() override;

  /**
   * @brief get vehicle position y
   */
  double GetY() override;

  /**
   * @brief get vehicle angle yaw
   */
  double GetYaw() override;

  /**
   * @brief get vehicle velocity vx
   */
  double GetVx() override;

  /**
   * @brief get vehicle angular-velocity wz
   */
  double GetWz() override;

  /**
   * @brief get vehicle linear-accration accx
   */
  double GetACCx() override;

  /**
   * @brief get vehicle steering angle
   */
  double GetSteer() override;

  /**
   * @brief update vehicle states
   * @param [in] dt delta time [s]
   */
  void Update(const double &dt) override;

  /**
   * @brief calculate derivative of states with ideal steering model
   * @param [in] state current model state
   * @param [in] input input vector to model
   */
  Eigen::VectorXd CalcModel(const Eigen::VectorXd &state, const Eigen::VectorXd &input) override;
};
