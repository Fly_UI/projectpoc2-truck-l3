/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_common/time/clock.h"

#include "apollo_common/log.h"

namespace apollo {
namespace common {
namespace time {

using AtomicWriteLockGuard = WriteLockGuard<AtomicRWLock>;
using AtomicReadLockGuard = ReadLockGuard<AtomicRWLock>;

Clock::Clock() {
  mock_now_ = Time(0);
}

Time Clock::Now() {
  auto clock = Instance();

  AtomicReadLockGuard lg(clock->rwlock_);
  return Time::Now();
}

double Clock::NowInSeconds() { return Now().ToSecond(); }

void Clock::SetNow(const Time& now) {
  auto clock = Instance();
  AtomicWriteLockGuard lg(clock->rwlock_);
  clock->mock_now_ = now;
}

}  // namespace time
}  // namespace common
}  // namespace apollo
