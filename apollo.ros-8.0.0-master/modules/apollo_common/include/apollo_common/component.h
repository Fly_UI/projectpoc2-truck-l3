/******************************************************************************
 * Copyright 2022 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/**
 * @file
 */

#pragma once

#include <csignal>
#include <string>

#include <gflags/gflags.h>

#include <ros/ros.h>
#include <std_msgs/String.h>

#include "apollo_common/log.h"
#include "apollo_common/status/status.h"

/**
 * @namespace apollo::common
 * @brief apollo::common
 */
namespace apollo {
namespace common {

/**
 * @class Component
 *
 * @brief The base module class to define the interface of an Apollo component.
 * An Apollo component runs infinitely until being shutdown by SIGINT or ROS. Many
 * essential components in Apollo, such as localization and control are examples
 * of Apollo apps. The COMPONENT macro helps developer to setup glog, gflag
 * and ROS in one line.
 */
class Component {
 public:
  /**
   * @brief module name. It is used to uniquely identify the component.
   */
  virtual std::string Name() const = 0;

  /**
   * @brief this is the entry point of an Apollo App. It initializes the component,
   * starts the component, and stop the component when the ros has shutdown.
   */
  virtual int Spin();

  /**
   * The default destructor.
   */
  virtual ~Component() = default;

  /**
   * @brief set the number of threads to handle ros message callbacks.
   * The default thread number is 1
   */
  void SetCallbackThreadNumber(uint32_t callback_thread_num);

 protected:
  /**
   * @brief The module initialization function. This is the first function being
   * called when the App starts. Usually this function loads the configurations,
   * subscribe the data from sensors or other modules.
   * @return Status initialization status
   */
  virtual bool Init() = 0;

  /** The callback thread number
   */
  uint32_t callback_thread_num_ = 1;
};

void component_sigint_handler(int signal_num);

}  // namespace common
}  // namespace apollo

#define APOLLO_COMPONENT(COMPONENT)                                   \
  int main(int argc, char **argv) {                                   \
    google::InitGoogleLogging(argv[0]);                               \
    google::ParseCommandLineFlags(&argc, &argv, true);                \
    signal(SIGINT, apollo::common::component_sigint_handler);         \
    COMPONENT component;                                              \
    ros::init(argc, argv, component.Name());                          \
    component.Spin();                                                 \
    return 0;                                                         \
  }
