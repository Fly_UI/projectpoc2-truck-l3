cmake_minimum_required(VERSION 3.0.2)
project(apollo_routing)

add_compile_options(-std=c++17)
# set(CMAKE_BUILD_TYPE "Debug")
set(CMAKE_BUILD_TYPE "Release")

find_package(Eigen3 REQUIRED)
find_package(absl REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  apollo_msgs
  apollo_common
  apollo_map
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES apollo_routing
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
  /usr/include
  /usr/local/include
)

add_executable(topo_creator 
  # common
  src/common/routing_gflags.cc
  # topo_creator
  src/topo_creator/edge_creator.cc
  src/topo_creator/graph_creator.cc
  src/topo_creator/node_creator.cc
  src/topo_creator/topo_creator.cc
)
add_dependencies(topo_creator
  ${${PROJECT_NAME}_EXPORTED_TARGETS} 
  ${catkin_EXPORTED_TARGETS}
)
target_link_libraries(topo_creator
  ${catkin_LIBRARIES}
  protobuf
  glog
  gflags
  absl::strings 
  absl::strings_internal
)

add_executable(${PROJECT_NAME} 
  src/routing_component.cc
  src/routing.cc
  # common
  src/common/routing_gflags.cc
  # core
  src/core/black_list_range_generator.cc
  src/core/navigator.cc
  src/core/result_generator.cc
  # graph
  src/graph/node_with_range.cc
  src/graph/sub_topo_graph.cc
  src/graph/topo_graph.cc
  src/graph/topo_node.cc
  src/graph/topo_range_manager.cc
  src/graph/topo_range.cc
  # strategy
  src/strategy/a_star_strategy.cc
)
add_dependencies(${PROJECT_NAME} 
  ${${PROJECT_NAME}_EXPORTED_TARGETS} 
  ${catkin_EXPORTED_TARGETS}
)
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
  protobuf
  glog
  gflags
  absl::strings 
  absl::strings_internal
)

# install
install(TARGETS topo_creator ${PROJECT_NAME} 
	ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
	LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)