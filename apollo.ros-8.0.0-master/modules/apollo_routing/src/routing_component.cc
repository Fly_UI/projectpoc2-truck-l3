/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/******************************************************************************
 * Copyright 2023 Forrest. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "apollo_routing/routing_component.h"

#include <utility>

#include "apollo_common/log.h"
#include "apollo_common/adapters/adapter_gflags.h"
#include "apollo_routing/common/routing_gflags.h"

DECLARE_string(flagfile);

namespace apollo {
namespace routing {

std::string RoutingComponent::Name() const { return FLAGS_routing_node_name; }

bool RoutingComponent::Init() {
  ros::NodeHandle nh;
  request_reader_ = nh.subscribe(FLAGS_routing_request_topic, 5, 
      &RoutingComponent::OnRoutingRequest, this);
  response_writer_ = nh.advertise<std_msgs::String>(FLAGS_routing_response_topic, 5, true);

  return routing_.Init().ok() && routing_.Start().ok();
}

void RoutingComponent::OnRoutingRequest(const std_msgs::String::ConstPtr& request_msg) {
  auto request = std::make_shared<RoutingRequest>();
  if (!request->ParseFromString(request_msg->data)) {
    AERROR << "Failed to parse proto-message.";
    return;
  }

  auto response = std::make_shared<RoutingResponse>();
  if (!routing_.Process(request, response.get())) {
    return;
  }
  common::util::FillHeader(Name(), response.get());
  std_msgs::String response_msg;
  response_msg.data = response->SerializeAsString();
  response_writer_.publish(response_msg);  
  {
    std::lock_guard<std::mutex> guard(mutex_);
    response_ = std::move(response);
  }
  return;
}

}  // namespace routing
}  // namespace apollo

APOLLO_COMPONENT(apollo::routing::RoutingComponent);