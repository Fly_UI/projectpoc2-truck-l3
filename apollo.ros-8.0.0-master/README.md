# apollo.ros-8.0.0

## 简介

 此项目为基于`ros1`的[apollo8.0.0](https://github.com/ApolloAuto/apollo/tree/v8.0.0)规划控制算法，移植主要目的如下：

- 学习`apollo`框架设计
- 学习`apollo`中的运动控制算法
- 学习`apollo`中的决策规划算法

目前只针对`apollo`中部分模块进行了移植，移植模块有:

```shell
.
├── docker
│   ├── build
│   └── scripts
├── images
├── modules
│   ├── apollo_common
│   ├── apollo_control
│   ├── apollo_map
│   ├── apollo_msgs
│   ├── apollo_planning
│   ├── apollo_routing
│   └── apollo_simulation
└── README.md
```

目前移植版本能实现车辆在高精度地图下进行`routing`、`planning`和`control`仿真测试，与原有版本改动点如下：

- 使用原生`ros`（基于`noetic`）替代`apollo`中的`CyberRT`
- 使用`ros pkg`封装`apollo`中的`module`
- 使用`cmake`进行编译
- 将`protobuff`版本提升到`3.6.1`
- 增加`sim_control`和`sim_vehicle`仿真工具包`apollo_simulation`
- 增加高精度地图`rviz`显示
- 增加动/静态障碍物`rviz`设置
- 更多的高精度地图测试数据

>此移植版本，针对apollo原有方案初学者难入门，测试操作不便等问题，能很好进行相关算法学习和测试，并可以将自己的算法增加到框架中，应用于机器人或者无人驾驶中。

## 代码部署

### docker安装

- 在线安装

  ```shell
  curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
  ```

- 安装验证：安装完成执行 `sudo docker version`看看docker目前安装的版本等信息：

  ![image-20220909092510047](images/image-20220909092510047.png)

- 使用配置：避免每次新开一个终端都要`sudo`输入权限，给docker增加用户的权限。执行完成后**重启电脑之后生效**。

  ```shell
  sudo usermod -aG docker $USER
  ```

### 镜像构建

- 执行一下指令进行镜像构建

  ```shell
  cd apollo.ros-8.0.0/docker/build/ # 进入镜像构建目录
  ./build_docker.sh  								# 开始构建镜像
  ```

- 镜像构建成功后，显示如下图所示：

![image-20220909091842275](images/image-20220909091842275.png)

### 代码编译

- 解压项目代码到对应目录，假设代码主目录名为`apollo.ros-8.0.0`，其目录结构如下：

  ```shell
  .
  ├── docker
  │   ├── build
  │   └── scripts
  ├── images
  ├── LICENSE
  ├── modules
  │   ├── apollo_common
  │   ├── apollo_control
  │   ├── apollo_map
  │   ├── apollo_msgs
  │   ├── apollo_planning
  │   ├── apollo_routing
  │   └── apollo_simulation
  └── README.md
  ```

- 启动容器：启动成功后，显示如下图所示：

  ```shell
  cd apollo.ros-8.0.0/							# 返回工程主目录
  ./docker/scripts/docker_start.sh	# 启动容器
  ```

  ![image-20220909091749700](images/image-20220909091749700.png)

  > **注意**：
  >
  > - 执行`./docker/scripts/docker_start.sh`指令启动容器前，一定要**返回工程主目录**
  > - 如果此终端关闭后会导致启动容器退出
  > - 重新启动容器需要重新编译代码

- 编译代码：编译成功后，显示如下图所示：

  ```shell
  cd apollo_ros_ws/	# 进入工作空间
  catkin build			# 开始编译
  ```

  ![image-20220909092203520](images/image-20220909092203520.png)

- 进入容器：如果容器已经启动，想新开一个终端进去，可使用以下指令：

  ```shell
  cd apollo.ros-8.0.0/						# 返回工程主目录 
  ./docker/scripts/docker_into.sh	# 进入容器
  ```

## 仿真测试

### 仿真模式

目前仿真测试支持两种模式：

- `sim_control`：`routing`和`planning`模块仿真测试，移植于原`apollo`中的`dreamview`
- `sim_vehicle`：`routing`、`planning`和`control`模块仿真测试，移植于`autoware.universe`中的`simple_planning_simulator`

仿真模式设置在`modules/apollo_simulation/launch/simulation.launch`文件中：

- 将`use_sim_ctrl`设置为`true`时，为`sim_control`仿真模式
- 将`use_sim_ctrl`设置为`false`时，为`sim_vehicle`仿真模式

```xml
<?xml version="1.0" encoding="UTF-8"?>
<launch>
  <arg name="map_name" default="demo"/>
  <arg name="use_sim_ctrl" default="true"/>

  <include file="$(find apollo_routing)/launch/routing.launch">
    <arg name="map_name" value="$(arg map_name)"/>
  </include>

  <include file="$(find apollo_planning)/launch/planning.launch">
    <arg name="map_name" value="$(arg map_name)"/>
  </include>

  <group unless="$(arg use_sim_ctrl)">
    <include file="$(find apollo_control)/launch/control.launch"/>
  </group>

  <include file="$(find apollo_simulation)/launch/simulator.launch">
    <arg name="map_name" value="$(arg map_name)"/>
    <arg name="use_sim_ctrl" value="$(arg use_sim_ctrl)"/>
  </include>
</launch>
```

### 仿真测试

- 启动仿真程序：启动成功后可以看到`rviz`显示如下图所示

  ```shell
  cd apollo_ros_ws/
  source devel/setup.bash	
  roslaunch apollo_simulation simulation.launch
  ```

  ![image-20220909114849063](images/image-20220909114849063.png)

- 初始化车辆位姿：在`rviz`软件显示窗口上，点击`2D Pose Estimate`按钮在高精度地图上相应位置进行车辆位姿初始化

  ![image-20220909114951065](images/image-20220909114951065.png)

- 下发导航目标点：在`rviz`软件显示窗口上，点击`2D Nav Goal`按钮在高精度地图上相应位置进行导航目标点下发

  ![image-20220909114928110](images/image-20220909114928110.png)

- 增加障碍物：

  - 障碍物的增加可通过在`rviz`软件显示窗口上，点击`Apollo Fake Prediction`按钮在高精度地图上相应位置进行**动/静**态障碍物设置

    ![image-20220909115008680](images/image-20220909115008680.png)

  - 障碍物速度清除、数量和速度设置，可使用`ros`中的`rqt_reconfigure`工具进行更改

    ​	![image-20230102145506520](images/image-20230102145506520.png)

    - 在显示界面中勾选`obstacle_clear`按钮即可进行设置障碍物清除
    - 障碍物速度设置只需在在显示界面`obstacle_speed`输入栏中输入对应速度即可
    - 障碍物数量设置只需在在显示界面`obstacle_max_size`输入栏中输入对应数量即可

  - 设置静态障碍物：在`rviz`软件显示窗口上，点击`Apollo Fake Prediction`按钮在高精度地图上相应位置进行静态障碍物设置

    > 注意：此时障碍物速度必须设置为0

    ![image-20230102150215435](images/image-20230102150215435.png)

  - 设置动态障碍物：在`rviz`软件显示窗口上，点击`Apollo Fake Prediction`按钮在高精度地图上相应位置进行动态障碍物设置

    > 注意：
    >
    > - 此时障碍物速度必须设置＞0
    > - 障碍物前方蓝色轨迹为障碍物预测轨迹

    ![image-20230102150448278](images/image-20230102150448278.png)

- 成功运行效果：如果程序启动正常，车辆位姿和导航目标都设置正确的话，可在`rviz`上看到正常运行的效果，如下图所示

  ![simulation](images/simulation.gif)

- 更换测试地图：仿真测试时，默认启动的地图是`modules/apollo_map/data/demo`，如果想更换成`modules/apollo_map/data/`目录下其他地图，可在启动仿真程序时，根据想使用的地图的名称，将启动指令更换成以下指令即可：

  ```shell
  roslaunch apollo_simulation simulation.launch map_name:=cube_town
  ```

## 高精度地图

仿真测试时，所使用的高精度地图放在`modules/apollo_map/data/`目录下，项目中已有的地图有：

```shell
.
├── autonomou_stuff
├── borregas_ave
├── cube_town
├── demo
├── go_mentum_dtl
├── modern_city
├── san_francisco
├── san_mateo
├── sunnyvale_big_loop
├── sunnyvale_loop
└── sunnyvale_with_two_offices
```

如果想添加新的高精度地图，则需要按照以下步骤进行：

- 在`modules/apollo_map/data/`目录下新建以添加地图名字命名的文件夹，这里以`test`为例：

  ```shell
  cd apollo.ros-8.0.0/modules/apollo_map/data/
  mkdir test
  ```

- 将高精度地图`test`的地图文件`base_map.bin`放到刚刚创建的目录`apollo.ros-8.0.0/modules/apollo_map/data/test`中

  - 高精度地图文件`base_map.bin`是由高精度地图绘制工具绘制得来，常用的工具有`Roadrunner`，不过是收费的
  - 本项目的高精度地图均是在`LGSVL`仿真平台下载

  > **注意**：`apollo.ros-8.0.0`只支持`apollo5.0`百度`OpenDRIVE`格式规范

- 生成`routing`所需的拓扑地图：以下指令中的`test`为第一步在`modules/apollo_map/data/`目录下新建的文件夹名称

  ```shell
  cd apollo_ros_ws/
  source devel/setup.bash	
  roslaunch apollo_routing topo_creator.launch map_name:=test
  ```

  执行成功后终端显示如下图所示，同时在`apollo.ros-8.0.0/modules/apollo_map/data/test`目录下还会多出`routing_map.bin`和`routing_map.txt`两个文件

  ![image-20220909140313105](images/image-20220909140313105.png)

- 到达此步骤，添加新的高精度地图流程已完成，在仿真测试时，选择对应的地图进行测试即可

## 维护者

[Forrest](709335543@qq.com)

