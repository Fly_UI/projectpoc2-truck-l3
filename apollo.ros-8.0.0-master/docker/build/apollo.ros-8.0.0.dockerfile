From osrf/ros:noetic-desktop-full
LABEL description="This image is base ros image for apollo.ros"

RUN apt update && apt install -y --no-install-recommends \
	python3-pip \
	vim net-tools \
	python3-osrf-pycommon \
	python3-catkin-tools \
	unzip \
	libtool \
	git \
	wget \
	libprotobuf-dev protobuf-compiler \
	libgoogle-glog-dev libgflags-dev libgtest-dev \
	libomp-dev \
	libcolpack-dev libadolc-dev \
	libtar-dev \
	libblas-dev  \
	coinor-libipopt-dev \
	gcc \
	g++ \
	gfortran \
	patch \
	pkg-config \
	liblapack-dev \
	libmetis-dev \
	liblapack-dev \
	language-pack-zh-hans \
	fontconfig \
	gdb xterm \
	software-properties-common \
	inetutils-ping

RUN cd / && wget https://github.com/abseil/abseil-cpp/archive/20200225.2.tar.gz \
	&& tar xzf 20200225.2.tar.gz \
	&& cd abseil-cpp-20200225.2 && mkdir build && cd build \
	&& cmake .. -DBUILD_SHARED_LIBS=ON \
	&& make -j8 && make install \
	&& cd ../.. && rm -rf 20200225.2.tar.gz abseil-cpp-20200225.2

RUN cd / && wget https://github.com/startcode/qp-oases/archive/v3.2.1-1.tar.gz \
	&& tar xzf v3.2.1-1.tar.gz \
	&& cd qp-oases-3.2.1-1 && mkdir bin \
	&& make -j8 CPPFLAGS="-Wall -pedantic -Wshadow -Wfloat-equal -O3 -Wconversion -Wsign-conversion -fPIC -DLINUX -DSOLVER_NONE -D__NO_COPYRIGHT__" \
	&& cp bin/libqpOASES.so /usr/local/lib \
	&& cp -r include/* /usr/local/include \
	&& cd .. && rm -rf v3.2.1-1.tar.gz qp-oases-3.2.1-1

RUN cd / && wget https://github.com/oxfordcontrol/osqp/archive/v0.5.0.tar.gz \
	&& tar xzf v0.5.0.tar.gz \
	&& cd osqp-0.5.0 \
	&& wget https://github.com/oxfordcontrol/qdldl/archive/v0.1.4.tar.gz \
	&& tar xzf v0.1.4.tar.gz --strip-components=1 -C ./lin_sys/direct/qdldl/qdldl_sources \
	&& rm -rf v0.1.4.tar.gz \
	&& mkdir build && cd build \
	&& cmake .. -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX="${SYSROOT_DIR}" -DCMAKE_BUILD_TYPE=Release \
	&& make -j8 && make install \
	&& cd ../.. && rm -rf v0.5.0.tar.gz osqp-0.5.0 \
	&& ldconfig

COPY fonts/yahei.ttf /usr/share/fonts/yahei.ttf
RUN cd /usr/share/fonts \
	&& mkdir /usr/share/fonts/chinese \
	&& chmod -R 755 /usr/share/fonts/chinese \
	&& fc-list :lang=zh \
	&& fc-cache

RUN useradd -ms /bin/bash apollo_ros \
	&& cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
	&& echo 'Asia/Shanghai' > /etc/timezone \
	&& echo "apollo_ros:0123456789" | chpasswd \
	&& echo 'apollo_ros    ALL=(ALL)   ALL' >> /etc/sudoers \
	&& locale-gen zh_CN.UTF-8 \
	&& echo "export LC_ALL=zh_CN.UTF-8">> /etc/profile \
	&& rm -rf /var/lib/apt/lists/*
	
WORKDIR /home/apollo_ros
USER apollo_ros
RUN mkdir log \
	apollo_ros_ws \
	apollo_ros_ws/build \
	apollo_ros_ws/devel \
	apollo_ros_ws/install \
	apollo_ros_ws/logs \
	apollo_ros_ws/src \  
	#add by travis
	&& echo "source /opt/ros/noetic/setup.bash" >> .bashrc    
#add by travis --begin	
COPY fonts/IpSmartPtr.hpp /usr/include/coin/
RUN echo "source /opt/ros/noetic/setup.bash" >> .bashrc

COPY fonts/matrix_vaeb.zip  /home/apollo_ros/
RUN cd /home/apollo_ros/ \
	&& unzip matrix_vaeb.zip \
	&& rm -rf matrix_vaeb.zip \
	&& echo "source /opt/ros/noetic/setup.bash" >> .bashrc
#add by travis --end
