# ProjectPoc2-Truck-L3

#this is project repository : access managed by louis-ad.
#软件和节点启动步骤
1、git项目到本地
   本地终端执行 
   git clone https://gitlab.com/louis-ad/projectpoc2-truck-l3.git

2、git大文件上传：
   由于视觉节点本身占空间比较大，可以在ubantu本地安装git lfs工具，来执行git push操作
   本地终端执行
   sudo apt-get install git-lfs
   git lfs install
   到git到的文件主目录下 使用以下命令跟踪大文件，<file> 是要跟踪的大文件的名称。
   git lfs track <file>
   添加、提交和推送文件到Git存储库的过程与常规Git操作相同。例如：
   git add <file>
   git commit -m "Add <file>"
   git push
   相应的，下载大文件时，要去大文件所在的路径，比如这里的/projectpoc2-truck-l3/apollo.ros-8.0.0-master/docker/build/fonts下面 执行 git lfs pull   
3、构建和启动docker
   本地终端执行 
   cd ~/projectpoc2-truck-l3/apollo.ros-8.0.0-master
   cd docker/build
   ./build_docker.sh
   cd ../..
   ./docker/scripts/docker_start.sh
   
4、进入docker 需要视觉信息时 需要先启动地平线的视觉节点
   docker下终端执行
   cd matrix_vaeb
   chmod 777 horizon_start.sh(此时如果需要密码  密码是0123456789 可在dockerfile自行修改)
   ./horizon_start.sh

5、视觉信息启动完毕，进入到apollo_ros_ws路径
   docker下终端执行
   cd ../apollo_ros_ws/

6、apollo8.0下各个节点启动：
   docker下终端执行
   进入apollo_ros_ws工作空间后，先执行代码编译
   catkin build
   再执行环境变量操作:
   source devel/setup.bash
   启动仿真界面：roslaunch apollo_simulation simulation.launch
   (目前版本的仿真程序，障碍物信息由视觉节点提供)
   启动定位节点：rosrun gps gps_node
              rosrun robot_gps robot_gps_node
   启动底盘节点：rosrun apollo_cartop apollo_cartop(注意：连接can卡之后，要去docker_start.sh里加一句：例如--device /dev/bus/usb/001/005，才能在docker下识别can设备
               rosrun apollo_cartop cartop_control
   启动控制参数的界面： rosrun rqt_reconfigure rqt_reconfigure

7、20231217：修改了docker_start.sh  让本地 /home/ubt/code//projectpoc2-truck-l3/apollo.ros-8.0.0-master/rosbag/和docker下/home/apollo_ros/apollo_ros_ws/src/apollo.ros-8.0.0/rosbag建立共享

（temporary copy）
/Gps2Apollo /apollo/canbus/chassis /apollo/control /apollo/control/pad /apollo/localization/pose /apollo/perception/traffic_light /apollo/planning /apollo/planning/pad /apollo/prediction /apollo/routing_request /apollo/routing_response /apollo/storytelling /apollo_simulation/obstacle /apollo_simulation/parameter_descriptions /apollo_simulation/parameter_updates /apollo_simulation/planning/reference_line /apollo_simulation/planning/trajectory /apollo_simulation/routing/path /apollo_simulation/sim_pose /apollo_simulation/sim_speed /fake_prediction /gps_data /initial_twist /initialpose /joint_states /move_base_simple/goal obstacle /rosout /rosout_agg /tf /tf_static
（temporary copy）

8、HDMap制作：
HDMap制作工具放在module/tools目录下 ，主要是map_gen和create_map两个工具。
(1)在运行rosrun robot_gps robot_gps_node之后，该UTM点迹文件会生成在/home/apollo_ros/apollo_ros_ws/src/apollo.ros-8.0.0/rosbag/data.csv路径下。

(2)经过处理转成没有表头，只有两列数据（分别是UTM_x和UTM_y)的格式,例如命名为 data.txt

(3)把data.txt放在map_gen路径下，在map_gen路径下执行

python3 map_gen_two_lanes_left_ext.py data.txt

(4)以上指令会在map_gen路径下生成一个map_data.txt.txt文件，再把map_data.txt.txt文件copy到create_map路径下

(5)create_map路径下  执行python3 convert_map_txt2bin.py -i  map_data.txt.txt  -o base_map.bin

(6)以上指令会在create_map路径下生成base_map.bin

(7)把base_map.bin和map_data.txt.txt（重命名为例如sim_map.txt）放置在projectpoc2-truck-l3/apollo.ros-8.0.0-master/modules/apollo_map/data/data路径下  
即可完成HDMap生成和替换

(8)生成拓扑地图
在`modules/apollo_map/data/`目录下新建以添加地图名字命名的文件夹，这里以`test`为例：

  ```shell
  cd apollo.ros-8.0.0/modules/apollo_map/data/
  mkdir test
  ```

- 将高精度地图`test`的地图文件`base_map.bin`放到刚刚创建的目录`apollo.ros-8.0.0/modules/apollo_map/data/test`中

  - 高精度地图文件`base_map.bin`是由高精度地图绘制工具绘制得来，常用的工具有`Roadrunner`，不过是收费的
  - 本项目的高精度地图均是在`LGSVL`仿真平台下载

  > **注意**：`apollo.ros-8.0.0`只支持`apollo5.0`百度`OpenDRIVE`格式规范

- 生成`routing`所需的拓扑地图：以下指令中的`test`为第一步在`modules/apollo_map/data/`目录下新建的文件夹名称

  ```shell
  cd apollo_ros_ws/
  source devel/setup.bash	
  roslaunch apollo_routing topo_creator.launch map_name:=test

  执行成功后，在`apollo.ros-8.0.0/modules/apollo_map/data/test`目录下还会多出`routing_map.bin`和`routing_map.txt`两个文件

  ![image-20220909140313105](images/image-20220909140313105.png)

  在运行仿真程序时  可以直接选择刚刚生成的拓扑地图

  roslaunch apollo_simulation simulation.launch map_name:=test
